﻿using CrystalReportWebAPI.Data;
using CrystalReportWebAPI.Models;
using CrystalReportWebAPI.Utilities;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace CrystalReportWebAPI.Controllers
{
    [RoutePrefix("api/localAlmacen")] 
    public class LocalAlmacenController : ApiController
    {


        [HttpPost]
        [Route("registro")]
        public String registroLocalAlmacen(AltmAlmacen altmAlmacen)
        {
            String resultado = "";
            try
            {
                resultado = WebAPI.registroLocalAlmacen(altmAlmacen);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado;// jsonConver.ToString();
        }


        [HttpPost]
        [Route("lista")]
        public List<AltmAlmacen> listaLocalAlmacen(AltmAlmacen altmAlmacen)
        {
            List<AltmAlmacen> resultado = new List<AltmAlmacen>();
            int estado = 1;
            try
            {
                resultado = WebAPI.listaLocalAlmacen(altmAlmacen);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }
    }
}