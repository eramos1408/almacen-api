﻿using CrystalReportWebAPI.Data;
using CrystalReportWebAPI.Models;
using CrystalReportWebAPI.Utilities;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;


namespace CrystalReportWebAPI.Controllers
{ 
    [RoutePrefix("api/layout")]
    public class LayoutController : ApiController
    {

        // GET: api/<controller>
        [HttpPost]
        [Route("ingreso")]
        public String layout(AltmLayout altmLayout)
        {
            String resultado = "";
            try
            {
                resultado = WebAPI.layout(altmLayout);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado;// jsonConver.ToString();
        }

        // GET: api/<controller>
        [HttpPost]
        [Route("update")]
        public String update(AltmLayout altmLayout)
        {
            String resultado = "";
            try
            {
                resultado = WebAPI.updateLayout(altmLayout);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado;// jsonConver.ToString();
        }

        [HttpPost]
        [Route("actualizaEstadoUbicacion")]
        public String actualizaEstadoUbicacion(AltmUbicacionFisica altmUbicacionFisica)
        {
            String resultado = "";
            try
            {
                resultado = WebAPI.actualizaEstadoUbicacion(altmUbicacionFisica);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado; // jsonConver.ToString();
        }


        [HttpPost]
        [Route("listaUbicacion")]
        public List<AltmUbicacionFisica> ubicaciones(AltmUbicacionFisica altmUbicacionFisica)
        {
            List<AltmUbicacionFisica> resultado = new List<AltmUbicacionFisica>();
            int estado = 1;
            try
            {
                resultado = WebAPI.ubicaciones(altmUbicacionFisica);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }

        [HttpPost]
        [Route("getUbicacionXCoUbicacion")]
        public ValidaUbicacionStocktatust getUbicacionXCoUbicacion(AltmLayout altmLayout)
        {

            ValidaUbicacionStocktatust resultado = new ValidaUbicacionStocktatust();
            try
            {
                resultado = WebAPI.getUbicacionXCoUbicacion(altmLayout);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }


        [HttpPost]
        [Route("listaLayout")]
        public List<AltmLayout> listaLayout(AltmLayout layout)
        {
            List<AltmLayout> resultado = new List<AltmLayout>();
            int estado = 1;
            try
            {
                resultado = WebAPI.listaLayout(layout);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }

        [HttpPost]
        [Route("layoutCelda")]
        public List<LayoutCeldas> layoutCelda(AltmLayout layout)
        {
            List<LayoutCeldas> resultado = new List<LayoutCeldas>();
            int estado = 1;
            try
            {
                resultado = WebAPI.layoutCelda(layout);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }

    }




}