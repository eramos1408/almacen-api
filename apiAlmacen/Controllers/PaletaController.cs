﻿using CrystalReportWebAPI.Data;
using CrystalReportWebAPI.Models;
using CrystalReportWebAPI.Utilities;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace CrystalReportWebAPI.Controllers
{
    [RoutePrefix("api/paleta")] 
    public class PaletaController : ApiController
    {


        [HttpPost]
        [Route("lista")]
        public List<AltvPaleta> listaPaleta(AltvPaleta altvPaleta)
        {
            List<AltvPaleta> resultado = new List<AltvPaleta>();
            int estado = 1;
            try
            {
                resultado = WebAPI.paletas(altvPaleta);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }

        [HttpPost]
        [Route("getMaxIdPaletaPorAlmacen")]
        public string getMaxIdPaleta_Por_Almacen(AltmRecepcion altmRecepcion)
        {

            string resutado = "";
            try
            {
                resutado = "" + (WebAPI.getMaxIdPaleta_Por_Almacen(altmRecepcion.idAlmacen) + 1);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resutado);
            //return jsonConver;
            return resutado;
        }



        [HttpPost]
        [Route("getPaletaXCodPaleta")]
        public ValidaMovimientoStockStatust getPaletaXCodPaleta(ValidaMovimientoStock movimiento)
        {
            ValidaMovimientoStockStatust validaMovimientoStockStatust;
            int resutado = 0;
            try
            {
                validaMovimientoStockStatust = WebAPI.getPaletaXCodPaleta(movimiento);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resutado);
            //return jsonConver;
            return validaMovimientoStockStatust;
        }

        [HttpPost]
        [Route("getUbicacionXCodPaleta")]
        public ValidaMovimientoStock getUbicacionXCodPaleta(ValidaMovimientoStock validaMovimientoStock)
        {
            ValidaMovimientoStock ValidaMovimientoStock1;
            int resutado = 0;
            try
            {
                ValidaMovimientoStock1 = WebAPI.getUbicacionXCodPaleta(validaMovimientoStock);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resutado);
            //return jsonConver;
            return ValidaMovimientoStock1;
        }

        [HttpPost]
        [Route("getPaletaXCodPaletaAlmacen")]
        public ValidaMovimientoStockStatust getPaletaXCodPaletaAlmacen(ValidaMovimientoStock movimiento)
        {
            ValidaMovimientoStockStatust validaMovimientoStockStatust;
            int resutado = 0;
            try
            {
                validaMovimientoStockStatust = WebAPI.getPaletaXCodPaletaAlmacen(movimiento);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resutado);
            //return jsonConver;
            return validaMovimientoStockStatust;
        }
        
    }
}