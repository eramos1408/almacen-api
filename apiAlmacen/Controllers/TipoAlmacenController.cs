﻿using CrystalReportWebAPI.Data;
using CrystalReportWebAPI.Models;
using CrystalReportWebAPI.Utilities;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace CrystalReportWebAPI.Controllers
{
    [RoutePrefix("api/tipoAlmacen")] 
    public class TipoAlmacenController : ApiController
    {


        [HttpPost]
        [Route("registro")]
        public String registroTipoAlmacen(AltrTipoUnidadAlmacena tipoUnidadAlmacena)
        {
            String resultado = "";
            try
            {
                resultado = WebAPI.registroTipoAlmacen(tipoUnidadAlmacena);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado; // jsonConver.ToString();
        }


        [HttpPost]
        [Route("lista")]
        public List<AltrTipoUnidadAlmacena> listaTipoAlmacen(AltrTipoUnidadAlmacena tipoUnidadAlmacena)
        {
            List<AltrTipoUnidadAlmacena> resultado = new List<AltrTipoUnidadAlmacena>();
            int estado = 1;
            try
            {
                resultado = WebAPI.listaTipoAlmacen(tipoUnidadAlmacena);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }
    }


}