﻿using CrystalReportWebAPI.Data;
using CrystalReportWebAPI.Models;
using CrystalReportWebAPI.Utilities;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.ServiceModel; 
using CrystalReportWebAPI.ServiceReference1;

namespace CrystalReportWebAPI.Controllers
{
    [RoutePrefix("api/movimiento")] 
    public class MovimientoController : ApiController
    {

        // GET: api/<controller>
        [HttpPost]
        [Route("registro")]
        public String movimiento(AltvStockMovimiento altvStockMovimiento)
        {
            String resultado = "";
            try
            {
                resultado = WebAPI.movimiento(altvStockMovimiento);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return jsonConver.ToString();
        }

        [HttpPost]
        [Route("movimientoSalidaEntrada")]
        public String movimientoSalidaEntrada(AltvStockMovimiento altvStockMovimiento)
        {
            String resultado = "";
            try
            {
                resultado = WebAPI.movimientoSalidaEntrada(altvStockMovimiento);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver.ToString();
            return resultado;
        }



        [HttpPost]
        [Route("getMovimientoStatus")]
        public ValidaMovimientoStockStatust getMovimientoStatus(ValidaMovimientoStock validaMovimientoStock)
        {
            ValidaMovimientoStockStatust resultado = null;
            try
            {
                if (validaMovimientoStock.inSalidaParaProduccion == "1")
                {
                    resultado = getMovimientoStatusProductoLoteDet(validaMovimientoStock);
                    if (resultado.status == "OK")
                    {
                        resultado = WebAPI.getMovimientoStatus(validaMovimientoStock);
                    }
                }
                else
                {
                    resultado = WebAPI.getMovimientoStatus(validaMovimientoStock);
                }

                //  resultado = WebAPI.getMovimientoStatus(validaMovimientoStock);
               // resultado.validaMovimientoStock.inSalidaParaProduccion = validaMovimientoStock.inSalidaParaProduccion;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado;
        }

        [HttpPost]
        [Route("getMovimientoStatusProductoLote")]
        public ValidaMovimientoStockStatust getMovimientoStatusProductoLote(ValidaMovimientoStock validaMovimientoStock)
        {
            return getMovimientoStatusProductoLoteDet(validaMovimientoStock);
        }


        public ValidaMovimientoStockStatust getMovimientoStatusProductoLoteDet(ValidaMovimientoStock validaMovimientoStock)
        {
            ValidaMovimientoStockStatust validaMovimientoStockStatust = new ValidaMovimientoStockStatust();


            try
            {
                BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                binding.MaxReceivedMessageSize = 20000000;
                binding.MaxBufferSize = 20000000;
                binding.MaxBufferPoolSize = 20000000;
                binding.AllowCookies = true;

                //EndpointAddress remoteAddress;
                //QA
                /*
               string ruta = "http://hanaqas.local:8001/sap/bc/srt/rfc/sap/zws_mm_read_matdoc/200/zsn_mm_read_matdoc/zbn_mm_read_matdoc";
              
               string userName = "Wachaica";
               string password = "sistemas14$";
                */

                //Produccion
                string ruta = "http://hanaprd.local:8001/sap/bc/srt/rfc/sap/zws_mm_read_matdoc/300/zsn_mm_read_matdoc/zbn_mm_read_matdoc";
                string userName = "ubicaciones";
                string password = "MPF123456";


                EndpointAddress remoteAddress = new EndpointAddress(ruta);

                ZWS_MM_READ_MATDOCClient cliente = new ZWS_MM_READ_MATDOCClient(binding, remoteAddress);
                cliente.ClientCredentials.UserName.UserName = userName;
                cliente.ClientCredentials.UserName.Password = password;

                String documento = validaMovimientoStock.documento;
                String anno = validaMovimientoStock.anno;

                ZfmMmReadMatdocRequest request = new ZfmMmReadMatdocRequest();
                ZfmMmReadMatdoc zfmMmReadMatdoc = new ZfmMmReadMatdoc();

                zfmMmReadMatdoc.IMblnr = documento; //"4900007119";
                zfmMmReadMatdoc.IMjahr = anno;
                zfmMmReadMatdoc.TData = new ZesMmReadMatdoc[0];
                request.ZfmMmReadMatdoc = zfmMmReadMatdoc;

                ZfmMmReadMatdocRequest request1 = request;

                ZfmMmReadMatdocResponse response = cliente.ZfmMmReadMatdoc(zfmMmReadMatdoc);

                int cantidad_Total = 0;
                int cantidad_Procesado = 0;
                int cantidad_No_Procesado = 0;
                int cantidad_Ya_Procesado = 0;
                string dato = "";
                if (response != null)
                {
                    ZesMmReadMatdoc[] pedido = response.TData;



                    String resultado = "";

                    cantidad_Total = pedido.Length;
                    string Mblnr;
                    string Mjahr;
                    string Zeile;
                    string Matnr;
                    string Charg;
                    string materialProd;
                    validaMovimientoStockStatust.status = "NO_OK";
                    validaMovimientoStockStatust.descripcion = "NO EXISTE LOTE ";

                    for (int j = 0; j < pedido.Length; j++)
                    {

                        Mblnr = pedido[j].Mblnr;
                        Mjahr = pedido[j].Mjahr;
                        Zeile = pedido[j].Zeile;
                        Matnr = pedido[j].Matnr;
                        Charg = pedido[j].Charg;

                        materialProd = pedido[j].Matnr.Substring(8);
                        //validaMovimientoStock.nuLote = "280415MERM";
                        if (materialProd == validaMovimientoStock.material)
                        {
                            if (Charg == validaMovimientoStock.nuLote)
                            {
                                validaMovimientoStockStatust.status = "OK";
                                validaMovimientoStockStatust.descripcion = "EXISTE LOTE PUEDE SALIR";
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                validaMovimientoStockStatust.status = "NO_OK";
                validaMovimientoStockStatust.descripcion = "NO EXISTE LOTE ";

            }
            return validaMovimientoStockStatust;



        }

        [HttpPost]
        [Route("lista")]
        public List<Movimiento> lista(AltvStockMovimiento altvStockMovimiento)
        {
            List<Movimiento> resultado = new List<Movimiento>();
            int estado = 1;
            try
            {
                resultado = WebAPI.listaMovimiento(altvStockMovimiento);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }
    }

}