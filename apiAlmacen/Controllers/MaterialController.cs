﻿using CrystalReportWebAPI.Data;
using CrystalReportWebAPI.Models;
using CrystalReportWebAPI.Utilities;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace CrystalReportWebAPI.Controllers
{
    [RoutePrefix("api/material")] 
    public class MaterialController : ApiController
    {

        // GET: api/<controller>
        [HttpPost]
        [Route("registro")]
        public int registroMateria(AlvmMaterial alvmMaterial)
        {
            int resultado = 0;
            try
            {
                resultado = WebAPI.regitroMaterial(alvmMaterial);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado;// jsonConver.ToString();
        }

        [HttpPost]
        [Route("listaProductoSAP")]
        public List<VwExtUbicaciones01> listaProductoSAP(VwExtUbicaciones01 vwExtUbicaciones01)
        {
            List<VwExtUbicaciones01> resultado = new List<VwExtUbicaciones01>();
            int estado = 1;
            try
            {
                resultado = WebAPI.listaProductos(vwExtUbicaciones01);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }

       
      
       
        
        /*
        [HttpPost]
        [Route("prueba")]
        public string prueba(VwExtUbicaciones01 vwExtUbicaciones01)
        {
            List<VwExtUbicaciones01> resultado = new List<VwExtUbicaciones01>();
            string dato = "";

            int estado = 1;
            try
            {
                dato = WebAPI.prueba(vwExtUbicaciones01);

            }
            catch (Exception ex)
            {
                dato = ex.Message;
                //throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return dato;
        }
        */

    }
}