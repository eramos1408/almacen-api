﻿using CrystalReportWebAPI.Data;
using CrystalReportWebAPI.Models;
using CrystalReportWebAPI.Utilities;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.ServiceModel;
using CrystalReportWebAPI.ServiceReference2;
using CrystalReportWebAPI.ServiceReference3;

namespace CrystalReportWebAPI.Controllers
{
    [RoutePrefix("api/recepcion")]
    public class RecepcionController : ApiController
    {

        // GET: api/<controller>
        [HttpPost]
        [Route("registro")]
        public RecepcionPaleta recepcion(AltmRecepcion AltmRecepcion)
        {
            RecepcionPaleta resultado = new RecepcionPaleta();
            try
            {
                resultado = WebAPI.recepcion(AltmRecepcion);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado;// jsonConver.ToString();
        }

        [HttpPost]
        [Route("registroQR")]
        public RecepcionPaleta recepcionQR(AltmRecepcion AltmRecepcion)
        {
            RecepcionPaleta resultado = new RecepcionPaleta();
            try
            {
                resultado = WebAPI.recepcionMigracionQR(AltmRecepcion);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado;// jsonConver.ToString();
        }


        // GET: api/<controller>
        [HttpPost]
        [Route("edicion")]
        public RecepcionPaleta edicion(AltvPaleta altvPaleta)
        {
            RecepcionPaleta resultado = new RecepcionPaleta();
            try
            {
                resultado = WebAPI.edicion(altvPaleta);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado;// jsonConver.ToString();
        }


        [HttpPost]
        [Route("registroImportacion")]
        public RecepcionPaleta registroImportacion(AltmRecepcion AltmRecepcion)
        {
            RecepcionPaleta resultado = new RecepcionPaleta();
            try
            {
                // resultado = WebAPI.recepcion(AltmRecepcion);

                return consuleServicio(AltmRecepcion);


            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado;// jsonConver.ToString();
        }

        private RecepcionPaleta consuleServicio(AltmRecepcion altmRecepcionCab)
        {
            RecepcionPaleta recepcionPaleta = new RecepcionPaleta();
            RecepcionPaleta recepcionPaletaProc = new RecepcionPaleta();
            try
            {
                BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                binding.MaxReceivedMessageSize = 20000000;
                binding.MaxBufferSize = 20000000;
                binding.MaxBufferPoolSize = 20000000;
                binding.AllowCookies = true;

                //EndpointAddress remoteAddress;
                //QA
                /*
              string ruta = "http://hanaqas.local:8001/sap/bc/srt/rfc/sap/zws_citas/200/zwa_citas/zbinding_citas";
              string userName = "wpomatay";
              string password = "Consolidado";
                */
                //Produccion
                string ruta = "http://hanaprd.local:8001/sap/bc/srt/rfc/sap/zws_citas/300/zwa_citas/zbinding_citas";

                string userName = "ubicaciones";
                string password = "MPF123456";



                EndpointAddress remoteAddress = new EndpointAddress(ruta);

                ZWS_CITASClient cliente = new ZWS_CITASClient(binding, remoteAddress);
                cliente.ClientCredentials.UserName.UserName = userName;
                cliente.ClientCredentials.UserName.Password = password;

                String fecInicial = altmRecepcionCab.feInicio;
                String fecFinal = altmRecepcionCab.feFin;
                ZMM_RFC_CITASRequest request = new ZMM_RFC_CITASRequest();
                ZMM_RFC_CITAS ZMM_RFC_CITAS = new ZMM_RFC_CITAS();
                ZMM_RFC_CITAS.FECHA_IMPRESION_INI = fecInicial;
                ZMM_RFC_CITAS.FECHA_IMPRESION_FIN = fecFinal;
                request.ZMM_RFC_CITAS = ZMM_RFC_CITAS;
                ZMM_RFC_CITASRequest request1 = request;

                ZMM_RFC_CITASResponse response1 = cliente.ZMM_RFC_CITAS(ZMM_RFC_CITAS);

                int cantidad_Total = 0;
                int cantidad_Procesado = 0;
                int cantidad_No_Procesado = 0;
                int cantidad_Ya_Procesado = 0;

                if (response1 != null)
                {
                    ZEMM_CITAS[] citas = response1.CITAS;

                    AltmRecepcion altmRecepcion;
                    AlvmMaterial alvmMaterial;

                    String resultado = "";

                    cantidad_Total = citas.Length;

                    if (cantidad_Total > 1)
                    {
                        for (int j = 0; j < citas.Length; j++)
                        {

                            altmRecepcion = new AltmRecepcion();
                            altmRecepcion.idAlmacen = altmRecepcionCab.idAlmacen;
                            altmRecepcion.idTipoMovimiento = altmRecepcionCab.idTipoMovimiento;

                            alvmMaterial = new AlvmMaterial();

                            alvmMaterial.idTipoMaterial = 1;
                            alvmMaterial.idGrupoMaterial = 1;
                            alvmMaterial.coMaterial = citas[j].CODIGO_MATERIAL;
                            alvmMaterial.deMaterial = citas[j].DESCRIPCION_MATERIAL;
                            resultado = WebAPI.regitroMaterialMigracion(alvmMaterial);

                            altmRecepcion.idMaterial = Int32.Parse(resultado);

                            altmRecepcion.idTipoUniAlmacena = 3;
                            altmRecepcion.feCrea = citas[j].FECHA_CREACION_CITA + citas[j].HORA_CREACION_CITA;
                            altmRecepcion.idUsuCrea = altmRecepcionCab.idUsuCrea;
                            altmRecepcion.idEstado = 1;
                            altmRecepcion.caPaletas = 1;
                            altmRecepcion.caCajaBolsa = (int)citas[j].CANTIDAD_BULTOS;
                            altmRecepcion.nuLote = citas[j].LOTE;
                            altmRecepcion.txtDocumento = "";


                            // altmRecepcion.IDPALETA_ALMACEN,
                            altmRecepcion.coPaleta22 = (citas[j].CITA + citas[j].POSICION_CITA + citas[j].POSICION_LOTE);


                            //altmRecepcion.idRecepcion,

                            altmRecepcion.idEstado = 1;
                            List<AltvPaleta> listAltvPaleta = WebAPI.paletasIdRecepcionExistentes(altmRecepcion);

                            if (listAltvPaleta.Count == 0)
                            {

                                recepcionPaletaProc = WebAPI.recepcionMigracion(altmRecepcion);
                                cantidad_Procesado = cantidad_Procesado + 1;
                            }
                            else
                            {
                                cantidad_Ya_Procesado = cantidad_Ya_Procesado + 1;
                            }

                        }
                    }
                    cantidad_No_Procesado = cantidad_Total - cantidad_Procesado;
                    recepcionPaleta.status = "OK";
                }
                recepcionPaleta.descripcion = "Total a Procesar: " + cantidad_Total + ", Procesado: " + cantidad_Procesado +
                    ", No Procesado: " + cantidad_No_Procesado + ", Procesado antes: " + cantidad_Ya_Procesado;
            }
            catch (Exception e)
            {
                recepcionPaleta.status = "NO_OK";
                recepcionPaleta.descripcion = "Error al Procesar...";

            }
            return recepcionPaleta;



        }

        
        // GET: api/<controller>
        [HttpPost]
        [Route("getRecepcionPaleta")]
        public RecepcionPaleta getRecepcionPaleta(AltmRecepcion AltmRecepcion)
        {
            RecepcionPaleta resultado = new RecepcionPaleta();
            try
            {
                resultado = WebAPI.getRecepcionPaleta(AltmRecepcion);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado;// jsonConver.ToString();
        }

        // GET: api/<controller>
        [HttpPost]
        [Route("getRecepcionCoPaleta")]
        public AltmRecepcion getRecepcionCoPaleta(AltmRecepcion AltmRecepcion)
        {
            AltmRecepcion resultado = new AltmRecepcion();
            try
            {
                resultado = WebAPI.getRecepcionCoPaleta(AltmRecepcion);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado;// jsonConver.ToString();
        }
                

        [HttpPost]
        [Route("trasladoQR")]
        public TrasladoResponseBean trasladoQR(TrasladoBean trasladoBean)
        {
            TrasladoResponseBean resultado = new TrasladoResponseBean();
            try
            {
                // resultado = WebAPI.recepcion(AltmRecepcion);

                return trasladoQRConsuleServicio(trasladoBean);


            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado;// jsonConver.ToString();
        }

        private TrasladoResponseBean trasladoQRConsuleServicio(TrasladoBean trasladoBean)
        {
            

            TrasladoResponseBean trasladoResponseBean = new TrasladoResponseBean();

            RecepcionPaleta recepcionPaleta = new RecepcionPaleta();
            RecepcionPaleta recepcionPaletaProc = new RecepcionPaleta();
            try
            {
                BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                binding.MaxReceivedMessageSize = 20000000;
                binding.MaxBufferSize = 20000000;
                binding.MaxBufferPoolSize = 20000000;
                binding.AllowCookies = true;

                //EndpointAddress remoteAddress;
                //QA
                string ruta = "http://hanaqas.local:8001/sap/bc/srt/rfc/sap/zws_pp_notif_trasl/200/zsn_pp_notif_trasl/zbn_pp_notif_trasl";
                //string ruta = "http://hanaqas.local:8001/sap/bc/srt/wsdl/flv_10002A111AD1/bndg_url/sap/bc/srt/rfc/sap/zws_pp_notif_trasl/200/zsn_pp_notif_trasl/zbn_pp_notif_trasl?sap-client=200";

                string userName = "wpomatay";
                string password = "Consolidado";

                //Produccion
                //string ruta = "http://hanaprd.local:8001/sap/bc/srt/rfc/sap/zws_pp_notif_trasl/300/zsn_pp_notif_trasl/zbn_pp_notif_trasl";
                //string ruta = "https://hanaprd.local:8001/sap/bc/srt/wsdl/flv_10002A111AD1/bndg_url/sap/bc/srt/rfc/sap/zws_pp_notif_trasl/300/zsn_pp_notif_trasl/zbn_pp_notif_trasl?sap-client=300";

                //string userName = "ubicaciones";
                //string password = "MPF123456";


                EndpointAddress remoteAddress = new EndpointAddress(ruta); 

                ServiceReference3.ZWS_PP_NOTIF_TRASLClient cliente = new ServiceReference3.ZWS_PP_NOTIF_TRASLClient(binding, remoteAddress);
                cliente.ClientCredentials.UserName.UserName = userName;
                cliente.ClientCredentials.UserName.Password = password;

                ZfPpNotifTrasl zfPpNotifTrasl1 = new ZfPpNotifTrasl();

                ZstParamNotifTrasl isParamField = new ZstParamNotifTrasl();
                isParamField.Aufnr = trasladoBean.aufnr;
                isParamField.Vornr = trasladoBean.vornr;
                isParamField.Posnr = trasladoBean.posnr;
                isParamField.Turno = trasladoBean.turno;
                isParamField.Linea = trasladoBean.linea;
                isParamField.Lgort = trasladoBean.lgort;
                isParamField.Menge = trasladoBean.menge;
                isParamField.Uname = trasladoBean.uname;
                isParamField.Texto = trasladoBean.texto;

                zfPpNotifTrasl1.IsParam = isParamField;


                zfPpNotifTrasl1.TReturn = new ZstReturnNotifTrasl[0];
                 
                ServiceReference3.ZfPpNotifTraslResponse response3 = cliente.ZfPpNotifTrasl(zfPpNotifTrasl1);                 

                if (response3 != null)
                {
                    ZstReturnNotifTrasl[] bapiret2 = response3.TReturn;
                   
                    if (bapiret2 !=null) {
                        ZstReturnNotifTrasl bapiretItem = bapiret2[0];
                        trasladoResponseBean.type = bapiretItem.Type;                 
                        trasladoResponseBean.message = bapiretItem.Message;
                    }

                }
                
            }
            catch (Exception e)
            {
                recepcionPaleta.status = "NO_OK";
                recepcionPaleta.descripcion = "Error al Procesar...";

            }
            return trasladoResponseBean;



        }

        [HttpPost]
        [Route("lista")]
        public List<Recepcion> lista(AltmRecepcion altmRecepcion)
        {
            List<Recepcion> resultado = new List<Recepcion>();
            int estado = 1;
            try
            {
                resultado = WebAPI.listaRecepcion(altmRecepcion);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }
    }

}