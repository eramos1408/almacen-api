﻿using CrystalReportWebAPI.Data;
using CrystalReportWebAPI.Models;
using CrystalReportWebAPI.Utilities;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace CrystalReportWebAPI.Controllers
{
    [RoutePrefix("api/[controller]")] 
    public class TipoMovimientoController : ApiController
    {

        [HttpGet]
        public List<AltrTipoMovimiento> tipoMovimiento()
        {
            List<AltrTipoMovimiento> resultado = new List<AltrTipoMovimiento>();
            int estado = 1;
            try
            {
                resultado = WebAPI.tipoMovimiento(estado);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }

        /*

        [HttpGet]
        public string tipoMovimiento()
        {
            string dato = "";
            List<AltrTipoMovimiento> resultado = new List<AltrTipoMovimiento>();
            int estado = 1;
            try
            {
                resultado = WebAPI.tipoMovimiento(estado);
            }
            catch (Exception ex)
            {

                //throw ex;
                dato = ex.Message;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return dato;
        }
        */
    }

}