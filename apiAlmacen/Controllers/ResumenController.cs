﻿using CrystalReportWebAPI.Data;
using CrystalReportWebAPI.Models;
using CrystalReportWebAPI.Utilities;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
namespace CrystalReportWebAPI.Controllers
{
    [RoutePrefix("api/resumen")] 
    public class ResumenController : ApiController
    {



        [HttpPost]
        [Route("lista")]
        public List<ResumenBean> resumenMovimiento(ResumenBean resumenBean)
        {
            List<ResumenBean> resultado = new List<ResumenBean>();
            int estado = 1;
            try
            {
                resultado = WebAPI.resumenMovimiento(resumenBean);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }

        [HttpPost]
        [Route("listaDetallado")]
        public List<ResumenBean> resumenMovimientoDetallado(ResumenBean resumenBean)
        {
            List<ResumenBean> resultado = new List<ResumenBean>();
            int estado = 1;
            try
            {
                resultado = WebAPI.resumenMovimientoDetallado(resumenBean);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }
        


        [HttpPost]
        [Route("resumenBloque")]
        public ResumenBloqueBean resumenBloque(AltmLayout layout)
        {
            ResumenBloqueBean resultado = new ResumenBloqueBean();

            try
            {
                resultado = WebAPI.resumenBloque(layout);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }
    }
}