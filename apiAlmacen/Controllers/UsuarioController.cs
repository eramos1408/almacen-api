﻿using CrystalReportWebAPI.Data;
using CrystalReportWebAPI.Models;
using CrystalReportWebAPI.Utilities;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace CrystalReportWebAPI.Controllers
{
    [RoutePrefix("api/usuario")] 
    public class UsuarioController : ApiController
    {


        [HttpPost]
        [Route("registro")]
        public String usuario(SetmUsuario setmUsuario)
        {
            String resultado = "";
            try
            {
                resultado = WebAPI.registroUsuario(setmUsuario);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado;// jsonConver.ToString();
        }


        [HttpPost]
        [Route("validaUsuario")]
        public String validaUsuario(SetmUsuario setmUsuario)
        {
            String resultado = "";
            try
            {
                resultado = WebAPI.validaUsuario(setmUsuario);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return jsonConver.ToString();
        }

        [HttpPost]
        [Route("loginUsuario")]
        public LoginUsuario loginUsuario(SetmUsuario setmUsuario)
        {
            LoginUsuario resultado = new LoginUsuario();
            try
            {
                resultado = WebAPI.loginUsuario(setmUsuario);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return resultado;
        }


        [HttpPost]
        [Route("lista")]
        public List<SetmUsuario> usuarioLista(SetmUsuario setmUsuario)
        {
            List<SetmUsuario> resultado = new List<SetmUsuario>();
            int estado = 1;
            try
            {
                resultado = WebAPI.listaResponsables(setmUsuario);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }
    }
}