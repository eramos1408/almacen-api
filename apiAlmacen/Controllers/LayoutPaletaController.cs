﻿using CrystalReportWebAPI.Data;
using CrystalReportWebAPI.Models;
using CrystalReportWebAPI.Utilities;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace CrystalReportWebAPI.Controllers
{
    [RoutePrefix("api/ubicacion")] 
    public class LayoutPaletaController : ApiController
    {

        [HttpPost]
        [Route("actualizaCantMaxPaletaUbicacion")]
        public String actualizaCantMaxPaletaUbicacion(AltmUbicacionFisica altmUbicacionFisica)
        {
            String resultado = "";
            try
            {
                resultado = WebAPI.actualizaCantMaxPaletaUbicacion(altmUbicacionFisica);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            return jsonConver.ToString();
        }


        [HttpPost]
        [Route("listaLayoutPaleta")]
        public List<UbicacionStock> listaLayoutPaleta(UbicacionStock layout)
        {
            List<UbicacionStock> resultado = new List<UbicacionStock>();
            int estado = 1;
            try
            {
                resultado = WebAPI.listaLayoutPaleta(layout);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }

        [HttpPost]
        [Route("layoutCelda")]
        public List<LayoutCeldas> layoutCelda(AltmLayout layout)
        {
            List<LayoutCeldas> resultado = new List<LayoutCeldas>();
            int estado = 1;
            try
            {
                resultado = WebAPI.layoutCelda(layout);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(resultado);
            //return jsonConver;
            return resultado;
        }

    }

}