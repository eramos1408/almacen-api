﻿using CrystalReportWebAPI.Models;
using CrystalReportWebAPI.Utilities;
using System.Net.Http;
using System.Web.Http;

namespace CrystalReportWebAPI.Controllers
{
    [RoutePrefix("api/Reports")]
    public class ReportsController : ApiController
    {
        [AllowAnonymous]
        [Route("Financial/VarianceAnalysisReport")]
        [HttpGet]
        [ClientCacheWithEtag(60)]  //1 min client side caching
        public HttpResponseMessage FinancialVarianceAnalysisReport()
        {
            string reportPath = "~/Reports/Financial";
            string reportFileName = "YTDVarianceCrossTab.rpt";
            string exportFilename = "YTDVarianceCrossTab.pdf";

            HttpResponseMessage result = CrystalReport.RenderReport(reportPath, reportFileName, exportFilename);
            return result;
        } 
        /*
        [AllowAnonymous]
        [Route("Demonstration/ComparativeIncomeStatement")]
        [HttpGet]
        [ClientCacheWithEtag(60)]  //1 min client side caching
        public HttpResponseMessage DemonstrationComparativeIncomeStatement()
        {
            string reportPath = "~/Reports/Demonstration";
            string reportFileName = "ComparativeIncomeStatement.rpt";
            string exportFilename = "ComparativeIncomeStatement.pdf";

            HttpResponseMessage result = CrystalReport.RenderReport(reportPath, reportFileName, exportFilename);
            return result;
        }
        */
        [AllowAnonymous]
        [Route("PlantillaPDF")]
        [HttpGet]
        [ClientCacheWithEtag(60)]  //1 min client side caching
        public HttpResponseMessage DemonstrationComparativeIncomeStatement_old()
        {
            string reportPath = "~/Reporte";
            string reportFileName = "ReportePaleta1.rpt";
            string exportFilename = "ReportePaleta1.pdf";

            // HttpResponseMessage result = CrystalReport.RenderReport(reportPath, reportFileName, exportFilename);
            AltvPaleta altvPaleta = new AltvPaleta();
            altvPaleta.idPaleta = -1;
            altvPaleta.idRecepcion = 30;
            HttpResponseMessage result = CrystalReport.imprimePalte(altvPaleta, reportPath, reportFileName, exportFilename);
            return result;
        }

        [AllowAnonymous]
        [Route("PlantillaPDF")]
        [HttpPost]
        [ClientCacheWithEtag(60)]  //1 min client side caching
        public HttpResponseMessage DemonstrationComparativeIncomeStatement(AltvPaleta altvPaleta)
        {
            string reportPath = "~/Reporte";
            string reportFileName = "ReportePaleta1.rpt";
            string exportFilename = "ReportePaleta1.pdf";

            // HttpResponseMessage result = CrystalReport.RenderReport(reportPath, reportFileName, exportFilename);
          /*  AltvPaleta altvPaleta = new AltvPaleta();
            altvPaleta.idPaleta = -1;
            altvPaleta.idRecepcion = 30;
          */
            HttpResponseMessage result = CrystalReport.imprimePalte(altvPaleta, reportPath, reportFileName, exportFilename);
            return result;
        }


        [AllowAnonymous]
        [Route("Demonstration/ImprimePaleta")]
        [HttpPost]
        [ClientCacheWithEtag(60)]  //1 min client side caching
        public HttpResponseMessage ImprimePaleta(Models.AltvPaleta altvPaleta)
        {
            string reportPath = "~/Reporte";
            string reportFileName = "Reporte1.rpt";
            string exportFilename = "Reporte1.pdf";

            HttpResponseMessage result = CrystalReport.imprimePalte(altvPaleta, reportPath, reportFileName, exportFilename);
            return result;
        }


        [AllowAnonymous]
        [Route("VersatileandPrecise/Invoice")]
        [HttpGet]
        [ClientCacheWithEtag(60)]  //1 min client side caching
        public HttpResponseMessage VersatileandPreciseInvoice()
        {
            string reportPath = "~/Reports/VersatileandPrecise";
            string reportFileName = "Invoice.rpt";
            string exportFilename = "Invoice.pdf";

            HttpResponseMessage result = CrystalReport.RenderReport(reportPath, reportFileName, exportFilename);
            return result;
        }

        [AllowAnonymous]
        [Route("VersatileandPrecise/FortifyFinancialAllinOneRetirementSavings")]
        [HttpGet]
        [ClientCacheWithEtag(60)]  //1 min client side caching
        public HttpResponseMessage VersatileandPreciseFortifyFinancialAllinOneRetirementSavings()
        {
            string reportPath = "~/Reports/VersatileandPrecise";
            string reportFileName = "FortifyFinancialAllinOneRetirementSavings.rpt";
            string exportFilename = "FortifyFinancialAllinOneRetirementSavings.pdf";

            HttpResponseMessage result = CrystalReport.RenderReport(reportPath, reportFileName, exportFilename);

            return result;
        }
    }
}