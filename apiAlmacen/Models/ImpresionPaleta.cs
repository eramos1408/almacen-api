﻿namespace CrystalReportWebAPI.Models
{
    public class ImpresionPaleta
    {
        public string idRecepcion { get; set; }
        public string idPaleta { get; set; }

        public string caPaleta { get; set; }
        public string coPaleta { get; set; }
        public byte[] coPaletaBarra { get; set; }
        public string coMaterial { get; set; }
        public string DeMaterial { get; set; }
        public string nuLote { get; set; }
        public string deResponsable { get; set; }
        public string deTipoUnidad { get; set; }

        public string caCajaBolsa { get; set; }
        public string fecha { get; set; }
        public string hora { get; set; }
        public string cantidad { get; set; }
    }
 
}