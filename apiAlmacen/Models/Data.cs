﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalReportWebAPI.Models
{
    public class AltvPaleta
    {
        public int idPaleta { get; set; }
        public String coPaleta { get; set; }
        public int idRecepcion { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }
        public int caCajaBolsa { get; set; }
        public String fecha { get; set; }
        public String hora { get; set; }

    }
  

    public class AltmAlmacen
    {
        public int idAlmacen { get; set; }
        public String coAlmacen { get; set; }
        public String deAlmacen { get; set; }
        public String deDireccion { get; set; }
        public int idCliente { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }

    }

    public class AltmCliente
    {
        public int idCliente { get; set; }
        public int coTipoDoc { get; set; }
        public String nuDocumento { get; set; }
        public String deRazon { get; set; }
        public int idEstado { get; set; }
        public String deDireccion { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
    }

    public class AltmLayout
    {
        public int idLayaout { get; set; }
        public String coLayaout { get; set; }
        public String deLayaout { get; set; }
        public int idAlmacen { get; set; }
        public int caBloques { get; set; }
        public int caNiveles { get; set; }
        public int caCarriles { get; set; }

        public int nuCantMax { get; set; }

        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }

        public String coUbicacionFisica { get; set; }

    }

    public class AltmPerfil
    {
        public int idPerfil { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }

    }

    public class AltmRecepcion
    {
        public int idRecepcion { get; set; }
        public int idPaleta { get; set; }
        public int idAlmacen { get; set; }
        public int idTipoMovimiento { get; set; }
        public int idMaterial { get; set; }
        public int idTipoUniAlmacena { get; set; }
        public string feCrea { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }
        public int caPaletas { get; set; }
        public String coPaleta22 { get; set; }
        public int caCajaBolsa { get; set; }
        public String nuLote { get; set; }
        public String txtDocumento { get; set; }
        public int idPaletasConsulta { get; set; }

        public String coPaleta { get; set; }
        public String material { get; set; }
        public String textoMaterial { get; set; }
        public String lote { get; set; }
        public String ubicacion { get; set; }
        public String coUbicacion { get; set; }

        public String feInicio { get; set; }
        public String feFin { get; set; }

    }
    public class TrasladoBean
    {


        public string aufnr { get; set; }
        public string vornr { get; set; }
        public string posnr { get; set; }
        public string turno { get; set; }
        public string linea { get; set; }
        public string lgort { get; set; }
        public decimal menge { get; set; }

        public string uname { get; set; }
        public string texto { get; set; }


    }

    public class TrasladoResponseBean
    {
        public String type { get; set; }
        public String id { get; set; }
        public String number { get; set; }
        public String message { get; set; }
        public String logNo { get; set; }
        public String logMsgNo { get; set; }
        public String messageV1 { get; set; }
        public String messageV2 { get; set; }
        public String messageV3 { get; set; }
        public String messageV4 { get; set; }
        public String parameter { get; set; }
        public String row { get; set; }
        public String field { get; set; }
        public String system { get; set; }
    }
        public class UbicacionStock
    {
        public int idAlmacen { get; set; }
        public int idUbicacionFisica { get; set; }
        public String coUbicacionFisica { get; set; }
        public int idLayaout { get; set; }
        public string coLayaout { get; set; }

        public int idBloque { get; set; }
        public string idNivel { get; set; }
        public string idCarril { get; set; }
        public int idEstado { get; set; }
        public int nuCantMax { get; set; }
        public String inFull { get; set; }
        public int caStock { get; set; }

    }

    public class AltmStock
    {
        public int idStock { get; set; }
        public int idUbicacionFisica { get; set; }
        public int caStock { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }

    }

    public class AltmUbicacionFisica
    {
        public int idUbicacionFisica { get; set; }
        public String coUbicacionFisica { get; set; }
        public int idLayaout { get; set; }
        public int idBloque { get; set; }
        public int idNivel { get; set; }
        public int idCarril { get; set; }
        public int idEstado { get; set; }
        public int nuCantMax { get; set; }
        public String inFull { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }

    }

    public class AltrBloque
    {
        public int idBloque { get; set; }
        public String coBloque { get; set; }
        public String deBloque { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }

    }

    public class AltrCarril
    {
        public int idCarril { get; set; }
        public String coCarril { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }

    }

    public class AltrEstado
    {
        public int idEstado { get; set; }
        public String deTabla { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }

    }

    public class AltrNivel
    {
        public int idNivel { get; set; }
        public String coNivel { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }

    }

    public class AltrTipoMaterial
    {
        public int idTipoMaterial { get; set; }
    }

    public class AltrTipoMovimiento
    {
        public int idTipoMovimiento { get; set; }
        public String coTipoMovimiento { get; set; }
        public String deTipoMovimiento { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }

    }

    public class AltrTipoUnidadAlmacena
    {
        public int idTipoUniAlmacena { get; set; }
        public String coTipoUniAlmacena { get; set; }
        public String deTipoUniAlmacena { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }

    }

   

    public class AltvStockMovimiento
    {
        public int idStockMovimiento { get; set; }

        public int idTipoMovimientoOld { get; set; }
        public int idTipoMovimiento { get; set; }
        public int idUbicacionFisicaOLd { get; set; }
        public int idUbicacionFisica { get; set; }
        public int idPaleta { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }

        public int idAlmacen { get; set; }

        public String material { get; set; }
        public String textoMaterial { get; set; }
        public String lote { get; set; }
        public String ubicacion { get; set; }
        public String coPaleta { get; set; }
        public String coUbicacion { get; set; }

        public String feInicio { get; set; }
        public String feFin { get; set; }

    }





    public class VwExtUbicaciones01
    {
        public int idMaterial { get; set; }
        public String centro { get; set; }
        public String material { get; set; }
        public String matDescripcion { get; set; }
        public String lote { get; set; }
        public String matTipo { get; set; }
        public String cantidadBase { get; set; }
        public String umBase { get; set; }
    }

    public class AlvmMaterial
    {
        public int idMaterial { get; set; }
        public int idTipoMaterial { get; set; }
        public int idGrupoMaterial { get; set; }
        public String coMaterial { get; set; }
        public String deMaterial { get; set; }

    }

    public class SetmOpcion
    {
        public int idOpcion { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }

    }

    public class SetmUsuario
    {
        public int idUsuario { get; set; }
        public String coUsuario { get; set; }
        public String deUsuario { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }
        public int idAlmacen { get; set; }
        public string deAlmacen { get; set; }
        public string deLocal { get; set; }

        public int inSupervisor { get; set; }
        public string deSupervisor { get; set; }
        public String deClave { get; set; }
    }

    public class LoginUsuario
    {
        public String status { get; set; }

        public SetmUsuario setmUsuario { get; set; }
    }



    public class RecepcionPaleta
    {
        public String status { get; set; }
        public AltmRecepcion recepcion { get; set; }
        public List<AltvPaleta> listAltvPaleta { get; set; }
        public String descripcion { get; set; }
    }

    public class SetvPerfilOpcion
    {
        public int idPerfilOpcion { get; set; }
        public int idPerfil { get; set; }
        public int idOpcion { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }

    }

    public class SetvUsuarioPerfil
    {
        public int idUsuarioPerfil { get; set; }
        public int idUsuario { get; set; }
        public int idPerfil { get; set; }
        public int idUsuCrea { get; set; }
        public int idUsuAct { get; set; }
        public int idEstado { get; set; }

    }

    public class Recepcion
    {
        public String idRecepcion { get; set; }
        public String idPaleta { get; set; }
        public String numeroAnno { get; set; }
        public String numero { get; set; }
        public String fecha { get; set; }
        public String hora { get; set; }
        public String txtDocumento { get; set; }
        public String material { get; set; }
        public String lote { get; set; }
        public String caPaletas { get; set; }
        public String caXPaleta { get; set; }
        public String idUsuario { get; set; }
        public String responsable { get; set; }
        public String idMotivo { get; set; }
        public String motivo { get; set; }
        public String textoMaterial { get; set; }
        public String idUma { get; set; }
        public String uma { get; set; }

    }

    public class Movimiento
    {

        public String idRecepcion { get; set; }
        public String palUbic { get; set; }
        public String fecha { get; set; }
        public String hora { get; set; }
        public String idPaleta { get; set; }
        public String ubic { get; set; }
        public String responsable { get; set; }
        public String motivo { get; set; }
        public String material { get; set; }
        public String textoMaterial { get; set; }
        public String lote { get; set; }
        public String uma { get; set; }
        public String bultos { get; set; }
        public String paletas { get; set; }
        public String cliente { get; set; }
        public String conInventario { get; set; }

    }

    public class ValidaMovimientoStock
    {

        public String idRecepcion { get; set; }
        public String idAlmacen { get; set; }
        public String idPaleta { get; set; }
        public String coPaleta { get; set; }
        public int caPaletas { get; set; }
        public int caCajaBolsa { get; set; }
        public String coTipoUniAlmacena { get; set; }
        public String coUbicacionFisica { get; set; }
        public int idStockMovimiento { get; set; }
        public int idUbicacionFisica { get; set; }
        public String coTipoMovimiento { get; set; }

        public int nuCantMax { get; set; }
        public int inFull { get; set; }
        public int caStock { get; set; }

        public String documento { get; set; }
        public String material { get; set; }
        public String deMaterial { get; set; }
        public String anno { get; set; }
        public String nuLote { get; set; }

        public String inSalidaParaProduccion { get; set; }

    }

    public class ValidaMovimientoStockStatust
    {
        public String status { get; set; }
        public String descripcion { get; set; }
        public ValidaMovimientoStock validaMovimientoStock { get; set; }
        public UbicacionStock ubicacionStock { get; set; }
    }

    public class ValidaUbicacionStocktatust
    {
        public String status { get; set; }
        public String descripcion { get; set; }
        public UbicacionStock ubicacionStock { get; set; }
    }

    public class LayoutCeldas
    {
        public String color;
        public String colorText;
        public int cols;
        public int rows;
        public String text;

    }

    public class ResumenBean
    {
        public String deAlmacen;
        public String material;
        public String textoMaterial;
        public String lote;
        public String uma;
        public String ubicacion;
        public String coUbicacion;
        public String tipoUma;
        public String idPaleta;
        public String sumaBultos;
        public String bulto;
        public String sumaPaletas;
        public int idStockMovimiento;
        public String coPaleta; 
        public int idAlmacen;
    }
    public class ResumenBloqueBean
    {
        public String totalAlmacen;
        public String llenoActivos;
        public String inactivos;
        public String disponibles;

        public String totalEspacioAlmacen;
        public String espacioActivos;
        public String espacioInactivos;
        public String espacioDisponibles;
    }

   

}