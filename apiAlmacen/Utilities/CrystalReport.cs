﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using System.Net;
using System.Net.Http;


using System.Data;
using CrystalReportWebAPI.Models;
using CrystalReportWebAPI.Data;
using System.Collections.Generic;

namespace CrystalReportWebAPI.Utilities
{
    public static class CrystalReport
    {
        public static HttpResponseMessage imprimePalte(Models.AltvPaleta altvPaleta, string reportPath, string reportFileName, string exportFilename)
        {
            var rd = new ReportDocument();

            rd.Load(Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(reportPath), reportFileName));

            /*
            ConnectionInfo connectionInfo = new ConnectionInfo();
             

            connectionInfo.ServerName = "192.168.0.3";
            connectionInfo.DatabaseName = "ALMACEN_2";
            connectionInfo.UserID = "sa";
           
            connectionInfo.Password = "admin";

            rd.SetParameterValue("id_recepcion", 1) ;
            SetDBLogonForReport(connectionInfo, ref rd);

            */
            List<ImpresionPaleta> listImpresionPaleta = new List<ImpresionPaleta>();

            listImpresionPaleta = WebAPI.listaPaletasImpresion(altvPaleta);

            rd.SetDataSource(listImpresionPaleta);

            MemoryStream ms = new MemoryStream();
            using (var stream = rd.ExportToStream(ExportFormatType.PortableDocFormat))
            {
                stream.CopyTo(ms);
            }
            rd.Close();
            rd.Dispose();
            //GC.Collect();


            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(ms.ToArray())
            };
            result.Content.Headers.ContentDisposition =
                new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = exportFilename
                };
            result.Content.Headers.ContentType =
                new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
            return result;
        }

        public static HttpResponseMessage RenderReport(string reportPath, string reportFileName, string exportFilename)
        {
            var rd = new ReportDocument();

            rd.Load(Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(reportPath), reportFileName));

            ConnectionInfo connectionInfo = new ConnectionInfo();


            connectionInfo.ServerName = "192.168.0.23";
            connectionInfo.DatabaseName = "ALMACEN_2";
            connectionInfo.UserID = "sa";

            connectionInfo.Password = "admin";

            rd.SetParameterValue("id_recepcion", 1);
            SetDBLogonForReport(connectionInfo, ref rd);

            MemoryStream ms = new MemoryStream();
            using (var stream = rd.ExportToStream(ExportFormatType.PortableDocFormat))
            {
                stream.CopyTo(ms);
            }

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(ms.ToArray())
            };
            result.Content.Headers.ContentDisposition =
                new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = exportFilename
                };
            result.Content.Headers.ContentType =
                new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
            return result;
        }

        private static void SetDBLogonForReport(ConnectionInfo connectionInfo, ref ReportDocument reportDocument)
        {

            Tables tables = reportDocument.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
            {
                TableLogOnInfo tableLogonInfo = table.LogOnInfo;

                tableLogonInfo.ConnectionInfo = connectionInfo;

                table.ApplyLogOnInfo(tableLogonInfo);
            }
        }
    }
}
    
