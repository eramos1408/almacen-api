﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrystalReportWebAPI.Models;

using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Collections;
using Limilabs.Barcode;
using System.IO;

using Sap.Data.Hana;
using 
    System.Collections;
using System.Collections.Generic;

namespace CrystalReportWebAPI.Data
{
    public class WebAPI
    {
        public static List<ImpresionPaleta> listaPaletasImpresion(AltvPaleta altvPaleta)
        {

            List<ImpresionPaleta> listImpresionPaleta = new List<ImpresionPaleta>();
            ImpresionPaleta impresionPaleta = new ImpresionPaleta();
            SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
            ConectString.Open();
            StringBuilder sqlText = new StringBuilder();
            try
            {


                sqlText.Append(" SELECT RE.ID_RECEPCION, PA.ID_PALETA,  RE.CA_PALETAS, ISNULL(PA.CA_CAJA_BOLSA, RE.CA_CAJA_BOLSA)  CA_CAJA_BOLSA, RE.NU_LOTE, PA.CO_PALETA, CONVERT(VARCHAR(20), FORMAT(PA.FE_CREA, 'dd/MM/yyyy')) AS FECHA, CONVERT(VARCHAR(20), FORMAT(PA.FE_CREA, 'HH:mm:ss')) AS HORA,  MA.CO_MATERIAL, MA.DE_MATERIAL, TU.CO_TIPO_UNI_ALMACENA ");
                sqlText.Append(" ,U.DE_USUARIO RESPONSABLE ");
                sqlText.Append(" FROM DBO.ALTM_RECEPCION RE, DBO.ALVM_MATERIAL MA, ALTR_TIPO_UNIDAD_ALMACENA TU, DBO.ALTV_PALETA PA , dbo.SETM_USUARIO U ");
                sqlText.Append("     WHERE RE.ID_RECEPCION = PA.ID_RECEPCION AND RE.ID_MATERIAL = MA.ID_MATERIAL AND  RE.ID_TIPO_UNI_ALMACENA = TU.ID_TIPO_UNI_ALMACENA ");
                sqlText.Append("            AND RE.ID_USU_CREA = U.ID_USUARIO AND RE.ID_RECEPCION = " + altvPaleta.idRecepcion + "  AND (" + altvPaleta.idPaleta + "= -1 OR PA.ID_PALETA = " + altvPaleta.idPaleta + ")");
                sqlText.Append(" ORDER BY PA.ID_PALETA ASC ");


                SqlCommand cmd = new SqlCommand(sqlText.ToString(), ConectString);

                SqlDataReader barras = cmd.ExecuteReader();


                BaseBarcode b = BarcodeFactory.GetBarcode(Symbology.Code128);
                b.Number = "123456789012";
                b.ChecksumAdd = true;

                b.NarrowBarWidth = 3;
                b.Height = 300;
                b.FontHeight = 0.1f;
                while (barras.Read())
                {
                    impresionPaleta = new ImpresionPaleta();
                    impresionPaleta.idRecepcion = barras.GetValue(0).ToString();
                    impresionPaleta.idPaleta = barras.GetValue(1).ToString();
                    impresionPaleta.caPaleta = barras.GetValue(2).ToString();
                    impresionPaleta.cantidad = barras.GetValue(3).ToString();
                    impresionPaleta.nuLote = barras.GetValue(4).ToString();

                    b.Number = barras.GetValue(5).ToString();
                    MemoryStream ms = new MemoryStream();
                    b.Save(ms, ImageType.Png, 300, 300);

                    impresionPaleta.coPaletaBarra = ms.GetBuffer();

                    // impresionPaleta.fecha = barras.GetValue(6).ToString();
                    impresionPaleta.coPaleta = barras.GetValue(5).ToString();
                    impresionPaleta.fecha = barras.GetValue(6).ToString();
                    impresionPaleta.hora = barras.GetValue(7).ToString();
                    impresionPaleta.coMaterial = barras.GetValue(8).ToString();
                    impresionPaleta.DeMaterial = barras.GetValue(9).ToString();
                    impresionPaleta.deTipoUnidad = barras.GetValue(10).ToString();
                    impresionPaleta.deResponsable = barras.GetValue(11).ToString();


                    listImpresionPaleta.Add(impresionPaleta);

                }
            }
            catch (Exception e)
            {
                _ = e.Message;
                listImpresionPaleta = null;
            }
            finally
            {
                ConectString.Close();
            }
            return listImpresionPaleta;
        }

    

    public static string registroUsuario(SetmUsuario setmUsuario)
    {
        String resultado = "";
        string sqlUsuario = "";



        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        //ID_STOCK_MOVIMIENTO,
        SqlCommand cmd = null;
        if (setmUsuario.idUsuario != 0)
        {

            sqlUsuario = "UPDATE DBO.SETM_USUARIO " +
                " SET  DE_USUARIO = '" + setmUsuario.deUsuario + "'," +
                "      DE_CLAVE = '" + setmUsuario.deClave + "'," +
                "      ID_ALMACEN = " + setmUsuario.idAlmacen + "," +
                "      IN_SUPERVISOR = " + setmUsuario.inSupervisor + "," +
                "      FE_ACTUALIZA = GETDATE()," +
                "      ID_USU_ACT  = " + setmUsuario.idUsuAct +
                "      WHERE ID_USUARIO = " + setmUsuario.idUsuario;
            cmd = new SqlCommand(sqlUsuario, ConectString);
            // Assign transaction object for a pending local transaction
        }
        else
        {
            resultado = validRegistroaUsuario(setmUsuario);

            if (resultado != "OK")
            {
                return resultado;
            }
            sqlUsuario = "INSERT INTO  DBO.SETM_USUARIO(CO_USUARIO, DE_USUARIO, DE_CLAVE,FE_CREA, ID_USU_CREA, ID_ESTADO, ID_ALMACEN, IN_SUPERVISOR  )" +
                             "VALUES('" + setmUsuario.coUsuario + "', '" + setmUsuario.deUsuario + "',  '" + setmUsuario.deClave + "', GETDATE(), " + setmUsuario.idUsuCrea + ", " + setmUsuario.idEstado + ", " + setmUsuario.idAlmacen + " ," + setmUsuario.inSupervisor + " )";
            cmd = new SqlCommand(sqlUsuario, ConectString);

        }
        cmd.Transaction = transaction;

        try
        {

            cmd = new SqlCommand(sqlUsuario, ConectString);
            cmd.Transaction = transaction;
            cmd.ExecuteNonQuery();
            resultado = "OK";

            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
        }
        finally
        {
            ConectString.Close();
        }
        return resultado;


    }


    public static string validRegistroaUsuario(SetmUsuario setmUsuario)
    {
        String resultado = "";
        String data = "";
        int result = 0;
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        SqlCommand cmd;
        int registro = 0;

        try
        {
            //Cantidad Ingreso
            data = " SELECT ISNULL(MAX(ID_USUARIO),0) FROM dbo.SETM_USUARIO m WHERE M.CO_USUARIO = '" + setmUsuario.coUsuario + "'";
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            registro = Convert.ToInt32(cmd.ExecuteScalar());

            //Valida DNI Registrado
            if (registro >= 1)
            {
                resultado = "Numero de Documento ya existe";
                return resultado;
            }
            else
            {
                resultado = "OK";

            }

            //Valida Nombre
            //Cantidad Ingreso
            data = " SELECT ISNULL(MAX(ID_USUARIO),0) FROM dbo.SETM_USUARIO m WHERE M.DE_USUARIO = '" + setmUsuario.deUsuario + "'";
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            registro = Convert.ToInt32(cmd.ExecuteScalar());


            if (registro >= 1)
            {
                resultado = "Nombres de Usuario ya existe";
                return resultado;
            }
            else
            {
                resultado = "OK";

            }

            // transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
        }
        finally
        {
            ConectString.Close();
        }
        return resultado;
    }

    public static int regitroMaterial(AlvmMaterial alvmMaterial)
    {
        int resultado = 0;
        int result = 0;
        int recepcionId = 0;
        string data = "";
        string sqlTockInsert = "";
        string sqlTockUpdate = "";



        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        SqlCommand cmd;

        int registro = 0;

        try
        {

            //Cantidad Ingreso
            data = " SELECT ISNULL(MAX(ID_MATERIAL),0) FROM dbo.ALVM_MATERIAL m WHERE M.CO_MATERIAL = '" + alvmMaterial.coMaterial + "'";
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            registro = Convert.ToInt32(cmd.ExecuteScalar());


            if (registro == 0)
            {
                //Insert Detalle                
                //ID_STOCK,
                sqlTockInsert = "INSERT INTO     DBO.ALVM_MATERIAL (ID_TIPO_MATERIAL, ID_GRUPO_MATERIAL, CO_MATERIAL, DE_MATERIAL)" +
                        "VALUES(" + alvmMaterial.idTipoMaterial + ", " + alvmMaterial.idGrupoMaterial + ",'" + alvmMaterial.coMaterial + "', '" + alvmMaterial.deMaterial + "');" +
                        "SELECT CAST(scope_identity() AS int)";

                cmd = new SqlCommand(sqlTockInsert, ConectString);
                cmd.Transaction = transaction;
                //  resultado = "Exito: " + cmd.ExecuteNonQuery();
                resultado = (int)cmd.ExecuteScalar();

            }
            else
            {
                resultado =   registro;

                //Upodate
                sqlTockUpdate = "UPDATE DBO.ALVM_MATERIAL  " +
                                    " SET     " +
                                    "    DE_MATERIAL ='" + alvmMaterial.deMaterial + "'" +
                                    "   WHERE ID_MATERIAL = " + registro;
                cmd = new SqlCommand(sqlTockUpdate, ConectString);
                cmd.Transaction = transaction;
                //  resultado = "Exito: " + cmd.ExecuteNonQuery();

            }
            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = 0;
        }
        finally
        {
            ConectString.Close();
        }
        return resultado;
    }


    public static string regitroMaterialMigracion(AlvmMaterial alvmMaterial)
    {
        String resultado = "";
        int result = 0;
        int recepcionId = 0;
        string data = "";
        string sqlTockInsert = "";
        string sqlTockUpdate = "";



        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        SqlCommand cmd;

        int registro = 0;

        try
        {

            //Cantidad Ingreso
            data = " SELECT ISNULL(MAX(ID_MATERIAL),0) FROM dbo.ALVM_MATERIAL m WHERE M.CO_MATERIAL = RIGHT('" + alvmMaterial.coMaterial + "' , 10) ";
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            registro = Convert.ToInt32(cmd.ExecuteScalar());


            if (registro == 0)
            {
                //Insert Detalle                
                //ID_STOCK,
                sqlTockInsert = "INSERT INTO     DBO.ALVM_MATERIAL (ID_TIPO_MATERIAL, ID_GRUPO_MATERIAL, CO_MATERIAL, DE_MATERIAL)" +
                        "VALUES(" + alvmMaterial.idTipoMaterial + ", " + alvmMaterial.idGrupoMaterial + ", RIGHT('" + alvmMaterial.coMaterial + "', 10), '" + alvmMaterial.deMaterial + "');" +
                        "SELECT CAST(scope_identity() AS int)";

                cmd = new SqlCommand(sqlTockInsert, ConectString);
                cmd.Transaction = transaction;
                //  resultado = "Exito: " + cmd.ExecuteNonQuery();
                resultado = "" + (int)cmd.ExecuteScalar();

            }
            else
            {
                resultado = "" + registro;

                //Upodate
                sqlTockUpdate = "UPDATE DBO.ALVM_MATERIAL  " +
                                    " SET     " +
                                    "    DE_MATERIAL ='" + alvmMaterial.deMaterial + "'" +
                                    "   WHERE ID_MATERIAL = " + registro;
                cmd = new SqlCommand(sqlTockUpdate, ConectString);
                cmd.Transaction = transaction;
                //  resultado = "Exito: " + cmd.ExecuteNonQuery();

            }
            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
        }
        finally
        {
            ConectString.Close();
        }
        return resultado;
    }

    public static List<VwExtUbicaciones01> listaProductos(VwExtUbicaciones01 vwExtUbicaciones01)
    {
        String resultado = "";

        List<VwExtUbicaciones01> listVwExtUbicaciones01 = new List<VwExtUbicaciones01>();
        VwExtUbicaciones01 material = new VwExtUbicaciones01();
        HanaConnection ConectString = new HanaConnection(DBConn.sapDS(null));
        ConectString.Open();

        try
        {

            HanaCommand cmd = new HanaCommand("SELECT CENTRO, MATERIAL, MAT_DESCRIPCION, LOTE, MAT_TIPO,CANTIDAD_BASE,UM_BASE FROM VW_EXT_UBICACIONES_01 WHERE MATERIAL like '%" + vwExtUbicaciones01.material + "%' AND MAT_DESCRIPCION like '" + vwExtUbicaciones01.matDescripcion + "%' AND LOTE like '" + vwExtUbicaciones01.lote + "%' ORDER BY MATERIAL ASC", ConectString);

            HanaDataReader barras = cmd.ExecuteReader();
            int contado = 0;
            while (barras.Read())
            {
                contado++;
                material = new VwExtUbicaciones01();
                material.idMaterial = contado;
                material.centro = barras.GetValue(0).ToString();
                material.material = barras.GetValue(1).ToString();
                material.matDescripcion = barras.GetValue(2).ToString();
                material.lote = barras.GetValue(3).ToString();
                material.matTipo = barras.GetValue(4).ToString();
                material.cantidadBase = barras.GetValue(5).ToString();
                material.umBase = barras.GetValue(6).ToString();

                listVwExtUbicaciones01.Add(material);

            }
        }
        catch (Exception e)
        {
            listVwExtUbicaciones01 = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listVwExtUbicaciones01;
    }
        /*
    public static string prueba(VwExtUbicaciones01 vwExtUbicaciones01)
    {
        String resultado = "";

        List<VwExtUbicaciones01> listVwExtUbicaciones01 = new List<VwExtUbicaciones01>();
        VwExtUbicaciones01 material = new VwExtUbicaciones01();
        HanaConnection ConectString = new HanaConnection(DBConn.sapDS(null));
        ConectString.Open();

        try
        {

            HanaCommand cmd = new HanaCommand("SELECT CENTRO, MATERIAL, MAT_DESCRIPCION, LOTE, MAT_TIPO,CANTIDAD_BASE,UM_BASE FROM VW_EXT_UBICACIONES_01 WHERE MATERIAL like '%" + vwExtUbicaciones01.material + "%' AND MAT_DESCRIPCION like '" + vwExtUbicaciones01.matDescripcion + "%' ORDER BY MATERIAL ASC", ConectString);

            HanaDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                material = new VwExtUbicaciones01();
                material.centro = barras.GetValue(0).ToString();
                material.material = barras.GetValue(1).ToString();
                material.matDescripcion = barras.GetValue(2).ToString();
                material.lote = barras.GetValue(3).ToString();
                material.matTipo = barras.GetValue(4).ToString();
                material.cantidadBase = barras.GetValue(5).ToString();
                material.umBase = barras.GetValue(6).ToString();

                listVwExtUbicaciones01.Add(material);


            }
            resultado = "Conecto";
        }
        catch (Exception e)
        {
            listVwExtUbicaciones01 = null;
            resultado = e.Message;
        }
        finally
        {
            ConectString.Close();
        }
        return resultado;
    }

        */
    public static List<SetmUsuario> listaResponsables(SetmUsuario usuario)
    {
        String resultado = "";

        List<SetmUsuario> listSetmUsuario = new List<SetmUsuario>();
        SetmUsuario setmUsuario = new SetmUsuario();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();

        try
        {

            SqlCommand cmd = new SqlCommand("SELECT U.ID_USUARIO,U.CO_USUARIO, U.DE_USUARIO, U.ID_ESTADO,U.ID_ALMACEN, A.DE_ALMACEN,   (CASE  U.IN_SUPERVISOR WHEN 1 THEN 'Personal'  WHEN 2 THEN 'Supervisor'  END ) AS supervisor , U.DE_CLAVE,U.IN_SUPERVISOR   FROM DBO.SETM_USUARIO U, DBO.ALTM_ALMACEN A WHERE U.ID_ALMACEN = A.ID_ALMACEN AND U.CO_USUARIO like '" + usuario.coUsuario + "%' AND DE_USUARIO like '" + usuario.deUsuario + "%' ORDER BY U.ID_USUARIO ASC", ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                setmUsuario = new SetmUsuario();

                setmUsuario.idUsuario = Int32.Parse(barras.GetValue(0).ToString());
                setmUsuario.coUsuario = barras.GetValue(1).ToString();
                setmUsuario.deUsuario = barras.GetValue(2).ToString();
                setmUsuario.idEstado = Int32.Parse(barras.GetValue(3).ToString());
                setmUsuario.idAlmacen = Int32.Parse(barras.GetValue(4).ToString());
                setmUsuario.deLocal = barras.GetValue(5).ToString();
                setmUsuario.deSupervisor = barras.GetValue(6).ToString();
                setmUsuario.deClave = barras.GetValue(7).ToString();
                setmUsuario.inSupervisor = Int32.Parse(barras.GetValue(8).ToString());

                listSetmUsuario.Add(setmUsuario);

            }
        }
        catch (Exception e)
        {
            listSetmUsuario = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listSetmUsuario;
    }


    public static string registroTipoAlmacen(AltrTipoUnidadAlmacena altrTipoUnidadAlmacena)
    {
        String resultado = "";
        string sqlUsuario = "";


        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        //ID_STOCK_MOVIMIENTO,
        sqlUsuario = "INSERT INTO  DBO.ALTR_TIPO_UNIDAD_ALMACENA(CO_TIPO_UNI_ALMACENA, DE_TIPO_UNI_ALMACENA, FE_CREA, ID_USU_CREA, ID_ESTADO )" +
                            "VALUES('" + altrTipoUnidadAlmacena.coTipoUniAlmacena.ToUpper() + "', '" + altrTipoUnidadAlmacena.deTipoUniAlmacena.ToUpper() + "', GETDATE(), " + altrTipoUnidadAlmacena.idUsuCrea + ", " + altrTipoUnidadAlmacena.idEstado + "  )";
        SqlCommand cmd = new SqlCommand(sqlUsuario, ConectString);
        // Assign transaction object for a pending local transaction
        cmd.Transaction = transaction;

        try
        {

            cmd = new SqlCommand(sqlUsuario, ConectString);
            cmd.Transaction = transaction;
            resultado = "Exito: " + cmd.ExecuteNonQuery();

            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
        }
        finally
        {
            ConectString.Close();
        }
        return resultado;


    }

    public static List<AltrTipoUnidadAlmacena> listaTipoAlmacen(AltrTipoUnidadAlmacena tipoUnidadAlmacena)
    {
        String resultado = "";

        List<AltrTipoUnidadAlmacena> listAltrTipoUnidadAlmacena = new List<AltrTipoUnidadAlmacena>();
        AltrTipoUnidadAlmacena altrTipoUnidadAlmacena = new AltrTipoUnidadAlmacena();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();

        try
        {

            SqlCommand cmd = new SqlCommand("SELECT ID_TIPO_UNI_ALMACENA, CO_TIPO_UNI_ALMACENA, DE_TIPO_UNI_ALMACENA,  ID_ESTADO  FROM DBO.ALTR_TIPO_UNIDAD_ALMACENA WHERE CO_TIPO_UNI_ALMACENA like '" + tipoUnidadAlmacena.coTipoUniAlmacena + "%' AND DE_TIPO_UNI_ALMACENA like '" + tipoUnidadAlmacena.deTipoUniAlmacena + "%' ORDER BY ID_TIPO_UNI_ALMACENA ASC", ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                altrTipoUnidadAlmacena = new AltrTipoUnidadAlmacena();

                altrTipoUnidadAlmacena.idTipoUniAlmacena = Int32.Parse(barras.GetValue(0).ToString());
                altrTipoUnidadAlmacena.coTipoUniAlmacena = barras.GetValue(1).ToString();
                altrTipoUnidadAlmacena.deTipoUniAlmacena = barras.GetValue(2).ToString();
                altrTipoUnidadAlmacena.idEstado = Int32.Parse(barras.GetValue(3).ToString());

                listAltrTipoUnidadAlmacena.Add(altrTipoUnidadAlmacena);

            }
        }
        catch (Exception e)
        {
            listAltrTipoUnidadAlmacena = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listAltrTipoUnidadAlmacena;
    }



    public static string registroLocalAlmacen(AltmAlmacen altmAlmacen)
    {
        String resultado = "";
        string sqlUsuario = "";

        resultado = validaAlmacen(altmAlmacen);

        if (resultado != "OK")
        {
            return resultado;
        }

        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;
        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        SqlCommand cmd = null;


        try
        {
            if (altmAlmacen.idAlmacen != 0)
            {
                sqlUsuario = "   UPDATE DBO.ALTM_ALMACEN " +
                "   SET DE_ALMACEN =   '" + altmAlmacen.deAlmacen + "'," +
                "       DE_DIRECCION =  '" + altmAlmacen.deDireccion + "', " +
                "       FE_ACTUALIZA = GETDATE()," +
                "      ID_USU_ACT  = " + altmAlmacen.idUsuAct +
                " WHERE ID_ALMACEN = " + altmAlmacen.idAlmacen;

                cmd = new SqlCommand(sqlUsuario, ConectString);
                // Assign transaction object for a pending local transaction
                cmd.Transaction = transaction;
                cmd = new SqlCommand(sqlUsuario, ConectString);
            }
            else
            {
                //ID_STOCK_MOVIMIENTO,
                sqlUsuario = "INSERT INTO  DBO.ALTM_ALMACEN(CO_ALMACEN, DE_ALMACEN, DE_DIRECCION, ID_CLIENTE, FE_CREA, ID_USU_CREA, ID_ESTADO )" +
                                    "VALUES('" + altmAlmacen.coAlmacen + "', '" + altmAlmacen.deAlmacen + "', '" + altmAlmacen.deDireccion + "', " + altmAlmacen.idCliente + ", GETDATE(), " + altmAlmacen.idUsuCrea + ", " + altmAlmacen.idEstado + "  )";
                cmd = new SqlCommand(sqlUsuario, ConectString);
                // Assign transaction object for a pending local transaction
                cmd.Transaction = transaction;
                cmd = new SqlCommand(sqlUsuario, ConectString);
            }
            cmd.Transaction = transaction;
            cmd.ExecuteNonQuery();
            resultado = "OK";
            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
        }
        finally
        {
            ConectString.Close();
        }
        return resultado;
    }

    public static string validaAlmacen(AltmAlmacen altmAlmacen)
    {
        String resultado = "";
        String data = "";
        int result = 0;
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        SqlCommand cmd;
        int registro = 0;

        try
        {
            //Cantidad Ingreso
            data = " SELECT ISNULL(MAX(ID_ALMACEN),0) FROM dbo.ALTM_ALMACEN m WHERE M.DE_ALMACEN = '" + altmAlmacen.deAlmacen + "' ";
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            registro = Convert.ToInt32(cmd.ExecuteScalar());


            if (registro >= 1)
            {
                resultado = "Ya existe Almacen  con el mismo nombre";
                return resultado;
            }
            else
            {
                resultado = "OK";

            }
            //transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
        }
        finally
        {
            ConectString.Close();
        }
        return resultado;
    }

    public static List<AltmAlmacen> listaLocalAlmacen(AltmAlmacen almacen)
    {
        String resultado = "";

        List<AltmAlmacen> listAltmAlmacen = new List<AltmAlmacen>();
        AltmAlmacen altmAlmacen = new AltmAlmacen();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();

        try
        {

            SqlCommand cmd = new SqlCommand("SELECT ID_ALMACEN, CO_ALMACEN, DE_ALMACEN, DE_DIRECCION, ID_CLIENTE,  ID_ESTADO  FROM DBO.ALTM_ALMACEN WHERE CO_ALMACEN like '" + almacen.coAlmacen + "%' AND DE_ALMACEN like '" + almacen.deAlmacen + "%' AND  DE_DIRECCION like '" + almacen.deDireccion + "%' ORDER BY ID_ALMACEN ASC", ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                altmAlmacen = new AltmAlmacen();

                altmAlmacen.idAlmacen = Int32.Parse(barras.GetValue(0).ToString());
                altmAlmacen.coAlmacen = barras.GetValue(1).ToString();
                altmAlmacen.deAlmacen = barras.GetValue(2).ToString();
                altmAlmacen.deDireccion = barras.GetValue(3).ToString();
                altmAlmacen.idCliente = Int32.Parse(barras.GetValue(4).ToString());
                altmAlmacen.idEstado = Int32.Parse(barras.GetValue(5).ToString());

                listAltmAlmacen.Add(altmAlmacen);

            }
        }
        catch (Exception e)
        {
            listAltmAlmacen = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listAltmAlmacen;
    }

    public static string validaUsuario(SetmUsuario setmUsuario)
    {
        String resultado = "";
        String data = "";
        int result = 0;
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        SqlCommand cmd;
        int registro = 0;

        try
        {
            //Cantidad Ingreso
            data = " SELECT ISNULL(MAX(ID_USUARIO),0) FROM dbo.SETM_USUARIO m WHERE M.CO_USUARIO = '" + setmUsuario.coUsuario + "' AND  M.DE_CLAVE = '" + setmUsuario.deClave + "'";
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            registro = Convert.ToInt32(cmd.ExecuteScalar());


            if (registro == 0)
            {
                resultado = "NO_OK";
            }
            else
            {
                resultado = "OK";

            }
            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
        }
        finally
        {
            ConectString.Close();
        }
        return resultado;
    }

    public static SetmUsuario getUsuario(int idUsuario)
    {
        String resultado = "";

        SetmUsuario setmUsuario = new SetmUsuario();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        String sqlUsuario = "";

        try
        {
            sqlUsuario = "SELECT U.ID_USUARIO, U.CO_USUARIO, U.DE_USUARIO, U.ID_ESTADO, U.ID_ALMACEN, A.DE_ALMACEN, U.IN_SUPERVISOR,  (CASE  U.IN_SUPERVISOR WHEN 1 THEN 'Personal'  WHEN 2 THEN 'Supervisor'  END ) AS supervisor   FROM DBO.SETM_USUARIO U, DBO.ALTM_ALMACEN A WHERE U.ID_ALMACEN = A.ID_ALMACEN  AND U.ID_USUARIO = " + idUsuario;
            SqlCommand cmd = new SqlCommand(sqlUsuario, ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                setmUsuario = new SetmUsuario();

                setmUsuario.idUsuario = Int32.Parse(barras.GetValue(0).ToString());
                setmUsuario.coUsuario = barras.GetValue(1).ToString();
                setmUsuario.deUsuario = barras.GetValue(2).ToString();
                setmUsuario.idEstado = Int32.Parse(barras.GetValue(3).ToString());
                setmUsuario.idAlmacen = Int32.Parse(barras.GetValue(4).ToString());
                setmUsuario.deAlmacen = barras.GetValue(5).ToString();
                setmUsuario.inSupervisor = Int32.Parse(barras.GetValue(6).ToString());
                setmUsuario.deSupervisor = barras.GetValue(7).ToString();

                break;

            }
        }
        catch (Exception e)
        {
            setmUsuario = null;
        }
        finally
        {
            ConectString.Close();
        }
        return setmUsuario;
    }


    public static LoginUsuario loginUsuario(SetmUsuario setmUsuario)
    {
        String resultado = "";
        SetmUsuario usuario = new SetmUsuario();
        LoginUsuario loginUsuario = new LoginUsuario();

        String data = "";
        int result = 0;
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        SqlCommand cmd;
        int registro = 0;

        try
        {
            //Cantidad Ingreso
            data = " SELECT ISNULL(MAX(ID_USUARIO),0) FROM dbo.SETM_USUARIO m WHERE M.CO_USUARIO = '" + setmUsuario.coUsuario + "' AND  M.DE_CLAVE = '" + setmUsuario.deClave + "'";
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            registro = Convert.ToInt32(cmd.ExecuteScalar());


            if (registro == 0)
            {
                resultado = "NO_OK";
                loginUsuario.status = resultado;
            }
            else
            {
                resultado = "OK";
                loginUsuario.status = resultado;
                loginUsuario.setmUsuario = getUsuario(registro);

            }
            //transaction.Commit();
        }
        catch (Exception e)
        {
            //transaction.Rollback();
            resultado = "NO_OK";
            loginUsuario.status = resultado;

        }
        finally
        {
            ConectString.Close();
        }
        return loginUsuario;
    }



    public static List<AltrTipoMovimiento> tipoMovimiento(int estado)
    {
        String resultado = "";

        List<AltrTipoMovimiento> listAltrTipoMovimiento = new List<AltrTipoMovimiento>();
        AltrTipoMovimiento altrTipoMovimiento = new AltrTipoMovimiento();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();

        try
        {

            SqlCommand cmd = new SqlCommand("SELECT ID_TIPO_MOVIMIENTO, CO_TIPO_MOVIMIENTO, DE_TIPO_MOVIMIENTO, ID_ESTADO FROM DBO.ALTR_TIPO_MOVIMIENTO  ORDER BY ID_TIPO_MOVIMIENTO ASC", ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                altrTipoMovimiento = new AltrTipoMovimiento();

                altrTipoMovimiento.idTipoMovimiento = Int32.Parse(barras.GetValue(0).ToString());
                altrTipoMovimiento.coTipoMovimiento = barras.GetValue(1).ToString();
                altrTipoMovimiento.deTipoMovimiento = barras.GetValue(2).ToString();
                altrTipoMovimiento.idEstado = Int32.Parse(barras.GetValue(3).ToString());

                listAltrTipoMovimiento.Add(altrTipoMovimiento);

            }
        }
        catch (Exception e)
        {

            altrTipoMovimiento = new AltrTipoMovimiento();
            altrTipoMovimiento.deTipoMovimiento = e.Message;

            listAltrTipoMovimiento = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listAltrTipoMovimiento;
    }

    public static List<AltvPaleta> paletas(AltvPaleta paleta)
    {
        String resultado = "";

        List<AltvPaleta> listAltvPaleta = new List<AltvPaleta>();
        AltvPaleta altvPaleta = new AltvPaleta();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();

        try
        {

            SqlCommand cmd = new SqlCommand("SELECT ID_PALETA,  CONCAT(P.ID_PALETA,'-',CONVERT(VARCHAR(50),FORMAT ( P.FE_CREA, 'yy' ))) as  CO_PALETA, ID_RECEPCION, ID_ESTADO FROM DBO.ALTV_PALETA P ORDER BY ID_PALETA DESC", ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                altvPaleta = new AltvPaleta();

                altvPaleta.idPaleta = Int32.Parse(barras.GetValue(0).ToString());
                altvPaleta.coPaleta = barras.GetValue(1).ToString();
                altvPaleta.idRecepcion = Int32.Parse(barras.GetValue(2).ToString());
                altvPaleta.idEstado = Int32.Parse(barras.GetValue(3).ToString());

                listAltvPaleta.Add(altvPaleta);

            }
        }
        catch (Exception e)
        {
            listAltvPaleta = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listAltvPaleta;
    }

    public static List<AltvPaleta> paletasIdRecepcion(AltvPaleta paleta)
    {
        String resultado = "";

        List<AltvPaleta> listAltvPaleta = new List<AltvPaleta>();
        AltvPaleta altvPaleta = new AltvPaleta();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        String sqlRecepcionPaleta = "";

        try
        {

            sqlRecepcionPaleta = "SELECT ID_PALETA,  P.CO_PALETA as  CO_PALETA, ID_RECEPCION, ID_ESTADO, " +
                " CONVERT(VARCHAR(20), FORMAT(P.FE_CREA, 'dd/MM/yyyy')) AS FECHA, " +
                " CONVERT(VARCHAR(20), FORMAT(P.FE_CREA, 'HH:mm:ss')) AS HORA" +
                " FROM DBO.ALTV_PALETA P " +
                "WHERE  ID_RECEPCION= " + paleta.idRecepcion + " " +
                "ORDER BY ID_PALETA DESC";
            SqlCommand cmd = new SqlCommand(sqlRecepcionPaleta, ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                altvPaleta = new AltvPaleta();
                altvPaleta.idPaleta = Int32.Parse(barras.GetValue(0).ToString());
                altvPaleta.coPaleta = barras.GetValue(1).ToString();
                altvPaleta.idRecepcion = Int32.Parse(barras.GetValue(2).ToString());
                altvPaleta.idEstado = Int32.Parse(barras.GetValue(3).ToString());
                altvPaleta.fecha = barras.GetValue(4).ToString();
                altvPaleta.hora = barras.GetValue(5).ToString();

                listAltvPaleta.Add(altvPaleta);

            }
        }
        catch (Exception e)
        {
            listAltvPaleta = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listAltvPaleta;
    }

    public static List<AltvPaleta> paletasIdRecepcionExistentes(AltmRecepcion altmRecepcion)
    {
        String resultado = "";

        List<AltvPaleta> listAltvPaleta = new List<AltvPaleta>();
        AltvPaleta altvPaleta = new AltvPaleta();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        String sqlRecepcionPaleta = "";

        try
        {

            sqlRecepcionPaleta = "SELECT ID_PALETA,  P.CO_PALETA as  CO_PALETA, ID_RECEPCION, ID_ESTADO, " +
                " CONVERT(VARCHAR(20), FORMAT(P.FE_CREA, 'dd/MM/yyyy')) AS FECHA, " +
                " CONVERT(VARCHAR(20), FORMAT(P.FE_CREA, 'HH:mm:ss')) AS HORA" +
                " FROM DBO.ALTV_PALETA P " +
                "WHERE  CO_PALETA= RIGHT('" + altmRecepcion.coPaleta22 + "', 22) " +
                "ORDER BY ID_PALETA DESC";
            SqlCommand cmd = new SqlCommand(sqlRecepcionPaleta, ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                altvPaleta = new AltvPaleta();
                altvPaleta.idPaleta = Int32.Parse(barras.GetValue(0).ToString());
                altvPaleta.coPaleta = barras.GetValue(1).ToString();
                altvPaleta.idRecepcion = Int32.Parse(barras.GetValue(2).ToString());
                altvPaleta.idEstado = Int32.Parse(barras.GetValue(3).ToString());
                altvPaleta.fecha = barras.GetValue(4).ToString();
                altvPaleta.hora = barras.GetValue(5).ToString();

                listAltvPaleta.Add(altvPaleta);

            }
        }
        catch (Exception e)
        {
            listAltvPaleta = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listAltvPaleta;
    }
    public static List<AltvPaleta> paletasIdRecepcionId(AltvPaleta paleta)
    {
        String resultado = "";
        List<AltvPaleta> listAltvPaleta = new List<AltvPaleta>();

        AltvPaleta altvPaleta = new AltvPaleta();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        String sqlRecepcionPaleta = "";

        try
        {

            sqlRecepcionPaleta = "SELECT ID_PALETA,   P.CO_PALETA as  CO_PALETA, ID_RECEPCION, ID_ESTADO, " +
                " CONVERT(VARCHAR(20), FORMAT(P.FE_CREA, 'dd/MM/yyyy')) AS FECHA, " +
                " CONVERT(VARCHAR(20), FORMAT(P.FE_CREA, 'HH:mm:ss')) AS HORA" +
                " FROM DBO.ALTV_PALETA P " +
                "WHERE  ID_RECEPCION= " + paleta.idRecepcion + " AND ID_PALETA= " + paleta.idPaleta +
                "ORDER BY ID_PALETA DESC";
            SqlCommand cmd = new SqlCommand(sqlRecepcionPaleta, ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                altvPaleta = new AltvPaleta();
                altvPaleta.idPaleta = Int32.Parse(barras.GetValue(0).ToString());
                altvPaleta.coPaleta = barras.GetValue(1).ToString();
                altvPaleta.idRecepcion = Int32.Parse(barras.GetValue(2).ToString());
                altvPaleta.idEstado = Int32.Parse(barras.GetValue(3).ToString());
                altvPaleta.fecha = barras.GetValue(4).ToString();
                altvPaleta.hora = barras.GetValue(5).ToString();
                listAltvPaleta.Add(altvPaleta);
                break;

            }
        }
        catch (Exception e)
        {
            listAltvPaleta = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listAltvPaleta;
    }


    public static List<AltmUbicacionFisica> ubicaciones(AltmUbicacionFisica ubi)
    {
        String resultado = "";

        List<AltmUbicacionFisica> listAltmUbicacionFisica = new List<AltmUbicacionFisica>();
        AltmUbicacionFisica altmUbicacionFisica = new AltmUbicacionFisica();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();

        try
        {

            SqlCommand cmd = new SqlCommand("SELECT ID_UBICACION_FISICA, CO_UBICACION_FISICA, NU_CANT_MAX,ID_ESTADO  FROM dbo.ALTM_UBICACION_FISICA UF ORDER BY ID_UBICACION_FISICA ASC", ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                altmUbicacionFisica = new AltmUbicacionFisica();

                altmUbicacionFisica.idUbicacionFisica = Int32.Parse(barras.GetValue(0).ToString());
                altmUbicacionFisica.coUbicacionFisica = barras.GetValue(1).ToString();
                altmUbicacionFisica.nuCantMax = Int32.Parse(barras.GetValue(2).ToString());
                altmUbicacionFisica.idEstado = Int32.Parse(barras.GetValue(3).ToString());

                listAltmUbicacionFisica.Add(altmUbicacionFisica);

            }
        }
        catch (Exception e)
        {
            listAltmUbicacionFisica = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listAltmUbicacionFisica;
    }

    public static List<AltmLayout> listaLayout(AltmLayout layout)
    {
        String resultado = "";

        List<AltmLayout> listAltmLayout = new List<AltmLayout>();
        AltmLayout altmLayout = new AltmLayout();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        string sqlLayout = "";
        try
        {
            sqlLayout = "SELECT ID_LAYAOUT, CO_LAYAOUT, ID_ALMACEN, CA_BLOQUES, CA_NIVELES, CA_CARRILES, NU_CANT_MAX,ID_ESTADO FROM ALTM_LAYOUT" +
                         " WHERE  ID_ALMACEN = " + layout.idAlmacen;

            SqlCommand cmd = new SqlCommand(sqlLayout, ConectString);
            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                altmLayout = new AltmLayout();

                altmLayout.idLayaout = Int32.Parse(barras.GetValue(0).ToString());
                altmLayout.coLayaout = barras.GetValue(1).ToString();
                altmLayout.idAlmacen = Int32.Parse(barras.GetValue(2).ToString());
                altmLayout.caBloques = Int32.Parse(barras.GetValue(3).ToString());
                altmLayout.caNiveles = Int32.Parse(barras.GetValue(4).ToString());
                altmLayout.caCarriles = Int32.Parse(barras.GetValue(5).ToString());
                altmLayout.nuCantMax = Int32.Parse(barras.GetValue(6).ToString());
                altmLayout.idEstado = Int32.Parse(barras.GetValue(7).ToString());

                listAltmLayout.Add(altmLayout);

            }
        }
        catch (Exception e)
        {
            listAltmLayout = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listAltmLayout;
    }


        public static AltmLayout layoutUnico(AltmLayout layout)
        {
            String resultado = "";
             
            AltmLayout altmLayout = new AltmLayout();
            SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
            ConectString.Open();
            string sqlLayout = "";
            try
            {
                sqlLayout = "SELECT ID_LAYAOUT, CO_LAYAOUT, ID_ALMACEN, CA_BLOQUES, CA_NIVELES, CA_CARRILES, NU_CANT_MAX,ID_ESTADO FROM ALTM_LAYOUT " +                            
                            " WHERE  ID_LAYAOUT = " + layout.idLayaout + " AND ID_ALMACEN = " + layout.idAlmacen;

                SqlCommand cmd = new SqlCommand(sqlLayout, ConectString);
                SqlDataReader barras = cmd.ExecuteReader();

                while (barras.Read())
                {
                    altmLayout = new AltmLayout();

                    altmLayout.idLayaout = Int32.Parse(barras.GetValue(0).ToString());
                    altmLayout.coLayaout = barras.GetValue(1).ToString();
                    altmLayout.idAlmacen = Int32.Parse(barras.GetValue(2).ToString());
                    altmLayout.caBloques = Int32.Parse(barras.GetValue(3).ToString());
                    altmLayout.caNiveles = Int32.Parse(barras.GetValue(4).ToString());
                    altmLayout.caCarriles = Int32.Parse(barras.GetValue(5).ToString());
                    altmLayout.nuCantMax = Int32.Parse(barras.GetValue(6).ToString());
                    altmLayout.idEstado = Int32.Parse(barras.GetValue(7).ToString());

                    break;

                }
            }
            catch (Exception e)
            {
                altmLayout = null;
            }
            finally
            {
                ConectString.Close();
            }
            return altmLayout;
        }


        public static List<UbicacionStock> listaLayoutPaleta(UbicacionStock layout)
    {
        String resultado = "";

        List<UbicacionStock> listUbicacionStock = new List<UbicacionStock>();
        UbicacionStock ubicacionStock = new UbicacionStock();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        string sqlLayout = "";
        try
        {
            sqlLayout = "SELECT UI.CO_UBICACION_FISICA, " +
                              " UI.ID_UBICACION_FISICA, " +
                              " UI.ID_LAYAOUT, " +
                              " L.CO_LAYAOUT, " +
                              " UI.ID_BLOQUE, " +
                              " UI.ID_NIVEL, " +
                              " UI.ID_CARRIL, " +
                              " UI.ID_ESTADO, " +
                              " UI.NU_CANT_MAX, " +
                              " UI.IN_FULL, " +
                              " S.CA_STOCK " +
                              " FROM ALTM_UBICACION_FISICA UI, ALTM_STOCK S, ALTM_LAYOUT L " +
                        " WHERE UI.ID_UBICACION_FISICA = S.ID_UBICACION_FISICA AND UI.ID_LAYAOUT = L.ID_LAYAOUT AND " +
                             " L.ID_ALMACEN = " + layout.idAlmacen +
                             " AND ( L.CO_LAYAOUT =  '" + layout.coLayaout + "' OR '' = '" + layout.coLayaout + "' )" +
                             " AND ( UI.ID_NIVEL = '" + layout.idNivel + "' OR '' = '" + layout.idNivel + "' )" +
                             " AND ( UI.ID_CARRIL = '" + layout.idCarril + "' OR '' = '" + layout.idCarril + "' )" +
                        " ORDER BY UI.ID_UBICACION_FISICA ASC ";

            SqlCommand cmd = new SqlCommand(sqlLayout, ConectString);
            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                ubicacionStock = new UbicacionStock();


                ubicacionStock.coUbicacionFisica = barras.GetValue(0).ToString();
                ubicacionStock.idUbicacionFisica = Int32.Parse(barras.GetValue(1).ToString());
                ubicacionStock.idLayaout = Int32.Parse(barras.GetValue(2).ToString());
                ubicacionStock.coLayaout = barras.GetValue(3).ToString();
                ubicacionStock.idBloque = Int32.Parse(barras.GetValue(4).ToString());
                ubicacionStock.idNivel = barras.GetValue(5).ToString();
                ubicacionStock.idCarril = barras.GetValue(6).ToString();
                ubicacionStock.idEstado = Int32.Parse(barras.GetValue(7).ToString());
                ubicacionStock.nuCantMax = Int32.Parse(barras.GetValue(8).ToString());
                ubicacionStock.inFull = barras.GetValue(9).ToString();
                ubicacionStock.caStock = Int32.Parse(barras.GetValue(10).ToString());
                listUbicacionStock.Add(ubicacionStock);

            }
        }
        catch (Exception e)
        {
            listUbicacionStock = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listUbicacionStock;
    }


    public static RecepcionPaleta recepcion(AltmRecepcion altmRecepcion)
    {
        String resultado = "";
        int result = 0;
        int recepcionId = 0;
        string sql = "";
        string sqlPaleta = "";

        RecepcionPaleta recepcionPaleta = new RecepcionPaleta();

        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);



        sql = "INSERT INTO DBO.ALTM_RECEPCION(ID_ALMACEN, ID_TIPO_MOVIMIENTO, ID_MATERIAL, ID_TIPO_UNI_ALMACENA, FE_CREA, ID_USU_CREA, ID_ESTADO, CA_PALETAS, CA_CAJA_BOLSA, NU_LOTE, TXT_DOCUMENTO) " +

        "VALUES(" + altmRecepcion.idAlmacen + ", " + altmRecepcion.idTipoMovimiento + ", " + altmRecepcion.idMaterial + ", " + altmRecepcion.idTipoUniAlmacena + ",  GETDATE()," + altmRecepcion.idUsuCrea + ", " + altmRecepcion.idEstado + ", " + altmRecepcion.caPaletas + ", " + altmRecepcion.caCajaBolsa + ", '" + altmRecepcion.nuLote + "', '" + altmRecepcion.txtDocumento + "' );" +
        "SELECT CAST(scope_identity() AS int)";
        SqlCommand cmd = new SqlCommand(sql, ConectString);
        // Assign transaction object for a pending local transaction
        cmd.Transaction = transaction;


        try
        {
            //OracleDataReader barras = cmd.ExecuteNonQuery();
            //result =  cmd.ExecuteNonQuery();
            recepcionId = (int)cmd.ExecuteScalar();
            //Insert Detalle
            int idPaletaAlmacenCont = 0;
            int idPaletaAlmacen = getMaxIdPaleta_Por_Almacen(altmRecepcion.idAlmacen);
            for (int i = 1; i <= altmRecepcion.caPaletas; i++)
            {

                idPaletaAlmacenCont = idPaletaAlmacen + i;
                sqlPaleta = "INSERT INTO DBO.ALTV_PALETA(ID_ALMACEN, ID_PALETA_ALMACEN, CO_PALETA, ID_RECEPCION, FE_CREA, ID_USU_CREA, ID_ESTADO) " +
                              "VALUES(" + altmRecepcion.idAlmacen + ", " + idPaletaAlmacenCont + ",  + CONCAT(" + idPaletaAlmacenCont + ", CONVERT(VARCHAR(50), FORMAT(GETDATE(), 'yy'))," + altmRecepcion.idAlmacen + ") , " + recepcionId + ", GETDATE(), " + altmRecepcion.idUsuCrea + ", " + altmRecepcion.idEstado + ")";

                cmd = new SqlCommand(sqlPaleta, ConectString);
                cmd.Transaction = transaction;
                resultado = "Exito: " + cmd.ExecuteNonQuery();

            }
            transaction.Commit();
            recepcionPaleta.status = "OK";
            AltvPaleta altvPaleta = new AltvPaleta();
            altvPaleta.idRecepcion = recepcionId;
            AltmRecepcion recepcion = new AltmRecepcion();
            recepcion.idRecepcion = recepcionId;
            recepcionPaleta.recepcion = recepcion;
            recepcionPaleta.listAltvPaleta = paletasIdRecepcion(altvPaleta);
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
            recepcionPaleta.status = "NO_OK";
            recepcionPaleta.listAltvPaleta = null;
        }
        finally
        {
            ConectString.Close();
        }
        return recepcionPaleta;
    }

    public static RecepcionPaleta edicion(AltvPaleta altvPaleta1)
    {
        String resultado = "";
        int result = 0;
        int recepcionId = 0;
        string sql = "";
        string sqlPaleta = "";

        RecepcionPaleta recepcionPaleta = new RecepcionPaleta();

        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);


        try
        {
            //OracleDataReader barras = cmd.ExecuteNonQuery();
            //result =  cmd.ExecuteNonQuery();
            //Insert Detalle
            int idPaletaAlmacenCont = 0;


            sqlPaleta = "UPDATE dbo.ALTV_PALETA " +
             " SET     CA_CAJA_BOLSA = " + altvPaleta1.caCajaBolsa + "," +
             "      FE_ACTUALIZA = GETDATE()," +
             "      ID_USU_ACT  = " + altvPaleta1.idUsuCrea +
             "      WHERE ID_PALETA = " + altvPaleta1.idPaleta;


            SqlCommand cmd = new SqlCommand(sqlPaleta, ConectString);
            cmd = new SqlCommand(sqlPaleta, ConectString);
            cmd.Transaction = transaction;
            resultado = "Exito: " + cmd.ExecuteNonQuery();


            transaction.Commit();
            recepcionPaleta.status = "OK";

        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
            recepcionPaleta.status = "NO_OK";

        }
        finally
        {
            ConectString.Close();
        }
        return recepcionPaleta;
    }

    public static RecepcionPaleta recepcionMigracion(AltmRecepcion altmRecepcion)
    {
        String resultado = "";
        int result = 0;
        int recepcionId = 0;
        string sql = "";
        string sqlPaleta = "";

        RecepcionPaleta recepcionPaleta = new RecepcionPaleta();

        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);



        sql = "INSERT INTO DBO.ALTM_RECEPCION(ID_ALMACEN, ID_TIPO_MOVIMIENTO, ID_MATERIAL, ID_TIPO_UNI_ALMACENA, FE_CREA, ID_USU_CREA, ID_ESTADO, CA_PALETAS, CA_CAJA_BOLSA, NU_LOTE, TXT_DOCUMENTO) " +

        "VALUES(" + altmRecepcion.idAlmacen + ", " + altmRecepcion.idTipoMovimiento + ", " + altmRecepcion.idMaterial + ", " + altmRecepcion.idTipoUniAlmacena + ",  GETDATE()," + altmRecepcion.idUsuCrea + ", " + altmRecepcion.idEstado + ", " + altmRecepcion.caPaletas + ", " + altmRecepcion.caCajaBolsa + ", '" + altmRecepcion.nuLote + "', '" + altmRecepcion.txtDocumento + "' );" +
        "SELECT CAST(scope_identity() AS int)";
        SqlCommand cmd = new SqlCommand(sql, ConectString);
        // Assign transaction object for a pending local transaction
        cmd.Transaction = transaction;


        try
        {
            //OracleDataReader barras = cmd.ExecuteNonQuery();
            //result =  cmd.ExecuteNonQuery();
            recepcionId = (int)cmd.ExecuteScalar();
            //Insert Detalle
            int idPaletaAlmacenCont = 0;
            int idPaletaAlmacen = getMaxIdPaleta_Por_Almacen(altmRecepcion.idAlmacen);
            for (int i = 1; i <= altmRecepcion.caPaletas; i++)
            {

                idPaletaAlmacenCont = idPaletaAlmacen + i;
                sqlPaleta = "INSERT INTO DBO.ALTV_PALETA(ID_ALMACEN, ID_PALETA_ALMACEN, CO_PALETA, ID_RECEPCION, FE_CREA, ID_USU_CREA, ID_ESTADO) " +
                              "VALUES(" + altmRecepcion.idAlmacen + "," + idPaletaAlmacenCont + ", RIGHT('" + altmRecepcion.coPaleta22 + "', 22) , " + recepcionId + ", GETDATE(), " + altmRecepcion.idUsuCrea + ", " + altmRecepcion.idEstado + ")";

                cmd = new SqlCommand(sqlPaleta, ConectString);
                cmd.Transaction = transaction;
                resultado = "Exito: " + cmd.ExecuteNonQuery();

            }
            transaction.Commit();
            recepcionPaleta.status = "OK";
            AltvPaleta altvPaleta = new AltvPaleta();
            altvPaleta.idRecepcion = recepcionId;
            AltmRecepcion recepcion = new AltmRecepcion();
            recepcion.idRecepcion = recepcionId;
            recepcionPaleta.recepcion = recepcion;
            recepcionPaleta.listAltvPaleta = paletasIdRecepcion(altvPaleta);
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
            recepcionPaleta.status = "NO_OK";
            recepcionPaleta.listAltvPaleta = null;
        }
        finally
        {
            ConectString.Close();
        }
        return recepcionPaleta;
    }

        public static RecepcionPaleta recepcionMigracionQR(AltmRecepcion altmRecepcion)
        {
            String resultado = "";
            int result = 0;
            int recepcionId = 0;
            string sql = "";
            string sqlPaleta = "";

            RecepcionPaleta recepcionPaleta = new RecepcionPaleta();

            SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
            ConectString.Open();
            SqlTransaction transaction;

            // Start a local transaction
            transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);



            sql = "INSERT INTO DBO.ALTM_RECEPCION(ID_ALMACEN, ID_TIPO_MOVIMIENTO, ID_MATERIAL, ID_TIPO_UNI_ALMACENA, FE_CREA, ID_USU_CREA, ID_ESTADO, CA_PALETAS, CA_CAJA_BOLSA, NU_LOTE, TXT_DOCUMENTO) " +

            "VALUES(" + altmRecepcion.idAlmacen + ", " + altmRecepcion.idTipoMovimiento + ", " + altmRecepcion.idMaterial + ", " + altmRecepcion.idTipoUniAlmacena + ",  GETDATE()," + altmRecepcion.idUsuCrea + ", " + altmRecepcion.idEstado + ", " + altmRecepcion.caPaletas + ", " + altmRecepcion.caCajaBolsa + ", '" + altmRecepcion.nuLote + "', '" + altmRecepcion.txtDocumento + "' );" +
            "SELECT CAST(scope_identity() AS int)";
            SqlCommand cmd = new SqlCommand(sql, ConectString);
            // Assign transaction object for a pending local transaction
            cmd.Transaction = transaction;


            try
            {
                //OracleDataReader barras = cmd.ExecuteNonQuery();
                //result =  cmd.ExecuteNonQuery();
                recepcionId = (int)cmd.ExecuteScalar();
                //Insert Detalle
                int idPaletaAlmacenCont = 0;
                int idPaletaAlmacen = getMaxIdPaleta_Por_Almacen(altmRecepcion.idAlmacen);
                for (int i = 1; i <= altmRecepcion.caPaletas; i++)
                {

                    idPaletaAlmacenCont = idPaletaAlmacen + i;
                    sqlPaleta = "INSERT INTO DBO.ALTV_PALETA(ID_ALMACEN, ID_PALETA_ALMACEN, CO_PALETA, ID_RECEPCION, FE_CREA, ID_USU_CREA, ID_ESTADO) " +
                                  "VALUES(" + altmRecepcion.idAlmacen + "," + idPaletaAlmacenCont + ", RIGHT('" + altmRecepcion.coPaleta22 + "', 22) , " + recepcionId + ", GETDATE(), " + altmRecepcion.idUsuCrea + ", " + altmRecepcion.idEstado + ")";

                    cmd = new SqlCommand(sqlPaleta, ConectString);
                    cmd.Transaction = transaction;
                    resultado = "Exito: " + cmd.ExecuteNonQuery();

                }
                transaction.Commit();
                recepcionPaleta.status = "OK";
                AltvPaleta altvPaleta = new AltvPaleta();
                altvPaleta.idRecepcion = recepcionId;
                AltmRecepcion recepcion = new AltmRecepcion();
                recepcion.idRecepcion = recepcionId;
                recepcionPaleta.recepcion = recepcion;
                recepcionPaleta.listAltvPaleta = paletasIdRecepcion(altvPaleta);
            }
            catch (Exception e)
            {
                transaction.Rollback();
                resultado = "";
                recepcionPaleta.status = "NO_OK";
                recepcionPaleta.listAltvPaleta = null;
            }
            finally
            {
                ConectString.Close();
            }
            return recepcionPaleta;
        }

        public static int getMaxIdPaleta_Por_Almacen(int idAlmacen)
    {

        String data = "";
        int registro = 0;
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        SqlCommand cmd;

        try
        {
            //Cantidad Ingreso
            data = " SELECT ISNULL(MAX(L.ID_PALETA_ALMACEN),0)  FROM dbo.ALTV_PALETA L" +
            " WHERE L.ID_ALMACEN = " + idAlmacen;

            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            registro = Convert.ToInt32(cmd.ExecuteScalar());


            // transaction.Commit();
        }
        catch (Exception e)
        {
            ;
            //transaction.Rollback();
            registro = 0;
        }
        finally
        {
            ConectString.Close();
        }
        return registro;
    }



    public static RecepcionPaleta getRecepcionPaleta(AltmRecepcion altmRecepcion)
    {
        String resultado = "";
        int result = 0;
        int recepcionId = 0;
        string sql = "";
        string sqlPaleta = "";

        RecepcionPaleta recepcionPaleta = new RecepcionPaleta();

        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);


        try
        {

            recepcionPaleta.status = "OK";
            AltvPaleta altvPaleta = new AltvPaleta();
            altvPaleta.idRecepcion = altmRecepcion.idRecepcion;
            altvPaleta.idPaleta = altmRecepcion.idPaletasConsulta;
            recepcionPaleta.recepcion = getRecepcionId(altmRecepcion);
            recepcionPaleta.listAltvPaleta = paletasIdRecepcionId(altvPaleta);
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
            recepcionPaleta.status = "NO_OK";
            recepcionPaleta.listAltvPaleta = null;
        }
        finally
        {
            ConectString.Close();
        }
        return recepcionPaleta;
    }



    public static AltmRecepcion getRecepcionId(AltmRecepcion re)
    {
        String resultado = "";


        AltmRecepcion recepcion = new AltmRecepcion();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        StringBuilder sqlText = new StringBuilder();
        try
        {


            sqlText.Append(" SELECT ");
            sqlText.Append(" R.ID_RECEPCION, ");
            sqlText.Append(" R.ID_ALMACEN, ");
            sqlText.Append(" R.ID_TIPO_MOVIMIENTO, ");
            sqlText.Append(" R.ID_MATERIAL, ");
            sqlText.Append(" R.ID_TIPO_UNI_ALMACENA, ");
            sqlText.Append(" R.ID_USU_CREA, ");
            sqlText.Append(" R.ID_ESTADO, ");
            sqlText.Append(" R.CA_PALETAS   CA_PALETAS, ");
            sqlText.Append(" ISNULL(P.CA_CAJA_BOLSA, R.CA_CAJA_BOLSA)  CA_CAJA_BOLSA, ");
            sqlText.Append(" R.NU_LOTE");
            sqlText.Append(" FROM dbo.ALTM_RECEPCION R , dbo.ALTV_PALETA P ");
            sqlText.Append("  WHERE R.ID_RECEPCION = P.ID_RECEPCION AND R.ID_RECEPCION = " + re.idRecepcion + " AND P.ID_PALETA= " + re.idPaletasConsulta);

            SqlCommand cmd = new SqlCommand(sqlText.ToString(), ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                recepcion = new AltmRecepcion();


                recepcion.idRecepcion = Int32.Parse(barras.GetValue(0).ToString());
                recepcion.idAlmacen = Int32.Parse(barras.GetValue(1).ToString());
                recepcion.idTipoMovimiento = Int32.Parse(barras.GetValue(2).ToString());
                recepcion.idMaterial = Int32.Parse(barras.GetValue(3).ToString());
                recepcion.idTipoUniAlmacena = Int32.Parse(barras.GetValue(4).ToString());
                recepcion.idUsuCrea = Int32.Parse(barras.GetValue(5).ToString());
                recepcion.idEstado = Int32.Parse(barras.GetValue(6).ToString());
                recepcion.caPaletas = Int32.Parse(barras.GetValue(7).ToString());
                recepcion.caCajaBolsa = Int32.Parse(barras.GetValue(8).ToString());
                recepcion.nuLote = barras.GetValue(9).ToString();

                break;

            }
        }
        catch (Exception e)
        {
            recepcion = null;
        }
        finally
        {
            ConectString.Close();
        }
        return recepcion;
    }

        public static AltmRecepcion getRecepcionCoPaleta(AltmRecepcion re)
        {
            String resultado = "";


            AltmRecepcion recepcion = new AltmRecepcion();
            SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
            ConectString.Open();
            StringBuilder sqlText = new StringBuilder();
            try
            {


                sqlText.Append(" SELECT ");
                sqlText.Append(" R.ID_RECEPCION, ");
                sqlText.Append(" R.ID_ALMACEN, ");
                sqlText.Append(" R.ID_TIPO_MOVIMIENTO, ");
                sqlText.Append(" R.ID_MATERIAL, ");
                sqlText.Append(" R.ID_TIPO_UNI_ALMACENA, ");
                sqlText.Append(" R.ID_USU_CREA, ");
                sqlText.Append(" R.ID_ESTADO, ");
                sqlText.Append(" R.CA_PALETAS   CA_PALETAS, ");
                sqlText.Append(" ISNULL(P.CA_CAJA_BOLSA, R.CA_CAJA_BOLSA)  CA_CAJA_BOLSA, ");
                sqlText.Append(" R.NU_LOTE,");
                sqlText.Append(" P.ID_PALETA");
                sqlText.Append(" FROM dbo.ALTM_RECEPCION R , dbo.ALTV_PALETA P ");
                sqlText.Append("  WHERE R.ID_RECEPCION = P.ID_RECEPCION AND ");
                sqlText.Append("          P.CO_PALETA = '" + re.coPaleta + "'");
                 
                SqlCommand cmd = new SqlCommand(sqlText.ToString(), ConectString);

                SqlDataReader barras = cmd.ExecuteReader();

                while (barras.Read())
                {
                    recepcion = new AltmRecepcion();


                    recepcion.idRecepcion = Int32.Parse(barras.GetValue(0).ToString());
                    recepcion.idAlmacen = Int32.Parse(barras.GetValue(1).ToString());
                    recepcion.idTipoMovimiento = Int32.Parse(barras.GetValue(2).ToString());
                    recepcion.idMaterial = Int32.Parse(barras.GetValue(3).ToString());
                    recepcion.idTipoUniAlmacena = Int32.Parse(barras.GetValue(4).ToString());
                    recepcion.idUsuCrea = Int32.Parse(barras.GetValue(5).ToString());
                    recepcion.idEstado = Int32.Parse(barras.GetValue(6).ToString());
                    recepcion.caPaletas = Int32.Parse(barras.GetValue(7).ToString());
                    recepcion.caCajaBolsa = Int32.Parse(barras.GetValue(8).ToString());
                    recepcion.nuLote = barras.GetValue(9).ToString();
                    recepcion.idPaleta = Int32.Parse(barras.GetValue(10).ToString());

                    break;

                }
            }
            catch (Exception e)
            {
                recepcion = null;
            }
            finally
            {
                ConectString.Close();
            }
            return recepcion;
        }

        public static string validRegistroBloque(AltmLayout altmLayout)
    {
        String resultado = "";
        String data = "";
        int result = 0;
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        SqlCommand cmd;
        int registro = 0;

        try
        {
            //Cantidad Ingreso
            data = " SELECT ISNULL(MAX(L.ID_LAYAOUT),0)  FROM dbo.ALTM_LAYOUT L" +
            " WHERE L.CO_LAYAOUT = '" + altmLayout.coLayaout + "' AND L.ID_ALMACEN = " + altmLayout.idAlmacen;

            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            registro = Convert.ToInt32(cmd.ExecuteScalar());

            //Valida DNI Registrado
            if (registro >= 1)
            {
                resultado = "El bloque ya existe en Almacen";
                return resultado;
            }
            else
            {
                resultado = "OK";
            }
            // transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
        }
        finally
        {
            ConectString.Close();
        }
        return resultado;
    }

        public static string validaBloqueCarril(AltmLayout altmLayoutOld, AltmLayout altmLayoutNew)
        {
            String resultado = "";


            if (altmLayoutNew.caBloques < altmLayoutOld.caBloques)
            {
                resultado = "Cantidad de bloques menor del Registro anterior";
                return resultado;
            }

            if (altmLayoutNew.caNiveles < altmLayoutOld.caNiveles)
            {
                resultado = "Cantidad de Niveles menor del Registro anterior";
                return resultado;
            }

            if (altmLayoutNew.caCarriles < altmLayoutOld.caCarriles)
            {
                resultado = "Cantidad de Carriles menor del Registro anterior";
                return resultado;
            }
            resultado = "OK";
            return resultado;
        }


        public static string layout(AltmLayout altmLayout)
    {
        String resultado = "";
        int result = 0;
        string sql = "";
        string sqlAltmLayout = "";

        string sqlAltmBloque = "";
        string sqlAltrCarril = "";
        string sqlAltrNivel = "";
        string sqlAltrLayut = "";

        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));

        ConectString.Open();
        SqlTransaction transaction;

        //SqlCommand cmd = null;
        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        resultado = validRegistroBloque(altmLayout);

        if (resultado != "OK")
        {
            return resultado;
        }

        //ID_LAYAOUT,
        int intLayout = 0;
        sqlAltmLayout = "INSERT INTO     DBO.ALTM_LAYOUT(CO_LAYAOUT, ID_ALMACEN, CA_BLOQUES, CA_NIVELES, CA_CARRILES, NU_CANT_MAX, FE_CREA, ID_USU_CREA, ID_ESTADO)" +
         "VALUES('" + altmLayout.coLayaout + "', " + altmLayout.idAlmacen + ", " + altmLayout.caBloques + ", " + altmLayout.caNiveles + ", " + altmLayout.caCarriles + ", " + altmLayout.nuCantMax + ", GETDATE(), " + altmLayout.idUsuCrea + ", " + altmLayout.idEstado + "); " +
            "SELECT CAST(scope_identity() AS int)";
        SqlCommand cmd = new SqlCommand(sqlAltmLayout, ConectString);
        cmd.Transaction = transaction;


        try
        {

            intLayout = (int)cmd.ExecuteScalar();
            //ID_BLOQUE,
            string data = " SELECT ISNULL(MAX(ID_BLOQUE),0)   FROM dbo.ALTR_BLOQUE WHERE CO_BLOQUE = '" + altmLayout.coLayaout + "'";
            cmd = new SqlCommand(data, ConectString);
            //  cmd.Parameters.AddWithValue("param", i);
            cmd.Transaction = transaction;
            int idBloque = Convert.ToInt32(cmd.ExecuteScalar());

            if (idBloque == 0)
            {

                data = "  SELECT ISNULL(MAX(ID_BLOQUE),0) +1  FROM dbo.ALTR_BLOQUE";
                cmd = new SqlCommand(data, ConectString);
                //  cmd.Parameters.AddWithValue("param", i);
                cmd.Transaction = transaction;
                idBloque = Convert.ToInt32(cmd.ExecuteScalar());

                sqlAltmBloque = "INSERT INTO     DBO.ALTR_BLOQUE( ID_BLOQUE,  CO_BLOQUE, DE_BLOQUE, FE_CREA, ID_USU_CREA, ID_ESTADO )" +
                 "  VALUES(" + idBloque + ",'" + altmLayout.coLayaout + "', '" + altmLayout.coLayaout + "', GETDATE(), " + altmLayout.idUsuCrea + ", " + altmLayout.idEstado + ")";

                cmd = new SqlCommand(sqlAltmBloque, ConectString);
                cmd.Transaction = transaction;
                cmd.ExecuteNonQuery();
                resultado = "OK";
            }


            //ID_CARRIL,	
            for (int j = 1; j <= altmLayout.caCarriles; j++)
            {
                data = " SELECT COUNT(1) FROM dbo.ALTR_CARRIL WHERE ID_CARRIL = " + j;
                cmd = new SqlCommand(data, ConectString);
                // cmd.Parameters.AddWithValue("@param", j);
                cmd.Transaction = transaction;
                int cant = Convert.ToInt32(cmd.ExecuteScalar());

                if (cant == 0)
                {
                    sqlAltrCarril = "INSERT INTO     DBO.ALTR_CARRIL (ID_CARRIL,CO_CARRIL, FE_CREA, ID_USU_CREA, ID_ESTADO )" +
                   "VALUES(" + j + ",  " + j + ", GETDATE(), " + altmLayout.idUsuCrea + ", " + altmLayout.idEstado + ")";

                    cmd = new SqlCommand(sqlAltrCarril, ConectString);
                    cmd.Transaction = transaction;
                    cmd.ExecuteNonQuery();
                    resultado = "OK";

                }
            }

            //ID_NIVEL,
            for (int j = 1; j <= altmLayout.caNiveles; j++)
            {
                data = " SELECT COUNT(1) FROM dbo.ALTR_NIVEL WHERE CO_NIVEL = " + j;
                cmd = new SqlCommand(data, ConectString);
                //cmd.Parameters.AddWithValue("@param", j);
                cmd.Transaction = transaction;
                int cant = Convert.ToInt32(cmd.ExecuteScalar());

                if (cant == 0)
                {
                    sqlAltrNivel = "INSERT INTO     DBO.ALTR_NIVEL (ID_NIVEL,CO_NIVEL, FE_CREA, ID_USU_CREA, ID_ESTADO )	" +
                   " VALUES(" + j + "," + j + ", GETDATE(), " + altmLayout.idUsuCrea + ", " + altmLayout.idEstado + ")";

                    cmd = new SqlCommand(sqlAltrNivel, ConectString);
                    cmd.Transaction = transaction;
                    cmd.ExecuteNonQuery();
                    resultado = "OK";

                }
            }

            //ID_UBICACION_FISICA,
            string coUbicacionFisica = "";
            string b_ = "";
            string c_ = "";
            string n_ = "";
            for (int c = 1; c <= altmLayout.caCarriles; c++)
            {
                for (int n = 1; n <= altmLayout.caNiveles; n++)
                {
                    b_ = "" + altmLayout.coLayaout;
                    c_ = "" + c;
                    n_ = "" + n;

                    coUbicacionFisica = b_ + completarCaractere(c_, 2, "0", "I") + completarCaractere(n_, 2, "0", "I");

                    sqlAltrLayut = "INSERT INTO DBO.ALTM_UBICACION_FISICA(CO_UBICACION_FISICA, ID_LAYAOUT, ID_BLOQUE, ID_NIVEL, ID_CARRIL, NU_CANT_MAX, IN_FULL, FE_CREA, ID_USU_CREA, ID_ESTADO)" +
                     "VALUES('" + coUbicacionFisica + "' , " + intLayout + ", " + idBloque + " , " + n + " , " + c + " , " + altmLayout.nuCantMax + " , 0 ,  GETDATE(), " + altmLayout.idUsuCrea + " , " + altmLayout.idEstado + ")";

                    cmd = new SqlCommand(sqlAltrLayut, ConectString);
                    cmd.Transaction = transaction;
                    cmd.ExecuteNonQuery();
                    resultado = "OK";

                }
            }

            cmd.Transaction = transaction;

            transaction.Commit();
            resultado = "OK";
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
        }
        finally
        {
            ConectString.Close();
        }
        return resultado;
    }

     public static string updateLayout(AltmLayout altmLayout)
        {
            String resultado = "";
            int result = 0;
            string sql = "";
            string sqlAltmLayout = "";

            string sqlAltmBloque = "";
            string sqlAltrCarril = "";
            string sqlAltrNivel = "";
            string sqlAltrLayut = "";

            SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));

            ConectString.Open();
            SqlTransaction transaction;

            //SqlCommand cmd = null;
            // Start a local transaction
            transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

            //Carriles y Niveles iniciales
            AltmLayout altmLayoutOld = layoutUnico(altmLayout);

            
            resultado = validaBloqueCarril(altmLayoutOld, altmLayout);

            if (resultado != "OK")
            {
                return resultado;
            }
            
 

            sqlAltmLayout = "UPDATE dbo.ALTM_LAYOUT " +
             " SET     CA_NIVELES = " + altmLayout.caNiveles + "," +
             "      CA_CARRILES  = " + altmLayout.caCarriles + "," +
             "      NU_CANT_MAX  = " + altmLayout.nuCantMax + "," +
             "      FE_ACTUALIZA = GETDATE()," +
             "      ID_USU_ACT  = " + altmLayout.idUsuCrea +
             "      WHERE ID_LAYAOUT = " + altmLayout.idLayaout;

            SqlCommand cmd = new SqlCommand(sqlAltmLayout, ConectString);

            cmd.Transaction = transaction;

            try
            {

               
                cmd.ExecuteNonQuery();

                String data;

                //ID_CARRIL,	
                for (int j = 1; j <= altmLayout.caCarriles; j++)
                {
                    data = " SELECT COUNT(1) FROM dbo.ALTR_CARRIL WHERE ID_CARRIL = " + j;
                    cmd = new SqlCommand(data, ConectString);
                    // cmd.Parameters.AddWithValue("@param", j);
                    cmd.Transaction = transaction;
                    int cant = Convert.ToInt32(cmd.ExecuteScalar());

                    if (cant == 0)
                    {
                        sqlAltrCarril = "INSERT INTO     DBO.ALTR_CARRIL (ID_CARRIL,CO_CARRIL, FE_CREA, ID_USU_CREA, ID_ESTADO )" +
                       "VALUES(" + j + ",  " + j + ", GETDATE(), " + altmLayout.idUsuCrea + ", " + altmLayout.idEstado + ")";

                        cmd = new SqlCommand(sqlAltrCarril, ConectString);
                        cmd.Transaction = transaction;
                        cmd.ExecuteNonQuery();
                        resultado = "OK";

                    }
                }

                //ID_NIVEL,
                for (int j = 1; j <= altmLayout.caNiveles; j++)
                {
                    data = " SELECT COUNT(1) FROM dbo.ALTR_NIVEL WHERE CO_NIVEL = " + j;
                    cmd = new SqlCommand(data, ConectString);
                    //cmd.Parameters.AddWithValue("@param", j);
                    cmd.Transaction = transaction;
                    int cant = Convert.ToInt32(cmd.ExecuteScalar());

                    if (cant == 0)
                    {
                        sqlAltrNivel = "INSERT INTO     DBO.ALTR_NIVEL (ID_NIVEL,CO_NIVEL, FE_CREA, ID_USU_CREA, ID_ESTADO )	" +
                       " VALUES(" + j + "," + j + ", GETDATE(), " + altmLayout.idUsuCrea + ", " + altmLayout.idEstado + ")";

                        cmd = new SqlCommand(sqlAltrNivel, ConectString);
                        cmd.Transaction = transaction;
                        cmd.ExecuteNonQuery();
                        resultado = "OK";

                    }
                }

                //IdBloque
                //ID_BLOQUE,
                string dataBloque = " SELECT ISNULL(MAX(ID_BLOQUE),0)   FROM dbo.ALTR_BLOQUE WHERE CO_BLOQUE = '" + altmLayout.coLayaout + "'";
                cmd = new SqlCommand(dataBloque, ConectString);
                //  cmd.Parameters.AddWithValue("param", i);
                cmd.Transaction = transaction;
                int idBloque = Convert.ToInt32(cmd.ExecuteScalar());

               

                //ID_UBICACION_FISICA,
                string coUbicacionFisica = "";
                string b_ = "";
                string c_ = "";
                string n_ = "";
                for (int c =  1; c <= altmLayout.caCarriles; c++)
                {
                    for (int n = 1; n <= altmLayout.caNiveles; n++)
                    {
                        b_ = "" + altmLayout.coLayaout;
                        c_ = "" + c;
                        n_ = "" + n;

                        coUbicacionFisica = b_ + completarCaractere(c_, 2, "0", "I") + completarCaractere(n_, 2, "0", "I");

                        string dataUbicacion = " SELECT COUNT(1)   FROM dbo.ALTM_UBICACION_FISICA WHERE CO_UBICACION_FISICA = '" + coUbicacionFisica + "' AND ID_LAYAOUT = " + altmLayout.idLayaout;
                        cmd = new SqlCommand(dataUbicacion, ConectString);
                        //  cmd.Parameters.AddWithValue("param", i);
                        cmd.Transaction = transaction;
                        int caUbicaciones = Convert.ToInt32(cmd.ExecuteScalar());

                        if (caUbicaciones == 0 ) {

                            sqlAltrLayut = "INSERT INTO DBO.ALTM_UBICACION_FISICA(CO_UBICACION_FISICA, ID_LAYAOUT, ID_BLOQUE, ID_NIVEL, ID_CARRIL, NU_CANT_MAX, IN_FULL, FE_CREA, ID_USU_CREA, ID_ESTADO)" +
                             "VALUES('" + coUbicacionFisica + "' , " + altmLayout.idLayaout + ", " + idBloque + " , " + n + " , " + c + " , " + altmLayout.nuCantMax + " , 0 ,  GETDATE(), " + altmLayout.idUsuCrea + " , " + altmLayout.idEstado + ")";

                            cmd = new SqlCommand(sqlAltrLayut, ConectString);
                            cmd.Transaction = transaction;
                            cmd.ExecuteNonQuery();
                        }

                        resultado = "OK";

                    }
                }

                cmd.Transaction = transaction;

                transaction.Commit();
                resultado = "OK";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                resultado = "";
            }
            finally
            {
                ConectString.Close();
            }
            return resultado;
        }
        public static string actualizaEstadoUbicacion(AltmUbicacionFisica altmUbicacionFisica)
    {
        String resultado = "";
        string sqlUsuario = "";
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        //ID_STOCK_MOVIMIENTO,
        SqlCommand cmd = null;
        if (altmUbicacionFisica.idUbicacionFisica != 0)
        {


            sqlUsuario = "UPDATE dbo.ALTM_UBICACION_FISICA " +
                " SET     ID_ESTADO = " + altmUbicacionFisica.idEstado + "," +
                "      FE_ACTUALIZA = GETDATE()," +
                "      ID_USU_ACT  = " + altmUbicacionFisica.idUsuAct +
                "      WHERE ID_UBICACION_FISICA = " + altmUbicacionFisica.idUbicacionFisica;
            cmd = new SqlCommand(sqlUsuario, ConectString);
            // Assign transaction object for a pending local transaction
        }
        cmd.Transaction = transaction;
        try
        {
            cmd = new SqlCommand(sqlUsuario, ConectString);
            cmd.Transaction = transaction;
            cmd.ExecuteNonQuery();
            resultado = "OK";

            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
        }
        finally
        {
            ConectString.Close();
        }
        return resultado;
    }

    public static string actualizaCantMaxPaletaUbicacion(AltmUbicacionFisica altmUbicacionFisica)
    {
        String resultado = "";
        string sqlUsuario = "";
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        //ID_STOCK_MOVIMIENTO,

        // cmd.Transaction = transaction;
        try
        {
            SqlCommand cmd = null;
            if (altmUbicacionFisica.idUbicacionFisica != 0)
            {


                sqlUsuario = "UPDATE dbo.ALTM_UBICACION_FISICA " +
                    " SET     NU_CANT_MAX = " + altmUbicacionFisica.nuCantMax + "," +
                    "      FE_ACTUALIZA = GETDATE()," +
                    "      ID_USU_ACT  = " + altmUbicacionFisica.idUsuAct +
                    "      WHERE ID_UBICACION_FISICA = " + altmUbicacionFisica.idUbicacionFisica;
                //  cmd = new SqlCommand(sqlUsuario, ConectString);
                // Assign transaction object for a pending local transaction
            }

            cmd = new SqlCommand(sqlUsuario, ConectString);
            cmd.Transaction = transaction;
            cmd.ExecuteNonQuery();
            resultado = "OK";

            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
        }
        finally
        {
            ConectString.Close();
        }

        return resultado;
    }


    public static List<LayoutCeldas> layoutCelda(AltmLayout layout)
    {
        String resultado = "";
        String sqlAlmacen = "";
        String sqlUbigeo = "";
        List<LayoutCeldas> listLayoutCeldas = new List<LayoutCeldas>();
        LayoutCeldas layoutCeldas = new LayoutCeldas();


        List<AltmAlmacen> listAltmAlmacen = new List<AltmAlmacen>();
        AltmAlmacen altmAlmacen = new AltmAlmacen();

        List<AltmLayout> listAltmLayout = new List<AltmLayout>();
        AltmLayout altmLayout = new AltmLayout();


        //List<UbicacionStock> listUbicacionStock = new List<UbicacionStock>();
        UbicacionStock ubicacionStock = new UbicacionStock();


        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        sqlAlmacen = "SELECT L.ID_LAYAOUT, L.CO_LAYAOUT, CONCAT( A.DE_ALMACEN, ' Layout ', L.CO_LAYAOUT),  L.ID_ALMACEN, L.CA_BLOQUES,  L.CA_NIVELES, L.CA_CARRILES, L.NU_CANT_MAX, L.ID_ESTADO  FROM dbo.ALTM_LAYOUT L, dbo.ALTM_ALMACEN A " +
            "WHERE  L.ID_ALMACEN = A.ID_ALMACEN AND L.ID_LAYAOUT = " + layout.idLayaout + " AND L.ID_ALMACEN = " + layout.idAlmacen;

        SqlCommand cmd = new SqlCommand(sqlAlmacen, ConectString);
        cmd.Transaction = transaction;

        try
        {

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                altmLayout = new AltmLayout();

                altmLayout.idLayaout = Int32.Parse(barras.GetValue(0).ToString());
                altmLayout.coLayaout = barras.GetValue(1).ToString();
                altmLayout.deLayaout = barras.GetValue(2).ToString();
                altmLayout.idAlmacen = Int32.Parse(barras.GetValue(3).ToString());
                altmLayout.caBloques = Int32.Parse(barras.GetValue(4).ToString());
                altmLayout.caNiveles = Int32.Parse(barras.GetValue(5).ToString());
                altmLayout.caCarriles = Int32.Parse(barras.GetValue(6).ToString());
                altmLayout.nuCantMax = Int32.Parse(barras.GetValue(7).ToString());
                altmLayout.idEstado = Int32.Parse(barras.GetValue(8).ToString());

                break;
            }

        }
        catch (Exception e)
        {
            listLayoutCeldas = null;
        }
        finally
        {
            ConectString.Close();
        }

        try
        {
            ConectString.Open();

            sqlUbigeo = " SELECT UF.ID_UBICACION_FISICA, UF.CO_UBICACION_FISICA, UF.ID_LAYAOUT, UF.ID_BLOQUE, UF.ID_NIVEL, UF.ID_CARRIL, UF.ID_ESTADO, ISNULL(ST.CA_STOCK,0)  CA_STOCK, UF.IN_FULL, UF.NU_CANT_MAX  " +
                "FROM  dbo.ALTM_LAYOUT  LA INNER JOIN  dbo.ALTM_UBICACION_FISICA UF ON   LA.ID_LAYAOUT = UF.ID_LAYAOUT " +
                " left outer join  dbo.ALTM_STOCK ST ON  UF.ID_UBICACION_FISICA = ST.ID_UBICACION_FISICA " +
                " WHERE  LA.ID_ALMACEN = " + layout.idAlmacen + " AND  LA.ID_LAYAOUT = " + +layout.idLayaout +
                   " ORDER BY ID_UBICACION_FISICA ASC";

            cmd = new SqlCommand(sqlUbigeo, ConectString);
            cmd.Transaction = transaction;
            SqlDataReader comando = cmd.ExecuteReader();

            Dictionary<string, UbicacionStock> dict = new Dictionary<string, UbicacionStock>();
            while (comando.Read())
            {
                ubicacionStock = new UbicacionStock();

                ubicacionStock.idUbicacionFisica = Int32.Parse(comando.GetValue(0).ToString());
                ubicacionStock.coUbicacionFisica = comando.GetValue(1).ToString();

                ubicacionStock.idLayaout = Int32.Parse(comando.GetValue(2).ToString());
                ubicacionStock.idBloque = Int32.Parse(comando.GetValue(3).ToString());
                ubicacionStock.idNivel = comando.GetValue(4).ToString();
                ubicacionStock.idCarril = comando.GetValue(5).ToString();

                ubicacionStock.idEstado = Int32.Parse(comando.GetValue(6).ToString());
                ubicacionStock.caStock = Int32.Parse(comando.GetValue(7).ToString());
                ubicacionStock.inFull = comando.GetValue(8).ToString();
                ubicacionStock.nuCantMax = Int32.Parse(comando.GetValue(9).ToString());
                //listUbicacionStock.Add(ubicacionStock);

                dict.Add(ubicacionStock.coUbicacionFisica, ubicacionStock);
            }
            Hashtable ht = new Hashtable(dict);
            //map<string, UbicacionStock> map = new HashMap<string, UbicacionStock>();


            //
            String debloque = altmLayout.deLayaout;
            int caNiveles = altmLayout.caNiveles;
            int caCarriles = altmLayout.caCarriles;

            int caStock = 0;

            layoutCeldas = new LayoutCeldas();
            layoutCeldas.color = "#FFE699";
            layoutCeldas.colorText = "#000000";
            layoutCeldas.cols = 2;
            layoutCeldas.rows = 2;
            layoutCeldas.text = debloque;
            listLayoutCeldas.Add(layoutCeldas);


            layoutCeldas = new LayoutCeldas();
            layoutCeldas.color = "#ACB9CA";
            layoutCeldas.colorText = "#FFFFFF";
            layoutCeldas.cols = caNiveles;
            layoutCeldas.rows = 1;
            layoutCeldas.text = "NIVEL ";
            listLayoutCeldas.Add(layoutCeldas);

            for (int n = 1; n <= altmLayout.caNiveles; n++)
            {
                layoutCeldas = new LayoutCeldas();
                layoutCeldas.color = "#D9E1F2";
                layoutCeldas.colorText = "#000000";
                layoutCeldas.cols = 1;
                layoutCeldas.rows = 1;
                layoutCeldas.text = completarCaractere(n + "", 2, "0", "I");
                listLayoutCeldas.Add(layoutCeldas);
            }

            layoutCeldas = new LayoutCeldas();
            layoutCeldas.color = "#4472C4";
            layoutCeldas.colorText = "#000000";
            layoutCeldas.cols = 1;
            layoutCeldas.rows = caCarriles;
            layoutCeldas.text = "CARRIL ";
            listLayoutCeldas.Add(layoutCeldas);

            string coUbicacion = "";
            UbicacionStock ubicacionStockRecuperado = new UbicacionStock();
            for (int c = 1; c <= altmLayout.caCarriles; c++)
            {
                layoutCeldas = new LayoutCeldas();
                layoutCeldas.color = "#ACB9CA";
                layoutCeldas.colorText = "#000000";
                layoutCeldas.cols = 1;
                layoutCeldas.rows = 1;
                layoutCeldas.text = completarCaractere(c + "", 2, "0", "I"); ;
                listLayoutCeldas.Add(layoutCeldas);
                for (int n = 1; n <= altmLayout.caNiveles; n++)
                {
                    layoutCeldas = new LayoutCeldas();
                    layoutCeldas.color = "#F4B084";
                    layoutCeldas.colorText = "#FFFFFF";
                    layoutCeldas.cols = 1;
                    layoutCeldas.rows = 1;



                    caStock = 0;

                    coUbicacion = altmLayout.coLayaout + completarCaractere(c + "", 2, "0", "I") + completarCaractere(n + "", 2, "0", "I");


                    ubicacionStockRecuperado = dict[coUbicacion];


                    if (ubicacionStockRecuperado.idEstado == 0)
                    {
                        layoutCeldas.color = "#000000";
                        layoutCeldas.text = "";
                    }
                    else if (ubicacionStockRecuperado.idEstado == 1)
                    {
                        if (ubicacionStockRecuperado.nuCantMax <= ubicacionStockRecuperado.caStock)
                        {
                            layoutCeldas.color = "#FF3355";
                        }
                        else
                        {
                            layoutCeldas.color = "#F4B084";
                        }
                        layoutCeldas.text = ubicacionStockRecuperado.caStock + "";
                    }
                    else
                    {
                        layoutCeldas.color = "#F4B084";

                        layoutCeldas.text = ubicacionStockRecuperado.caStock + "";
                    }


                    /*
                    foreach (UbicacionStock ubicacion in listUbicacionStock)
                    {
                        if (ubicacion.idNivel ==n && ubicacion.idCarril == c)
                        {
                            caStock =  ubicacion.caStock;

                            break;
                        }
                    }
                    */

                    listLayoutCeldas.Add(layoutCeldas);
                }
            }


        }
        catch (Exception e)
        {
            listLayoutCeldas = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listLayoutCeldas;
    }

        public static ValidaMovimientoStockStatust getPaletaXCodPaletaAlmacen(ValidaMovimientoStock movimiento)
        {
            int intPaleta = 0;
            int intUbicacion = 0;
            ValidaMovimientoStock validaMovimientoStock = null;
            ValidaMovimientoStockStatust validaMovimientoStockStatust = null;


            SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
            ConectString.Open();
            SqlTransaction transaction;

            // Start a local transaction
            transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

            //  string[] dato = movimiento.coPaleta.Split("-");


            string data = "";
            SqlCommand cmd;
            validaMovimientoStockStatust = new ValidaMovimientoStockStatust();
            validaMovimientoStock = new ValidaMovimientoStock();

            try
            {
                data = " SELECT ISNULL(MAX(ID_PALETA),0)  FROM dbo.ALTV_PALETA  WHERE CO_PALETA = '" + movimiento.coPaleta + "' AND ID_ALMACEN =" + movimiento.idAlmacen;
                cmd = new SqlCommand(data, ConectString);
                cmd.Transaction = transaction;
                intPaleta = Convert.ToInt32(cmd.ExecuteScalar());
                validaMovimientoStock.idPaleta = "" + intPaleta;
                //Stock
                if (intPaleta > 0)
                {
                    validaMovimientoStockStatust.status = "OK";
                    validaMovimientoStockStatust.descripcion = "La Paleta " + movimiento.coPaleta + " existe";
                }
                else
                {
                    validaMovimientoStockStatust.status = "NO_OK";
                    validaMovimientoStockStatust.descripcion = "La Paleta " + movimiento.coPaleta + " no existe";
                }


            }
            catch (Exception e)
            {
                intPaleta = 0;
                validaMovimientoStockStatust.status = "NO_OK";
                validaMovimientoStockStatust.descripcion = "La Paleta " + movimiento.coPaleta + " no existe";
            }
            finally
            {
                ConectString.Close();
            }
            //return intPaleta;
            

            validaMovimientoStockStatust.validaMovimientoStock = validaMovimientoStock;
            return validaMovimientoStockStatust;


        }

        public static ValidaMovimientoStockStatust getPaletaXCodPaleta(ValidaMovimientoStock movimiento)
    {
        int intPaleta = 0;
        int intUbicacion = 0;
        ValidaMovimientoStock validaMovimientoStock = null;
        ValidaMovimientoStockStatust validaMovimientoStockStatust = null;


        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        //  string[] dato = movimiento.coPaleta.Split("-");


        string data = "";
        SqlCommand cmd;
        validaMovimientoStockStatust = new ValidaMovimientoStockStatust();
        validaMovimientoStock = new ValidaMovimientoStock();

        try
        {
            data = " SELECT ISNULL(MAX(ID_PALETA),0)  FROM dbo.ALTV_PALETA  WHERE CO_PALETA = '" + movimiento.coPaleta + "' AND ID_ALMACEN =" + movimiento.idAlmacen;
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            intPaleta = Convert.ToInt32(cmd.ExecuteScalar());
            validaMovimientoStock.idPaleta = "" + intPaleta;
            //Stock
            if (intPaleta > 0)
            {

                if (movimiento.coTipoMovimiento == "01")//Ingreso
                {
                    data = " SELECT ISNULL(MAX(ID_PALETA),0)  FROM dbo.ALTV_STOCK_MOVIMIENTO  WHERE ID_PALETA = " + intPaleta;
                    cmd = new SqlCommand(data, ConectString);
                    cmd.Transaction = transaction;
                    intUbicacion = Convert.ToInt32(cmd.ExecuteScalar());

                    if (intUbicacion > 0)
                    {
                        validaMovimientoStockStatust.status = "NO_OK";
                        validaMovimientoStockStatust.descripcion = "La Paleta " + movimiento.coPaleta + " ya Fue Ingresado al Almacen";
                    }
                    else
                    {
                        validaMovimientoStockStatust.status = "OK";
                        validaMovimientoStockStatust.descripcion = "La Paleta " + movimiento.coPaleta + " se puede asignar";
                    }

                }
                else
                {
                    if (movimiento.coTipoMovimiento == "05")//Ingreso
                    {
                        validaMovimientoStock.idPaleta = "" + intPaleta;
                        validaMovimientoStock.idAlmacen = movimiento.idAlmacen;
                        validaMovimientoStock.coTipoMovimiento = movimiento.coTipoMovimiento;
                        validaMovimientoStockStatust = getMovimientoStatusReingresoPaleta(validaMovimientoStock);

                    }
                    else
                    {
                        validaMovimientoStockStatust.status = "OK";
                        validaMovimientoStockStatust.descripcion = "La Paleta se puede asignar";
                        validaMovimientoStock = getMaterialXIdPaleta(intPaleta);
                    }
                }
            }
            else
            {
                validaMovimientoStockStatust.status = "NO_OK";
                validaMovimientoStockStatust.descripcion = "La Paleta " + movimiento.coPaleta + " no existe";
            }


        }
        catch (Exception e)
        {
            intPaleta = 0;
            validaMovimientoStockStatust.status = "NO_OK";
            validaMovimientoStockStatust.descripcion = "La Paleta " + movimiento.coPaleta + " no existe";
        }
        finally
        {
            ConectString.Close();
        }
        //return intPaleta;
        /*
        validaMovimientoStockStatust = new ValidaMovimientoStockStatust();
        validaMovimientoStock = new ValidaMovimientoStock();
        validaMovimientoStock.idPaleta = ""+intPaleta;
        if (intPaleta >0)
        {
            validaMovimientoStockStatust.status = "OK";
            validaMovimientoStockStatust.descripcion = "La Paleta se puede asignar";
        }
        else
        {
            validaMovimientoStockStatust.status = "NO_OK";
            validaMovimientoStockStatust.descripcion = "La Paleta no existe";
        }
        */

        validaMovimientoStockStatust.validaMovimientoStock = validaMovimientoStock;
        return validaMovimientoStockStatust;


    }

    public static ValidaUbicacionStocktatust getUbicacionXCoUbicacion(AltmLayout altmLayout)
    {
        int intUbicacion = 0;

        ValidaUbicacionStocktatust validaUbicacionStocktatust = new ValidaUbicacionStocktatust();
        UbicacionStock ubicacionStock = new UbicacionStock();

        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);




        string data = "";
        SqlCommand cmd;
        try
        {
            /*
            //Stock
            data = " SELECT ISNULL(MAX(UF.ID_UBICACION_FISICA),0)  FROM dbo.ALTM_UBICACION_FISICA UF, dbo.ALTM_LAYOUT L   WHERE  UF.ID_LAYAOUT = L.ID_LAYAOUT AND L.ID_ALMACEN =" + altmLayout.idAlmacen + " AND CO_UBICACION_FISICA = '" + altmLayout.coUbicacionFisica + "'";
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            intUbicacion = Convert.ToInt32(cmd.ExecuteScalar());
            */

            data = " SELECT ISNULL(MAX(UF.ID_UBICACION_FISICA),0) ID_UBICACION_FISICA, UF.ID_ESTADO  FROM dbo.ALTM_UBICACION_FISICA UF, dbo.ALTM_LAYOUT L   WHERE  UF.ID_LAYAOUT = L.ID_LAYAOUT AND L.ID_ALMACEN =" + altmLayout.idAlmacen + " AND CO_UBICACION_FISICA = '" + altmLayout.coUbicacionFisica + "' GROUP BY UF.ID_ESTADO ";


            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            SqlDataReader comando = cmd.ExecuteReader();

            Dictionary<string, UbicacionStock> dict = new Dictionary<string, UbicacionStock>();
            while (comando.Read())
            {
                ubicacionStock = new UbicacionStock();
                ubicacionStock.idUbicacionFisica = Int32.Parse(comando.GetValue(0).ToString());
                ubicacionStock.idEstado = Int32.Parse(comando.GetValue(1).ToString());
            }




            /*

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                altmLayout = new AltmLayout();

                altmLayout.idLayaout = Int32.Parse(barras.GetValue(0).ToString());
                altmLayout.coLayaout = barras.GetValue(1).ToString();
                altmLayout.idAlmacen = Int32.Parse(barras.GetValue(2).ToString());
                altmLayout.caBloques = Int32.Parse(barras.GetValue(3).ToString());
                altmLayout.caNiveles = Int32.Parse(barras.GetValue(4).ToString());
                altmLayout.caCarriles = Int32.Parse(barras.GetValue(5).ToString());
                altmLayout.nuCantMax = Int32.Parse(barras.GetValue(6).ToString());
                altmLayout.idEstado = Int32.Parse(barras.GetValue(7).ToString());

                break;
            }
            */
        }
        catch (Exception e)
        {
            //intPaleta = 0;
            ubicacionStock = null;
        }
        finally
        {
            ConectString.Close();
        }
        if (ubicacionStock != null)
        {
            if (ubicacionStock.idUbicacionFisica > 0)
            {
                if (validaStock(ubicacionStock.idUbicacionFisica) <= 0)
                {
                    validaUbicacionStocktatust.status = "NO_OK";
                    validaUbicacionStocktatust.descripcion = "La ubicación " + altmLayout.coUbicacionFisica + " Se encuentra lleno";
                    validaUbicacionStocktatust.ubicacionStock = ubicacionStock;
                }
                else
                {
                    validaUbicacionStocktatust.status = "OK";
                    validaUbicacionStocktatust.ubicacionStock = ubicacionStock;
                }
            }
            else
            {
                validaUbicacionStocktatust.status = "NO OK";
                validaUbicacionStocktatust.descripcion = "La ubicación " + altmLayout.coUbicacionFisica + " no se encuentra registrado";
            }

            return validaUbicacionStocktatust;
        }
        else
        {
            validaUbicacionStocktatust.status = "NO OK";
            validaUbicacionStocktatust.descripcion = "La ubicación " + altmLayout.coUbicacionFisica + " no se encuentra registrado";
        }
        return validaUbicacionStocktatust;
    }
 
    public static int validaStock(int idUbicacionFisica)
    {

        String data = "";
        int result = 0;
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        SqlCommand cmd;
        int registro = 0;

        try
        {
            //Cantidad Ingreso

            data = "SELECT UF.NU_CANT_MAX - ISNULL(S.CA_STOCK, 0) AS RESTO " +
            " FROM DBO.ALTM_UBICACION_FISICA UF LEFT OUTER JOIN DBO.ALTM_STOCK S ON(UF.ID_UBICACION_FISICA = S.ID_UBICACION_FISICA) " +
            " WHERE UF.ID_UBICACION_FISICA = " + idUbicacionFisica;

            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            registro = Convert.ToInt32(cmd.ExecuteScalar());


            transaction.Commit();
        }
        catch (Exception e)
        {
            registro = 0;
        }
        finally
        {
            ConectString.Close();
        }
        return registro;
    }

    public static int movimientoValiMovimiento(AltvStockMovimiento altvStockMovimiento)
    {
        int intPaleta = 0;


        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);


        int movIngreso = 0;
        int movSalida = 0;
        int caStock = 0;

        string data = "";
        SqlCommand cmd;
        try
        {
            //Stock


            //Cantidad Ingreso
            data = " SELECT COUNT(1) FROM  ALTV_STOCK_MOVIMIENTO MO,   DBO.ALTM_UBICACION_FISICA UF, DBO.ALTR_TIPO_MOVIMIENTO TM WHERE MO.ID_PALETA =  " + altvStockMovimiento.idPaleta + "AND " +
                   " UF.ID_UBICACION_FISICA = MO.ID_UBICACION_FISICA AND MO.ID_TIPO_MOVIMIENTO = TM.ID_TIPO_MOVIMIENTO AND TM.CO_TIPO_MOVIMIENTO = '01' ";
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            movIngreso = Convert.ToInt32(cmd.ExecuteScalar());

            //Cantidad Salida
            data = " SELECT COUNT(1) FROM dbo.ALTV_STOCK_MOVIMIENTO M, DBO.ALTR_TIPO_MOVIMIENTO TM WHERE M.ID_UBICACION_FISICA = " + altvStockMovimiento.idUbicacionFisica + " AND M.ID_TIPO_MOVIMIENTO = TM.ID_TIPO_MOVIMIENTO AND TM.CO_TIPO_MOVIMIENTO = '02' ";
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            movSalida = Convert.ToInt32(cmd.ExecuteScalar());

            caStock = movIngreso - movSalida;


            /*

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                altmLayout = new AltmLayout();

                altmLayout.idLayaout = Int32.Parse(barras.GetValue(0).ToString());
                altmLayout.coLayaout = barras.GetValue(1).ToString();
                altmLayout.idAlmacen = Int32.Parse(barras.GetValue(2).ToString());
                altmLayout.caBloques = Int32.Parse(barras.GetValue(3).ToString());
                altmLayout.caNiveles = Int32.Parse(barras.GetValue(4).ToString());
                altmLayout.caCarriles = Int32.Parse(barras.GetValue(5).ToString());
                altmLayout.nuCantMax = Int32.Parse(barras.GetValue(6).ToString());
                altmLayout.idEstado = Int32.Parse(barras.GetValue(7).ToString());

                break;
            }
            */
        }
        catch (Exception e)
        {
            intPaleta = 0;
        }
        finally
        {
            ConectString.Close();
        }
        return intPaleta;
    }

    public static string movimiento_old(AltvStockMovimiento altvStockMovimiento)
    {
        String resultado = "";
        int result = 0;
        int recepcionId = 0;
        string sqlUbicacion = "";
        string sqlTockInsert = "";
        string sqlTockUpdate = "";



        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);
        sqlUbicacion = " SELECT ISNULL(MAX(ID_STOCK),0)  FROM dbo.ALTM_STOCK  WHERE ID_UBICACION_FISICA = " + altvStockMovimiento.idUbicacionFisica;
        try
        { /*
                if ()
                {

                }else if ()
                {
                    SqlCommand cmd = new SqlCommand(sqlUbicacion, ConectString);
                    cmd.Transaction = transaction;
                    int existeStock = Convert.ToInt32(cmd.ExecuteScalar());

                    if (existeStock == 0)
                    {
                    }
                }
                else if ()
                {

                }else if ()
                {

                }
                */

        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
        }
        finally
        {
            ConectString.Close();
        }
        return resultado;

    }

    public static string movimientoSalidaEntrada(AltvStockMovimiento altvStockMovimiento)
    {
        int idUbicacionFisicaOLd = altvStockMovimiento.idUbicacionFisicaOLd;
        int idTipoMovimientoOld = altvStockMovimiento.idTipoMovimientoOld;

        int idUbicacionFisica = altvStockMovimiento.idUbicacionFisica;
        int idTipoMovimiento = altvStockMovimiento.idTipoMovimiento;


            //Salida
            AltvStockMovimiento mvSalida = new AltvStockMovimiento();
            //mvSalida = altvStockMovimiento;
            mvSalida.idPaleta = altvStockMovimiento.idPaleta;
            mvSalida.idUsuCrea = altvStockMovimiento.idUsuCrea;
            mvSalida.idEstado = altvStockMovimiento.idEstado;
            mvSalida.idAlmacen = altvStockMovimiento.idAlmacen;
            mvSalida.idUbicacionFisica = idUbicacionFisicaOLd;
            mvSalida.idTipoMovimiento = idTipoMovimientoOld;

            //Ingreso
            AltvStockMovimiento mvEntrada = new AltvStockMovimiento();
            //mvEntrada = altvStockMovimiento;
            mvEntrada.idPaleta = altvStockMovimiento.idPaleta;
            mvEntrada.idUsuCrea = altvStockMovimiento.idUsuCrea;
            mvEntrada.idEstado = altvStockMovimiento.idEstado;
            mvEntrada.idAlmacen = altvStockMovimiento.idAlmacen;
            mvEntrada.idUbicacionFisica = idUbicacionFisica;
            mvEntrada.idTipoMovimiento = idTipoMovimiento;

        movimiento(mvSalida);
        
        movimiento(mvEntrada);
        return "OK";

    }
    public static string movimiento(AltvStockMovimiento altvStockMovimiento)
    {
        String resultado = "";
        int result = 0;
        int recepcionId = 0;
        string sqlMovimiento = "";
        string sqlTockInsert = "";
        string sqlTockUpdate = "";

            SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
            ConectString.Open();
            SqlTransaction transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted); 

            try
        {
               

                // Start a local transaction
               
                SqlCommand cmd = null;
                //ID_STOCK_MOVIMIENTO,
                if (validaExistenciaMovimiento(altvStockMovimiento) == 0)
                {
                    sqlMovimiento = "INSERT INTO  DBO.ALTV_STOCK_MOVIMIENTO(ID_TIPO_MOVIMIENTO, ID_UBICACION_FISICA, ID_PALETA, FE_CREA, ID_USU_CREA, ID_ESTADO )" +
                                        "VALUES(" + altvStockMovimiento.idTipoMovimiento + ", " + altvStockMovimiento.idUbicacionFisica + ", " + altvStockMovimiento.idPaleta + ", GETDATE(), " + altvStockMovimiento.idUsuCrea + ", " + altvStockMovimiento.idEstado + "  )";
                    cmd = new SqlCommand(sqlMovimiento, ConectString);
                    // Assign transaction object for a pending local transaction
                    cmd.Transaction = transaction;
                    int resultado1 = (int)cmd.ExecuteNonQuery();
                }

                int movIngreso = 0;
                int movSalida = 0;
                int caStock = 0;
                string data = "";


                //Cantidad Ingreso
                data = " SELECT COUNT(1) FROM dbo.ALTV_STOCK_MOVIMIENTO M, DBO.ALTR_TIPO_MOVIMIENTO TM WHERE M.ID_UBICACION_FISICA = " + altvStockMovimiento.idUbicacionFisica + " AND M.ID_TIPO_MOVIMIENTO = TM.ID_TIPO_MOVIMIENTO AND TM.CO_TIPO_MOVIMIENTO IN ('01','04','05')  ";
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            movIngreso = Convert.ToInt32(cmd.ExecuteScalar());

            //Cantidad Salida
            data = " SELECT COUNT(1) FROM dbo.ALTV_STOCK_MOVIMIENTO M, DBO.ALTR_TIPO_MOVIMIENTO TM WHERE M.ID_UBICACION_FISICA = " + altvStockMovimiento.idUbicacionFisica + " AND M.ID_TIPO_MOVIMIENTO = TM.ID_TIPO_MOVIMIENTO AND TM.CO_TIPO_MOVIMIENTO IN ('02','03') ";
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            movSalida = Convert.ToInt32(cmd.ExecuteScalar());

            caStock = movIngreso - movSalida;

            //Stock
            data = " SELECT ISNULL(MAX(ID_STOCK),0)  FROM dbo.ALTM_STOCK  WHERE ID_UBICACION_FISICA = " + altvStockMovimiento.idUbicacionFisica;
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            int existeStock = Convert.ToInt32(cmd.ExecuteScalar());

            if (existeStock == 0)
            {
                //Insert Detalle                
                //ID_STOCK,
                sqlTockInsert = "INSERT INTO     DBO.ALTM_STOCK (ID_UBICACION_FISICA, CA_STOCK, FE_CREA, ID_USU_CREA, ID_ESTADO)" +
                        "VALUES(" + altvStockMovimiento.idUbicacionFisica + ", " + caStock + ", GETDATE(), " + altvStockMovimiento.idUsuCrea + ", " + altvStockMovimiento.idEstado + ")";

                cmd = new SqlCommand(sqlTockInsert, ConectString);
                cmd.Transaction = transaction;
                resultado = "Exito: " + cmd.ExecuteNonQuery();

            }
            else
            {
                //Upodate
                sqlTockUpdate = "UPDATE DBO.ALTM_STOCK  " +
                                    " SET     " +
                                    "    ID_UBICACION_FISICA =" + altvStockMovimiento.idUbicacionFisica + "," +
                                    "    CA_STOCK = " + caStock + "," +
                                    "    FE_ACTUALIZA = GETDATE() ," +
                                    "    ID_USU_CREA = " + altvStockMovimiento.idUsuCrea + "," +
                                    "    ID_ESTADO = " + altvStockMovimiento.idEstado +
                                    "   WHERE ID_STOCK = " + existeStock;
                cmd = new SqlCommand(sqlTockUpdate, ConectString);
                cmd.Transaction = transaction;
                resultado = "Exito: " + cmd.ExecuteNonQuery();
            }
            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
        }
        finally
        {
            ConectString.Close();
        }
        return resultado;
    }

    public static int validaExistenciaMovimiento(AltvStockMovimiento altvStockMovimiento)
    {

        String data = "";
        int result = 0;
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        SqlCommand cmd;
        int registro = 0;
        int registro1 = 0;
        int registro2 = 0;
        try
        {
            //Cantidad Ingreso
            /*
           data = "SELECT COUNT(1) " +
           " FROM DBO.ALTV_STOCK_MOVIMIENTO SM" +
           " WHERE SM.ID_TIPO_MOVIMIENTO = " + altvStockMovimiento.idTipoMovimiento + 
           " AND SM.ID_UBICACION_FISICA = " + 
           altvStockMovimiento.idUbicacionFisica + " AND SM.ID_PALETA= " + altvStockMovimiento.idPaleta;

           */

            data = "SELECT COUNT(1) " +
          " FROM DBO.ALTV_STOCK_MOVIMIENTO SM" +
          " WHERE SM.ID_TIPO_MOVIMIENTO = " + altvStockMovimiento.idTipoMovimiento +
          " AND SM.ID_UBICACION_FISICA = " +
          altvStockMovimiento.idUbicacionFisica + " AND SM.ID_PALETA= " + altvStockMovimiento.idPaleta +
           " AND SM.ID_STOCK_MOVIMIENTO = (SELECT ISNULL(MAX(ID_STOCK_MOVIMIENTO), 0) AS ID_STOCK_MOVIMIENTO " +
           " FROM DBO.ALTV_STOCK_MOVIMIENTO WHERE ID_PALETA = " + altvStockMovimiento.idPaleta +  " )";


            /*
            data = "SELECT COUNT(1) " +
          " FROM DBO.ALTV_STOCK_MOVIMIENTO SM" +
          " WHERE SM.ID_TIPO_MOVIMIENTO = " + altvStockMovimiento.idTipoMovimiento;
            */
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            registro = Convert.ToInt32(cmd.ExecuteScalar());

            /*
            data = "SELECT ISNULL(MAX(ID_STOCK_MOVIMIENTO), 0) AS ID_STOCK_MOVIMIENTO "+
           " FROM DBO.ALTV_STOCK_MOVIMIENTO WHERE ID_PALETA = " + altvStockMovimiento.idPaleta + " AND ID_TIPO_MOVIMIENTO = " + altvStockMovimiento.idTipoMovimiento;

            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            registro2 = Convert.ToInt32(cmd.ExecuteScalar());


            if(registro1== registro2)
            {
                registro = 1;
            }else{
                registro = 0;
            }
            */

            transaction.Commit();
        }
        catch (Exception e)
        {
            registro = 0;
        }
        finally
        {
            ConectString.Close();
        }
        return registro;
    }

    public static string movimientoSalida(AltvStockMovimiento altvStockMovimiento)
    {
        String resultado = "";
        int result = 0;
        int recepcionId = 0;
        string sqlMovimiento = "";
        string sqlTockInsert = "";
        string sqlTockUpdate = "";



        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);



        int movIngreso = 0;
        int movSalida = 0;
        int caStock = 0;
        string data = "";

        try
        {



            //ID_STOCK_MOVIMIENTO,
            sqlMovimiento = "INSERT INTO  DBO.ALTV_STOCK_MOVIMIENTO(ID_TIPO_MOVIMIENTO, ID_UBICACION_FISICA, ID_PALETA, FE_CREA, ID_USU_CREA, ID_ESTADO )" +
                                "VALUES(" + altvStockMovimiento.idTipoMovimiento + ", " + altvStockMovimiento.idUbicacionFisica + ", " + altvStockMovimiento.idPaleta + ", GETDATE(), " + altvStockMovimiento.idUsuCrea + ", " + altvStockMovimiento.idEstado + "  )";
            SqlCommand cmd = new SqlCommand(sqlMovimiento, ConectString);
            // Assign transaction object for a pending local transaction
            cmd.Transaction = transaction;


            int resultado1 = (int)cmd.ExecuteNonQuery();

            //Cantidad Ingreso
            data = " SELECT COUNT(1) FROM dbo.ALTV_STOCK_MOVIMIENTO M, DBO.ALTR_TIPO_MOVIMIENTO TM WHERE M.ID_UBICACION_FISICA = " + altvStockMovimiento.idUbicacionFisica + " AND M.ID_TIPO_MOVIMIENTO = TM.ID_TIPO_MOVIMIENTO AND TM.CO_TIPO_MOVIMIENTO IN ('01','04','05') ";
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            movIngreso = Convert.ToInt32(cmd.ExecuteScalar());

            //Cantidad Salida
            data = " SELECT COUNT(1) FROM dbo.ALTV_STOCK_MOVIMIENTO M, DBO.ALTR_TIPO_MOVIMIENTO TM WHERE M.ID_UBICACION_FISICA = " + altvStockMovimiento.idUbicacionFisica + " AND M.ID_TIPO_MOVIMIENTO = TM.ID_TIPO_MOVIMIENTO AND TM.CO_TIPO_MOVIMIENTO IN ('02','03') ";
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            movSalida = Convert.ToInt32(cmd.ExecuteScalar());

            caStock = movIngreso - movSalida;

            //Stock
            data = " SELECT ISNULL(MAX(ID_STOCK),0)  FROM dbo.ALTM_STOCK  WHERE ID_UBICACION_FISICA = " + altvStockMovimiento.idUbicacionFisica;
            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            int existeStock = Convert.ToInt32(cmd.ExecuteScalar());

            if (existeStock == 0)
            {
                //Insert Detalle                
                //ID_STOCK,
                sqlTockInsert = "INSERT INTO     DBO.ALTM_STOCK (ID_UBICACION_FISICA, CA_STOCK, FE_CREA, ID_USU_CREA, ID_ESTADO)" +
                        "VALUES(" + altvStockMovimiento.idUbicacionFisica + ", " + caStock + ", GETDATE(), " + altvStockMovimiento.idUsuCrea + ", " + altvStockMovimiento.idEstado + ")";

                cmd = new SqlCommand(sqlTockInsert, ConectString);
                cmd.Transaction = transaction;
                resultado = "Exito: " + cmd.ExecuteNonQuery();

            }
            else
            {
                //Upodate
                sqlTockUpdate = "UPDATE DBO.ALTM_STOCK  " +
                                    " SET     " +
                                    "    ID_UBICACION_FISICA =" + altvStockMovimiento.idUbicacionFisica + "," +
                                    "    CA_STOCK = " + caStock + "," +
                                    "    FE_ACTUALIZA = GETDATE() ," +
                                    "    ID_USU_CREA = " + altvStockMovimiento.idUsuCrea + "," +
                                    "    ID_ESTADO = " + altvStockMovimiento.idEstado +
                                    "   WHERE ID_STOCK = " + existeStock;
                cmd = new SqlCommand(sqlTockUpdate, ConectString);
                cmd.Transaction = transaction;
                resultado = "Exito: " + cmd.ExecuteNonQuery();
            }
            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            resultado = "";
        }
        finally
        {
            ConectString.Close();
        }
        return resultado;
    }

    public static ValidaMovimientoStockStatust getMovimientoStatusReingresoPaleta(ValidaMovimientoStock validaMovimientoStock)
    {
        String resultado = "";

        ValidaMovimientoStock movimientoStock = null;
        ValidaMovimientoStockStatust validaMovimientoStockStatust = null;

        SetmUsuario setmUsuario = new SetmUsuario();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        String sqlMovimineto = "";

        try
        {


            sqlMovimineto = "SELECT MO.ID_STOCK_MOVIMIENTO, ISNULL(UF.ID_UBICACION_FISICA, 0) ID_UBICACION_FISICA, TM.CO_TIPO_MOVIMIENTO, UF.NU_CANT_MAX, UF.IN_FULL " +
            " FROM  DBO.ALTV_STOCK_MOVIMIENTO MO, DBO.ALTM_UBICACION_FISICA UF, dbo.ALTM_LAYOUT L, DBO.ALTR_TIPO_MOVIMIENTO TM " +
            "     WHERE " +
            "         MO.ID_PALETA =" + validaMovimientoStock.idPaleta + " AND " +
            "         UF.ID_UBICACION_FISICA = MO.ID_UBICACION_FISICA AND " +
            "         MO.ID_TIPO_MOVIMIENTO = TM.ID_TIPO_MOVIMIENTO  AND " +
            "         UF.ID_LAYAOUT = L.ID_LAYAOUT AND L.ID_ALMACEN = " + validaMovimientoStock.idAlmacen +
            "         AND MO.ID_STOCK_MOVIMIENTO = (SELECT  MAX(M.ID_STOCK_MOVIMIENTO) FROM ALTV_STOCK_MOVIMIENTO M WHERE M.ID_PALETA = MO.ID_PALETA) " +
            " ORDER BY MO.ID_STOCK_MOVIMIENTO DESC ";


            SqlCommand cmd = new SqlCommand(sqlMovimineto, ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                movimientoStock = new ValidaMovimientoStock();
                movimientoStock.idStockMovimiento = Int32.Parse(barras.GetValue(0).ToString());
                movimientoStock.idUbicacionFisica = Int32.Parse(barras.GetValue(1).ToString());
                movimientoStock.coTipoMovimiento = barras.GetValue(2).ToString();
                movimientoStock.nuCantMax = Int32.Parse(barras.GetValue(3).ToString());
                movimientoStock.inFull = Int32.Parse(barras.GetValue(4).ToString());

                break;
            }

            validaMovimientoStockStatust = new ValidaMovimientoStockStatust();
            validaMovimientoStockStatust.validaMovimientoStock = movimientoStock;


        }
        catch (Exception e)
        {
            movimientoStock = null;
        }
        finally
        {
            ConectString.Close();
        }

        if (validaMovimientoStock.coTipoMovimiento == "01")//Ingreso
        {
            if (movimientoStock == null)
            {
                validaMovimientoStockStatust.status = "00";
                validaMovimientoStockStatust.descripcion = "PUEDE INGRESAR ";
            }
            else if (movimientoStock.coTipoMovimiento == "02" || movimientoStock.coTipoMovimiento == "03")
            {

                validaMovimientoStockStatust.status = "00";
                validaMovimientoStockStatust.descripcion = "PUEDE INGRESAR ";

            }
            else if (movimientoStock.coTipoMovimiento == "01" || movimientoStock.coTipoMovimiento == "04" || movimientoStock.coTipoMovimiento == "05")
            {

                validaMovimientoStockStatust.status = "01";
                validaMovimientoStockStatust.descripcion = "LA ETIQUETA YA ESTA REGISTRADA ";
            }

        }
        else if (validaMovimientoStock.coTipoMovimiento == "02")//Salida
        {

            if (movimientoStock == null)
            {
                validaMovimientoStockStatust.status = "06";
                validaMovimientoStockStatust.descripcion = "NO EXISTE EN NICHO ";
            }
            else if (movimientoStock.coTipoMovimiento == "01" || movimientoStock.coTipoMovimiento == "04" || movimientoStock.coTipoMovimiento == "05")
            {

                validaMovimientoStockStatust.status = "04";
                validaMovimientoStockStatust.descripcion = "PUEDE RETIRAR ";

            }
            else if (movimientoStock.coTipoMovimiento == "02" || movimientoStock.coTipoMovimiento == "03")
            {

                validaMovimientoStockStatust.status = "05";
                validaMovimientoStockStatust.descripcion = "YA FUE RETIRADO DEL NICHO ";
            }
        }
        else if (validaMovimientoStock.coTipoMovimiento == "03-04")//Cambiar de Ubicación
        {

            if (movimientoStock == null)
            {
                validaMovimientoStockStatust.status = "06";
                validaMovimientoStockStatust.descripcion = "NO EXISTE EN NICHO ";
            }
            else if (movimientoStock.coTipoMovimiento == "01" || movimientoStock.coTipoMovimiento == "04" || movimientoStock.coTipoMovimiento == "05")
            {

                validaMovimientoStockStatust.status = "04";
                validaMovimientoStockStatust.descripcion = "PUEDE RETIRAR ";

            }
            else if (movimientoStock.coTipoMovimiento == "02" || movimientoStock.coTipoMovimiento == "03")
            {

                validaMovimientoStockStatust.status = "05";
                validaMovimientoStockStatust.descripcion = "YA FUE RETIRADO EN NICHO ";
            }
        }


        else if (validaMovimientoStock.coTipoMovimiento == "05")//Reingreso de Paleta
        {
            if (movimientoStock == null)
            {
                validaMovimientoStockStatust.status = "OK";
                validaMovimientoStockStatust.descripcion = "PUEDE INGRESAR ";

                //Valida Estado Almacen
            }
            else if (movimientoStock.coTipoMovimiento == "02" || movimientoStock.coTipoMovimiento == "03")
            {

                validaMovimientoStockStatust.status = "OK";
                validaMovimientoStockStatust.descripcion = "PUEDE INGRESAR ";

            }
            else if (movimientoStock.coTipoMovimiento == "01" || movimientoStock.coTipoMovimiento == "04" || movimientoStock.coTipoMovimiento == "05")
            {

                validaMovimientoStockStatust.status = "NO_OK";
                validaMovimientoStockStatust.descripcion = "LA ETIQUETA YA ESTA REGISTRADA ";
            }

        }


        return validaMovimientoStockStatust;
    }

        public static ValidaMovimientoStock getUbicacionXCodPaleta(ValidaMovimientoStock validaMovimientoStock)
        {
            ValidaMovimientoStock validaMovimientoStock1 = new ValidaMovimientoStock();

            SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
            ConectString.Open();
            SqlTransaction transaction;

            // Start a local transaction
            transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

            string data = "";
            SqlCommand cmd;
            try
            {
                 
                data = " SELECT  UF.CO_UBICACION_FISICA, UB.CO_PALETA "+
                       " FROM dbo.ALTM_UBICACION_FISICA UF, " +
                           " dbo.ALTV_STOCK_MOVIMIENTO SM, " +
                           " (SELECT SM.ID_PALETA, P.CO_PALETA, MAX(SM.ID_STOCK_MOVIMIENTO) ID_STOCK_MOVIMIENTO " +
                           " FROM dbo.ALTV_STOCK_MOVIMIENTO SM, " +
                           "       dbo.ALTV_PALETA P " +
                           "      WHERE SM.ID_PALETA = P.ID_PALETA AND " +
                           "          P.CO_PALETA = '" + validaMovimientoStock.coPaleta + "'" +
                           "  GROUP BY SM.ID_PALETA, P.CO_PALETA  ) UB " +
                           "      WHERE  SM.ID_STOCK_MOVIMIENTO = UB.ID_STOCK_MOVIMIENTO  AND  SM.ID_TIPO_MOVIMIENTO in (2,5,6) AND " +
                           "      SM.ID_UBICACION_FISICA = UF.ID_UBICACION_FISICA ";

                cmd = new SqlCommand(data, ConectString);
                cmd.Transaction = transaction;
                SqlDataReader comando = cmd.ExecuteReader();

                Dictionary<string, UbicacionStock> dict = new Dictionary<string, UbicacionStock>();
                while (comando.Read())
                {
                    validaMovimientoStock1 = new ValidaMovimientoStock();

                    validaMovimientoStock1.coUbicacionFisica = comando.GetValue(0).ToString();
                    validaMovimientoStock1.coPaleta = comando.GetValue(1).ToString(); 
                    break;
                }
            }
            catch (Exception e)
            {
                //intPaleta = 0;
                validaMovimientoStock1 = null;
            }
            finally
            {
                ConectString.Close();
            }
            return validaMovimientoStock1;
        }

        public static ValidaMovimientoStock getMaterialXIdPaleta(int idPaleta)
    {
        ValidaMovimientoStock validaMovimientoStock = new ValidaMovimientoStock();

        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        SqlTransaction transaction;

        // Start a local transaction
        transaction = ConectString.BeginTransaction(IsolationLevel.ReadCommitted);

        string data = "";
        SqlCommand cmd;
        try
        {

            data = " SELECT RE.ID_RECEPCION, PA.ID_PALETA,  RE.CA_PALETAS, RE.CA_CAJA_BOLSA, RE.NU_LOTE, PA.CO_PALETA, MA.CO_MATERIAL, MA.DE_MATERIAL, TU.CO_TIPO_UNI_ALMACENA  " +
            " FROM DBO.ALTM_RECEPCION RE, DBO.ALVM_MATERIAL MA, ALTR_TIPO_UNIDAD_ALMACENA TU, DBO.ALTV_PALETA PA " +
            "  WHERE RE.ID_RECEPCION = PA.ID_RECEPCION AND RE.ID_MATERIAL = MA.ID_MATERIAL AND RE.ID_TIPO_UNI_ALMACENA = TU.ID_TIPO_UNI_ALMACENA " +
                      "   AND PA.ID_PALETA = " + idPaleta +
                      "   ORDER BY PA.ID_PALETA ASC";

            cmd = new SqlCommand(data, ConectString);
            cmd.Transaction = transaction;
            SqlDataReader comando = cmd.ExecuteReader();

            Dictionary<string, UbicacionStock> dict = new Dictionary<string, UbicacionStock>();
            while (comando.Read())
            {
                validaMovimientoStock = new ValidaMovimientoStock();

                validaMovimientoStock.idRecepcion = comando.GetValue(0).ToString();
                validaMovimientoStock.idPaleta = comando.GetValue(1).ToString();
                validaMovimientoStock.caPaletas = Int32.Parse(comando.GetValue(2).ToString());
                validaMovimientoStock.caCajaBolsa = Int32.Parse(comando.GetValue(3).ToString());
                validaMovimientoStock.nuLote = comando.GetValue(4).ToString();
                validaMovimientoStock.coPaleta = comando.GetValue(5).ToString();
                validaMovimientoStock.material = comando.GetValue(6).ToString();
                validaMovimientoStock.deMaterial = comando.GetValue(7).ToString();
                validaMovimientoStock.coTipoUniAlmacena = comando.GetValue(8).ToString();
                break;
            }
        }
        catch (Exception e)
        {
            //intPaleta = 0;
            validaMovimientoStock = null;
        }
        finally
        {
            ConectString.Close();
        }
        return validaMovimientoStock;
    }


    public static ValidaMovimientoStockStatust getMovimientoStatus(ValidaMovimientoStock validaMovimientoStock)
    {
        String resultado = "";

        ValidaMovimientoStock movimientoStock = null;
        ValidaMovimientoStockStatust validaMovimientoStockStatust = null;

        SetmUsuario setmUsuario = new SetmUsuario();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        String sqlMovimineto = "";

        try
        {


            sqlMovimineto = "SELECT MO.ID_STOCK_MOVIMIENTO, ISNULL(UF.ID_UBICACION_FISICA, 0) ID_UBICACION_FISICA, TM.CO_TIPO_MOVIMIENTO, UF.NU_CANT_MAX, UF.IN_FULL, UF.CO_UBICACION_FISICA " +
            " FROM  DBO.ALTV_STOCK_MOVIMIENTO MO, DBO.ALTM_UBICACION_FISICA UF, dbo.ALTM_LAYOUT L, DBO.ALTR_TIPO_MOVIMIENTO TM " +
            "     WHERE " +
            "         MO.ID_PALETA =" + validaMovimientoStock.idPaleta + " AND " +
            "         UF.ID_UBICACION_FISICA = MO.ID_UBICACION_FISICA AND " +
            "         MO.ID_TIPO_MOVIMIENTO = TM.ID_TIPO_MOVIMIENTO  AND " +
            "         UF.ID_LAYAOUT = L.ID_LAYAOUT AND L.ID_ALMACEN = " + validaMovimientoStock.idAlmacen +
            "         AND MO.ID_STOCK_MOVIMIENTO = (SELECT  MAX(M.ID_STOCK_MOVIMIENTO) FROM ALTV_STOCK_MOVIMIENTO M WHERE M.ID_PALETA = MO.ID_PALETA) " +
            " ORDER BY MO.ID_STOCK_MOVIMIENTO DESC ";


            SqlCommand cmd = new SqlCommand(sqlMovimineto, ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                movimientoStock = new ValidaMovimientoStock();
                movimientoStock.idStockMovimiento = Int32.Parse(barras.GetValue(0).ToString());
                movimientoStock.idUbicacionFisica = Int32.Parse(barras.GetValue(1).ToString());
                movimientoStock.coTipoMovimiento = barras.GetValue(2).ToString();
                movimientoStock.nuCantMax = Int32.Parse(barras.GetValue(3).ToString());
                movimientoStock.inFull = Int32.Parse(barras.GetValue(4).ToString());
                movimientoStock.coUbicacionFisica = barras.GetValue(5).ToString();

                break;
            }

            validaMovimientoStockStatust = new ValidaMovimientoStockStatust();
            validaMovimientoStockStatust.validaMovimientoStock = movimientoStock;
    


        }
        catch (Exception e)
        {
            movimientoStock = null;
        }
        finally
        {
            ConectString.Close();
        }

        if (validaMovimientoStock.coTipoMovimiento == "01")//Ingreso
        {
            if (movimientoStock == null)
            {
                validaMovimientoStockStatust.status = "00";
                validaMovimientoStockStatust.descripcion = "PUEDE INGRESAR ";
            }
            else if (movimientoStock.coTipoMovimiento == "02" || movimientoStock.coTipoMovimiento == "03")
            {

                validaMovimientoStockStatust.status = "00";
                validaMovimientoStockStatust.descripcion = "PUEDE INGRESAR ";

            }
            else if (movimientoStock.coTipoMovimiento == "01" || movimientoStock.coTipoMovimiento == "04" || movimientoStock.coTipoMovimiento == "05")
            {

                validaMovimientoStockStatust.status = "01";
                validaMovimientoStockStatust.descripcion = "LA ETIQUETA YA ESTA REGISTRADA ";
            }

        }
        else if (validaMovimientoStock.coTipoMovimiento == "02")//Salida
        {

            if (movimientoStock == null)
            {
                validaMovimientoStockStatust.status = "06";
                validaMovimientoStockStatust.descripcion = "NO EXISTE EN NICHO ";
            }
            else if (movimientoStock.coTipoMovimiento == "01" || movimientoStock.coTipoMovimiento == "04" || movimientoStock.coTipoMovimiento == "05")
            {

                validaMovimientoStockStatust.status = "04";
                validaMovimientoStockStatust.descripcion = "PUEDE RETIRAR ";

            }
            else if (movimientoStock.coTipoMovimiento == "02" || movimientoStock.coTipoMovimiento == "03")
            {

                validaMovimientoStockStatust.status = "05";
                validaMovimientoStockStatust.descripcion = "YA FUE RETIRADO DEL NICHO ";
            }
        }
        else if (validaMovimientoStock.coTipoMovimiento == "03-04")//Cambiar de Ubicación
        {

            if (movimientoStock == null)
            {
                validaMovimientoStockStatust.status = "06";
                validaMovimientoStockStatust.descripcion = "NO EXISTE EN NICHO ";
            }
            else if (movimientoStock.coTipoMovimiento == "01" || movimientoStock.coTipoMovimiento == "04" || movimientoStock.coTipoMovimiento == "05")
            {
                if (validaMovimientoStock.coUbicacionFisica == validaMovimientoStockStatust.validaMovimientoStock.coUbicacionFisica)
                {

                    validaMovimientoStockStatust.status = "05";
                    validaMovimientoStockStatust.descripcion = "CAMBIAR A OTRA UBICACION GEOGRAFICA ";
                }
                else
                {
                   

                    AltmLayout altmLayout = new AltmLayout();
                    altmLayout.idAlmacen = Int32.Parse(validaMovimientoStock.idAlmacen);
                    altmLayout.coUbicacionFisica = validaMovimientoStock.coUbicacionFisica;
                    ValidaUbicacionStocktatust validaUbicacionStocktatust = getUbicacionXCoUbicacion(altmLayout);                        
                    validaMovimientoStockStatust.ubicacionStock = validaUbicacionStocktatust.ubicacionStock;
                        if(validaUbicacionStocktatust.status != "NO_OK")
                        {
                            validaMovimientoStockStatust.status = "04";
                            //validaMovimientoStockStatust.status = "00";
                            validaMovimientoStockStatust.descripcion = "PUEDE RETIRAR ";
                        }
                        else
                        {
                            validaMovimientoStockStatust.status = validaUbicacionStocktatust.status;
                            validaMovimientoStockStatust.descripcion = validaUbicacionStocktatust.descripcion;
                        }
                        
                    }


            }
            else if (movimientoStock.coTipoMovimiento == "02" || movimientoStock.coTipoMovimiento == "03")
            {

                validaMovimientoStockStatust.status = "05";
                validaMovimientoStockStatust.descripcion = "YA FUE RETIRADO EN NICHO ";
            }
            else if (movimientoStock.coUbicacionFisica == validaMovimientoStockStatust.validaMovimientoStock.coUbicacionFisica)
            {

                validaMovimientoStockStatust.status = "05";
                validaMovimientoStockStatust.descripcion = "CAMBIAR A OTRA UBICACION GEOGRAFICA ";
            }
        }


        else if (validaMovimientoStock.coTipoMovimiento == "05")//Reingreso de Paleta
        {
            if (movimientoStock == null)
            {
                validaMovimientoStockStatust.status = "00";
                validaMovimientoStockStatust.descripcion = "PUEDE INGRESAR ";

                //Valida Estado Almacen
            }
            else if (movimientoStock.coTipoMovimiento == "02" || movimientoStock.coTipoMovimiento == "03")
            {

                validaMovimientoStockStatust.status = "00";
                validaMovimientoStockStatust.descripcion = "PUEDE INGRESAR ";

            }
            else if (movimientoStock.coTipoMovimiento == "01" || movimientoStock.coTipoMovimiento == "04" || movimientoStock.coTipoMovimiento == "05")
            {

                validaMovimientoStockStatust.status = "01";
                validaMovimientoStockStatust.descripcion = "LA ETIQUETA YA ESTA REGISTRADA ";
            }

        }

        if (validaMovimientoStockStatust.status == "00")
        {
            AltmLayout altmLayout = new AltmLayout();
            altmLayout.idAlmacen = Int32.Parse(validaMovimientoStock.idAlmacen);
            altmLayout.coUbicacionFisica = validaMovimientoStock.coUbicacionFisica;
            ValidaUbicacionStocktatust validaUbicacionStocktatust = getUbicacionXCoUbicacion(altmLayout);
            validaMovimientoStockStatust.status = validaUbicacionStocktatust.status;
            validaMovimientoStockStatust.descripcion = validaUbicacionStocktatust.descripcion;
            validaMovimientoStockStatust.ubicacionStock = validaUbicacionStocktatust.ubicacionStock;

        }

        return validaMovimientoStockStatust;
    }




    public static List<Recepcion> listaRecepcion(AltmRecepcion AltmRecepcion)
    {
        String resultado = "";

        List<Recepcion> listRecepcion = new List<Recepcion>();
        Recepcion recepcion = new Recepcion();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        StringBuilder sqlText = new StringBuilder();
        try
        {


            sqlText.Append(" SELECT ");
            sqlText.Append(" R.ID_RECEPCION, ");

            sqlText.Append(" P.ID_PALETA, ");

            sqlText.Append(" P.CO_PALETA, ");
            //sqlText.Append(" CONCAT(P.ID_PALETA, '-', CONVERT(VARCHAR(50), FORMAT(P.FE_CREA, 'yy')))  NUMERO_ANNO, ");


            sqlText.Append(" P.ID_PALETA_ALMACEN NUMERO, ");
            sqlText.Append(" CONVERT(VARCHAR(20), FORMAT(P.FE_CREA, 'dd/MM/yyyy')) AS FECHA, ");
            sqlText.Append(" CONVERT(VARCHAR(20), FORMAT(P.FE_CREA, 'HH:mm:ss')) AS HORA, ");
            sqlText.Append(" R.TXT_DOCUMENTO,   ");
            sqlText.Append(" M.CO_MATERIAL MATERIAL, ");
            sqlText.Append(" R.NU_LOTE LOTE, ");
            sqlText.Append(" R.CA_PALETAS, ");

            //sqlText.Append(" R.CA_CAJA_BOLSA CA_X_PALETA, ");
            sqlText.Append(" ISNULL(P.CA_CAJA_BOLSA, R.CA_CAJA_BOLSA)  CA_X_PALETA, ");

            sqlText.Append(" R.ID_USU_CREA ID_USUARIO, ");
            sqlText.Append(" U.DE_USUARIO RESPONSABLE, ");
            sqlText.Append(" R.ID_TIPO_MOVIMIENTO ID_MOTIVO, ");
            sqlText.Append(" TM.DE_TIPO_MOVIMIENTO MOTIVO, ");
            sqlText.Append(" M.DE_MATERIAL TEXTO_MATERIAL, ");
            sqlText.Append(" R.ID_TIPO_UNI_ALMACENA ID_UMA, ");

            sqlText.Append(" TUA.DE_TIPO_UNI_ALMACENA UMA  ");

            sqlText.Append(" FROM dbo.ALTM_RECEPCION R, dbo.ALVM_MATERIAL M, dbo.ALTV_PALETA P, dbo.ALTR_TIPO_MOVIMIENTO TM, dbo.SETM_USUARIO U, dbo.ALTR_TIPO_UNIDAD_ALMACENA TUA ");
            sqlText.Append(" WHERE R.ID_RECEPCION = P.ID_RECEPCION AND R.ID_MATERIAL = M.ID_MATERIAL AND ");
            sqlText.Append("       R.ID_ALMACEN = " + AltmRecepcion.idAlmacen + " AND ( " + AltmRecepcion.idUsuCrea + "=-1 OR R.ID_USU_CREA = " + AltmRecepcion.idUsuCrea + " ) AND ");

            sqlText.Append("       R.ID_TIPO_MOVIMIENTO = TM.ID_TIPO_MOVIMIENTO AND R.ID_USU_CREA = U.ID_USUARIO AND  ");
            sqlText.Append("        M.CO_MATERIAL LIKE '" + AltmRecepcion.material + "%'  AND  ");
            sqlText.Append("        M.DE_MATERIAL LIKE '" + AltmRecepcion.textoMaterial + "%'  AND  ");
            sqlText.Append("        P.CO_PALETA     LIKE '" + AltmRecepcion.coPaleta + "%'  AND  ");
            sqlText.Append("        R.NU_LOTE     LIKE '" + AltmRecepcion.lote + "%'  AND  ");
            if (AltmRecepcion.feInicio != null && AltmRecepcion.feFin != null)
            {
                sqlText.Append("   R.FE_CREA  BETWEEN Convert(SmallDateTime,'" + AltmRecepcion.feInicio + " 00:00',103) AND Convert(SmallDateTime,'" + AltmRecepcion.feFin + " 23:59',103) AND ");

            }


            sqlText.Append("       R.ID_TIPO_UNI_ALMACENA = TUA.ID_TIPO_UNI_ALMACENA ");
            sqlText.Append(" ORDER BY p.ID_PALETA DESC ");


            SqlCommand cmd = new SqlCommand(sqlText.ToString(), ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                recepcion = new Recepcion();
                recepcion.idRecepcion = barras.GetValue(0).ToString();
                recepcion.idPaleta = barras.GetValue(1).ToString();
                recepcion.numeroAnno = barras.GetValue(2).ToString();
                recepcion.numero = barras.GetValue(3).ToString();
                recepcion.fecha = barras.GetValue(4).ToString();
                recepcion.hora = barras.GetValue(5).ToString();
                recepcion.txtDocumento = barras.GetValue(6).ToString();
                recepcion.material = barras.GetValue(7).ToString();
                recepcion.lote = barras.GetValue(8).ToString();
                recepcion.caPaletas = barras.GetValue(9).ToString();
                recepcion.caXPaleta = barras.GetValue(10).ToString();
                recepcion.idUsuario = barras.GetValue(11).ToString();
                recepcion.responsable = barras.GetValue(12).ToString();
                recepcion.idMotivo = barras.GetValue(13).ToString();
                recepcion.motivo = barras.GetValue(14).ToString();
                recepcion.textoMaterial = barras.GetValue(15).ToString();
                recepcion.idUma = barras.GetValue(16).ToString();
                recepcion.uma = barras.GetValue(17).ToString();


                listRecepcion.Add(recepcion);

            }
        }
        catch (Exception e)
        {
            listRecepcion = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listRecepcion;
    }



    public static List<Movimiento> listaMovimiento(AltvStockMovimiento altvStockMovimiento)
    {
        String resultado = "";

        List<Movimiento> listMovimiento = new List<Movimiento>();
        Movimiento movimiento = new Movimiento();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();
        StringBuilder sqlText = new StringBuilder();
        try
        {



            sqlText.Append(" SELECT ");
            sqlText.Append("    R.ID_RECEPCION,  ");

            sqlText.Append("    CONCAT(P.CO_PALETA, ':', UF.CO_UBICACION_FISICA)  PAL_UBIC,	  ");

            sqlText.Append("     CONVERT(VARCHAR(20), FORMAT(SM.FE_CREA, 'dd/MM/yyyy')) AS FECHA, ");
            sqlText.Append("    CONVERT(VARCHAR(20), FORMAT(SM.FE_CREA, 'HH:mm:ss')) AS HORA, ");
            sqlText.Append("    P.CO_PALETA ID_PALETA, ");

            sqlText.Append("    UF.CO_UBICACION_FISICA UBIC, ");
            sqlText.Append("    U.DE_USUARIO RESPONSABLE, ");
            sqlText.Append("    TM.DE_TIPO_MOVIMIENTO MOTIVO, ");
            sqlText.Append("    M.CO_MATERIAL MATERIAL, ");
            sqlText.Append("    M.DE_MATERIAL TEXTO_MATERIAL, ");
            sqlText.Append("    R.NU_LOTE LOTE, ");
            sqlText.Append("    TUA.DE_TIPO_UNI_ALMACENA UMA, ");
            sqlText.Append("    R.CA_CAJA_BOLSA BULTOS, ");
            sqlText.Append("     (CASE  TM.CO_TIPO_MOVIMIENTO WHEN '01' THEN ' 1' WHEN '04' THEN ' 1' WHEN '05' THEN ' 1'  WHEN '02' THEN '-1'  WHEN '03' THEN '-1'  END ) AS PALETAS, ");
            sqlText.Append("     'MPF' CLIENTE , ");
            sqlText.Append("    (CASE  TM.CO_TIPO_MOVIMIENTO WHEN '01' THEN 'Si'  WHEN '04' THEN 'Si'  WHEN '05' THEN 'Si' WHEN '02' THEN 'No' WHEN '03' THEN 'No'  END ) AS CON_INVENTARIO ");
            sqlText.Append("  FROM dbo.ALTM_RECEPCION R, dbo.ALVM_MATERIAL M, dbo.ALTV_PALETA P, dbo.ALTR_TIPO_MOVIMIENTO TM, dbo.ALTV_STOCK_MOVIMIENTO SM, dbo.ALTM_UBICACION_FISICA UF, dbo.SETM_USUARIO U, dbo.ALTR_TIPO_UNIDAD_ALMACENA TUA ");

            sqlText.Append("  WHERE R.ID_RECEPCION = P.ID_RECEPCION AND R.ID_MATERIAL = M.ID_MATERIAL AND ");
            sqlText.Append("        R.ID_ALMACEN = " + altvStockMovimiento.idAlmacen + " AND  (" + altvStockMovimiento.idUsuCrea + "=-1 OR SM.ID_USU_CREA = " + altvStockMovimiento.idUsuCrea + " ) AND ");
            sqlText.Append("        SM.ID_TIPO_MOVIMIENTO = TM.ID_TIPO_MOVIMIENTO AND SM.ID_USU_CREA = U.ID_USUARIO AND ");
            sqlText.Append("        SM.ID_PALETA = P.ID_PALETA  AND SM.ID_UBICACION_FISICA = UF.ID_UBICACION_FISICA AND ");

            sqlText.Append("        M.CO_MATERIAL LIKE '" + altvStockMovimiento.material + "%'  AND  ");
            sqlText.Append("        M.DE_MATERIAL LIKE '" + altvStockMovimiento.textoMaterial + "%'  AND  ");
            sqlText.Append("        R.NU_LOTE     LIKE '" + altvStockMovimiento.lote + "%'  AND  ");
            sqlText.Append("        P.CO_PALETA     LIKE '" + altvStockMovimiento.coPaleta + "%'  AND  ");
            sqlText.Append("        UF.CO_UBICACION_FISICA     LIKE '" + altvStockMovimiento.coUbicacion + "%'  AND  ");

            if (altvStockMovimiento.feInicio != null && altvStockMovimiento.feFin != null)
            {
                sqlText.Append("   SM.FE_CREA  BETWEEN Convert(SmallDateTime,'" + altvStockMovimiento.feInicio + " 00:00',103) AND Convert(SmallDateTime,'" + altvStockMovimiento.feFin + " 23:59',103) AND ");

            }


            sqlText.Append("        R.ID_TIPO_UNI_ALMACENA = TUA.ID_TIPO_UNI_ALMACENA ");
            sqlText.Append("  ORDER BY SM.ID_STOCK_MOVIMIENTO DESC ");

            SqlCommand cmd = new SqlCommand(sqlText.ToString(), ConectString);
            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                movimiento = new Movimiento();

                movimiento.idRecepcion = barras.GetValue(0).ToString();
                movimiento.palUbic = barras.GetValue(1).ToString();
                movimiento.fecha = barras.GetValue(2).ToString();
                movimiento.hora = barras.GetValue(3).ToString();
                movimiento.idPaleta = barras.GetValue(4).ToString();
                movimiento.ubic = barras.GetValue(5).ToString();
                movimiento.responsable = barras.GetValue(6).ToString();
                movimiento.motivo = barras.GetValue(7).ToString();
                movimiento.material = barras.GetValue(8).ToString();
                movimiento.textoMaterial = barras.GetValue(9).ToString();
                movimiento.lote = barras.GetValue(10).ToString();
                movimiento.uma = barras.GetValue(11).ToString();
                movimiento.bultos = barras.GetValue(12).ToString();
                movimiento.paletas = barras.GetValue(13).ToString();
                movimiento.cliente = barras.GetValue(14).ToString();
                movimiento.conInventario = barras.GetValue(15).ToString();

                listMovimiento.Add(movimiento);
            }
        }
        catch (Exception e)
        {
            listMovimiento = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listMovimiento;
    }


    public static string completarCaractere(string pValor, int pLongitud, string pSimbolo, string pAlineacion)
    {
        StringBuilder tempStringBuilder = new StringBuilder(pValor);
        for (int i = pValor.Length; i < pLongitud; i++)
        {
            if (pAlineacion.Equals("I"))
            {
                tempStringBuilder.Insert(0, pSimbolo);
            }
            else
            {
                tempStringBuilder.Append(pSimbolo);//tempString += pSimbolo;
            }
        }
        return tempStringBuilder.ToString();
    }

    public static List<ResumenBean> resumenMovimiento(ResumenBean resumenBean)
    {
        String resultado = "";

        List<ResumenBean> listResumenBean = new List<ResumenBean>();
        ResumenBean resumen = new ResumenBean();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();

        String sql = "";

        try
        {
            sql =
                  " SELECT " +
                    " RR.DE_ALMACEN, " +
                    " RR.MATERIAL, " +
                    " RR.TEXTO_MATERIAL, " +
                    " RR.LOTE, " +
                    " RR.UMA, " +
                    " RR.UBICACION, " +
                    //       " RR.ID_PALETA, " +
                    " SUM(RR.BULTOS) Suma_Bultos, " +
                    " SUM(RR.CANTIDAD) Suma_Paletas " +
                    " FROM (	  SELECT  " +
                                 " M.CO_MATERIAL MATERIAL,  " +
                                 " M.DE_MATERIAL TEXTO_MATERIAL,  " +
                                 " R.NU_LOTE LOTE,  " +
                                 " R.ID_TIPO_UNI_ALMACENA ID_UMA,  " +
                                 " TUA.DE_TIPO_UNI_ALMACENA UMA,   " +
                                 " UF.CO_UBICACION_FISICA UBICACION,  " +
                                 " TMM.ID_TIPO_MOVIMIENTO, " +
                                 " CONCAT(P.ID_PALETA, '-', CONVERT(VARCHAR(50), FORMAT(P.FE_CREA, 'yy')))  ID_PALETA,  " +
                                 " R.CA_PALETAS,  " +
                                 " ISNULL(P.CA_CAJA_BOLSA, R.CA_CAJA_BOLSA)   BULTOS, " +
                                 " 1 CANTIDAD, " +
                                 " R.ID_ALMACEN, " +
                                 " AL.DE_ALMACEN " +
                           " FROM dbo.ALTM_RECEPCION R, dbo.ALVM_MATERIAL M, dbo.ALTV_PALETA P, dbo.ALTR_TIPO_MOVIMIENTO TMR, dbo.ALTR_TIPO_UNIDAD_ALMACENA TUA,  " +
                           "  dbo.ALTV_STOCK_MOVIMIENTO SM, dbo.ALTM_UBICACION_FISICA UF,	(SELECT * FROM dbo.ALTR_TIPO_MOVIMIENTO TM WHERE TM.CO_TIPO_MOVIMIENTO IN ('01','04','05')) TMM	, dbo.ALTM_ALMACEN AL," +
                           //" (SELECT SM.ID_PALETA, MAX(SM.ID_STOCK_MOVIMIENTO) ID_STOCK_MOVIMIENTO, UF.ID_UBICACION_FISICA FROM dbo.ALTM_UBICACION_FISICA UF, dbo.ALTV_STOCK_MOVIMIENTO SM,  dbo.ALTM_STOCK S, dbo.ALTR_TIPO_MOVIMIENTO TM " +
                           " (SELECT SM.ID_PALETA, MAX(SM.ID_STOCK_MOVIMIENTO) ID_STOCK_MOVIMIENTO FROM dbo.ALTM_UBICACION_FISICA UF, dbo.ALTV_STOCK_MOVIMIENTO SM,  dbo.ALTM_STOCK S, dbo.ALTR_TIPO_MOVIMIENTO TM " +
                           " WHERE SM.ID_UBICACION_FISICA = UF.ID_UBICACION_FISICA AND  " +
                                 " UF.ID_UBICACION_FISICA =S.ID_UBICACION_FISICA AND " +
                                 " SM.ID_TIPO_MOVIMIENTO = TM.ID_TIPO_MOVIMIENTO  " +
                           //" GROUP BY SM.ID_PALETA, UF.ID_UBICACION_FISICA) UM	  " +
                            " GROUP BY SM.ID_PALETA) UM	  " +
                           " WHERE R.ID_RECEPCION = P.ID_RECEPCION AND R.ID_MATERIAL = M.ID_MATERIAL AND  " +
                                  " R.ID_TIPO_MOVIMIENTO = TMR.ID_TIPO_MOVIMIENTO AND  " +
                                  " R.ID_TIPO_UNI_ALMACENA = TUA.ID_TIPO_UNI_ALMACENA AND " +
                                  " P.ID_PALETA = SM.ID_PALETA AND SM.ID_UBICACION_FISICA = UF.ID_UBICACION_FISICA AND  " +
                                  " SM.ID_PALETA= UM.ID_PALETA AND SM.ID_STOCK_MOVIMIENTO = UM.ID_STOCK_MOVIMIENTO AND SM.ID_TIPO_MOVIMIENTO = TMM.ID_TIPO_MOVIMIENTO AND "+ //UM.ID_UBICACION_FISICA = UF.ID_UBICACION_FISICA AND " +
                                  " R.ID_ALMACEN = AL.ID_ALMACEN AND " +
                                  " M.CO_MATERIAL LIKE '" + resumenBean.material + "%'  AND " +
                                  " M.DE_MATERIAL LIKE '" + resumenBean.textoMaterial + "%'  AND " +
                                  " R.NU_LOTE     LIKE '" + resumenBean.lote + "%'  AND " +
                                  " UF.CO_UBICACION_FISICA     LIKE '" + resumenBean.coUbicacion.ToUpper() + "%'  AND " +


                                  " (" + resumenBean.tipoUma + " = -1 OR R.ID_TIPO_UNI_ALMACENA = '" + resumenBean.tipoUma + "' ) AND " +
                                  " (" + resumenBean.idAlmacen + " = -1 OR R.ID_ALMACEN = " + resumenBean.idAlmacen + ") " +
                                  " ) RR  " +
                        " GROUP BY	RR.DE_ALMACEN, " +
                                   " RR.MATERIAL, " +
                                   " RR.TEXTO_MATERIAL, " +
                                   " RR.LOTE, " +
                                   " RR.UMA, " +
                                   " RR.UBICACION " +
                    //               " RR.ID_PALETA, " +
                    //               " RR.BULTOS " +
                    " ORDER BY RR.DE_ALMACEN, RR.MATERIAL ASC ";




            SqlCommand cmd = new SqlCommand(sql, ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                resumenBean = new ResumenBean();
                resumenBean.deAlmacen = barras.GetValue(0).ToString();
                resumenBean.material = barras.GetValue(1).ToString();
                resumenBean.textoMaterial = barras.GetValue(2).ToString();
                resumenBean.lote = barras.GetValue(3).ToString();
                resumenBean.uma = barras.GetValue(4).ToString();
                resumenBean.ubicacion = barras.GetValue(5).ToString();
                resumenBean.sumaBultos = barras.GetValue(6).ToString();
                resumenBean.sumaPaletas = barras.GetValue(7).ToString();

                listResumenBean.Add(resumenBean);

            }
        }
        catch (Exception e)
        {
            listResumenBean = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listResumenBean;
    }

        public static List<ResumenBean> resumenMovimientoDetallado(ResumenBean resumenBean)
        {
            String resultado = "";

            List<ResumenBean> listResumenBean = new List<ResumenBean>();
            ResumenBean resumen = new ResumenBean();
            SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
            ConectString.Open();
            String sql = "";
            try
            {
                sql =
                      " SELECT " +
                        " RR.DE_ALMACEN, " +
                        " RR.MATERIAL, " +
                        " RR.TEXTO_MATERIAL, " +
                        " RR.LOTE, " +
                        " RR.UMA, " +
                        " RR.UBICACION, " +
                        //       " RR.ID_PALETA, " +
                        " RR.BULTOS  Suma_Bultos, " +
                        " RR.CANTIDAD  Suma_Paletas, " +
                        " RR.ID_STOCK_MOVIMIENTO, RR.CO_PALETA " +
                        " FROM (	  SELECT  " +
                                     " SM.ID_STOCK_MOVIMIENTO, " +
                                     " M.CO_MATERIAL MATERIAL,  " +
                                     " M.DE_MATERIAL TEXTO_MATERIAL,  " +
                                     " R.NU_LOTE LOTE,  " +
                                     " R.ID_TIPO_UNI_ALMACENA ID_UMA,  " +
                                     " TUA.DE_TIPO_UNI_ALMACENA UMA,   " +
                                     " UF.CO_UBICACION_FISICA UBICACION,  " +
                                     " TMM.ID_TIPO_MOVIMIENTO, " +
                                     " CONCAT(P.ID_PALETA, '-', CONVERT(VARCHAR(50), FORMAT(P.FE_CREA, 'yy')))  ID_PALETA,  " +
                                     " P.CO_PALETA, " +
                                     " R.CA_PALETAS,  " +
                                     " ISNULL(P.CA_CAJA_BOLSA, R.CA_CAJA_BOLSA)   BULTOS, " +
                                     " 1 CANTIDAD, " +
                                     " R.ID_ALMACEN, " +
                                     " AL.DE_ALMACEN " +
                               " FROM dbo.ALTM_RECEPCION R, dbo.ALVM_MATERIAL M, dbo.ALTV_PALETA P, dbo.ALTR_TIPO_MOVIMIENTO TMR, dbo.ALTR_TIPO_UNIDAD_ALMACENA TUA,  " +
                               "  dbo.ALTV_STOCK_MOVIMIENTO SM, dbo.ALTM_UBICACION_FISICA UF,(SELECT * FROM dbo.ALTR_TIPO_MOVIMIENTO TM WHERE TM.CO_TIPO_MOVIMIENTO IN ('01','04','05')) TMM, dbo.ALTM_ALMACEN AL," +
                               //" (SELECT SM.ID_PALETA, MAX(SM.ID_STOCK_MOVIMIENTO) ID_STOCK_MOVIMIENTO, UF.ID_UBICACION_FISICA FROM dbo.ALTM_UBICACION_FISICA UF, dbo.ALTV_STOCK_MOVIMIENTO SM,  dbo.ALTM_STOCK S, dbo.ALTR_TIPO_MOVIMIENTO TM " +
                               " (SELECT SM.ID_PALETA, MAX(SM.ID_STOCK_MOVIMIENTO) ID_STOCK_MOVIMIENTO FROM dbo.ALTM_UBICACION_FISICA UF, dbo.ALTV_STOCK_MOVIMIENTO SM,  dbo.ALTM_STOCK S, dbo.ALTR_TIPO_MOVIMIENTO TM " +
                               " WHERE SM.ID_UBICACION_FISICA = UF.ID_UBICACION_FISICA AND  " +
                                     " UF.ID_UBICACION_FISICA =S.ID_UBICACION_FISICA AND " +
                                     " SM.ID_TIPO_MOVIMIENTO = TM.ID_TIPO_MOVIMIENTO  " +
                                //" GROUP BY SM.ID_PALETA, UF.ID_UBICACION_FISICA) UM	  " +
                                " GROUP BY SM.ID_PALETA) UM	  " +
                               " WHERE R.ID_RECEPCION = P.ID_RECEPCION AND R.ID_MATERIAL = M.ID_MATERIAL AND  " +
                                      " R.ID_TIPO_MOVIMIENTO = TMR.ID_TIPO_MOVIMIENTO AND  " +
                                      " R.ID_TIPO_UNI_ALMACENA = TUA.ID_TIPO_UNI_ALMACENA AND " +
                                      " P.ID_PALETA = SM.ID_PALETA AND SM.ID_UBICACION_FISICA = UF.ID_UBICACION_FISICA AND  " +
                                      " SM.ID_PALETA= UM.ID_PALETA AND SM.ID_STOCK_MOVIMIENTO = UM.ID_STOCK_MOVIMIENTO AND SM.ID_TIPO_MOVIMIENTO = TMM.ID_TIPO_MOVIMIENTO AND " + //UM.ID_UBICACION_FISICA = UF.ID_UBICACION_FISICA AND " +
                                      " R.ID_ALMACEN = AL.ID_ALMACEN AND " +
                                      " M.CO_MATERIAL LIKE '" + resumenBean.material + "%'  AND " +
                                      " M.DE_MATERIAL LIKE '" + resumenBean.textoMaterial + "%'  AND " +
                                      " R.NU_LOTE     LIKE '" + resumenBean.lote + "%'  AND " +
                                      " UF.CO_UBICACION_FISICA     LIKE '" + resumenBean.coUbicacion.ToUpper() + "%'  AND " +


                                      " (" + resumenBean.tipoUma + " = -1 OR R.ID_TIPO_UNI_ALMACENA = '" + resumenBean.tipoUma + "' ) AND " +
                                      " (" + resumenBean.idAlmacen + " = -1 OR R.ID_ALMACEN = " + resumenBean.idAlmacen + ") " +
                                      " ) RR  " +

                        " ORDER BY RR.DE_ALMACEN, RR.MATERIAL, RR.ID_STOCK_MOVIMIENTO,RR.CO_PALETA ASC "; 

                SqlCommand cmd = new SqlCommand(sql, ConectString);

                SqlDataReader barras = cmd.ExecuteReader();

                while (barras.Read())
                {
                    resumenBean = new ResumenBean();
                    resumenBean.deAlmacen = barras.GetValue(0).ToString();
                    resumenBean.material = barras.GetValue(1).ToString();
                    resumenBean.textoMaterial = barras.GetValue(2).ToString();
                    resumenBean.lote = barras.GetValue(3).ToString();
                    resumenBean.uma = barras.GetValue(4).ToString();
                    resumenBean.ubicacion = barras.GetValue(5).ToString();
                    resumenBean.bulto = barras.GetValue(6).ToString();
                    resumenBean.sumaPaletas = barras.GetValue(7).ToString();
                    resumenBean.idStockMovimiento = Int32.Parse(barras.GetValue(8).ToString());
                    resumenBean.coPaleta = barras.GetValue(9).ToString();
                  

                    listResumenBean.Add(resumenBean);

                }
            }
            catch (Exception e)
            {
                listResumenBean = null;
            }
            finally
            {
                ConectString.Close();
            }
            return listResumenBean;
        }


        public static ResumenBloqueBean resumenBloque(AltmLayout layout)
    {
        String resultado = "";


        ResumenBloqueBean resumen = new ResumenBloqueBean();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();

        String sql = "";

        try
        {
            sql = " " +
        " SELECT TT.TOTAL_ALMACEN, SUM(TT.LLENO) LLENO_ACTIVOS, ISNULL(SUM(TT.INACTIVOS),0)  INACTIVOS, " +
        " SUM(TT.CA_LLENO) CA_LLENO_ACTIVOS, ISNULL(SUM(TT.CA_CTIVOS),0) CA_CTIVOS, ISNULL(SUM(TT.CA_INACTIVOS),0) CA_INACTIVOS FROM " +
        " (SELECT LA.CA_CARRILES* LA.CA_NIVELES AS TOTAL_ALMACEN, UF.CO_UBICACION_FISICA, (S.CA_STOCK) AS CANTIDAD, UF.NU_CANT_MAX TOPE, " +
            " CASE " +
            "   WHEN S.CA_STOCK >= UF.NU_CANT_MAX THEN 1 " +
            "   WHEN S.CA_STOCK < UF.NU_CANT_MAX THEN 0 " +
            " END AS LLENO, " +
            " CASE " +
            "   WHEN UF.ID_ESTADO = 0 THEN 1 " +
            " END AS INACTIVOS, " +
            " UF.NU_CANT_MAX CA_LLENO, " +
            " CASE WHEN UF.ID_ESTADO = 1 THEN S.CA_STOCK END AS CA_CTIVOS,  " +
            " CASE WHEN UF.ID_ESTADO = 0 THEN UF.NU_CANT_MAX END AS CA_INACTIVOS " +
         " FROM DBO.ALTM_LAYOUT LA " +
         " INNER JOIN DBO.ALTM_UBICACION_FISICA UF ON(LA.ID_LAYAOUT = UF.ID_LAYAOUT) " +
         " LEFT OUTER JOIN DBO.ALTM_STOCK S ON(UF.ID_UBICACION_FISICA = S.ID_UBICACION_FISICA) " +
        " WHERE LA.ID_ALMACEN = " + layout.idAlmacen + " AND LA.ID_LAYAOUT = " + layout.idLayaout +
       "  ) TT " +
       "  GROUP BY TT.TOTAL_ALMACEN ";




            SqlCommand cmd = new SqlCommand(sql, ConectString);

            SqlDataReader barras = cmd.ExecuteReader();
            int disponibles = 0;
            int espacioDisponibles = 0;
            while (barras.Read())
            {
                resumen = new ResumenBloqueBean();

                resumen.totalAlmacen = "" + (Int32.Parse(barras.GetValue(0).ToString()) - Int32.Parse(barras.GetValue(2).ToString()));
                resumen.llenoActivos = barras.GetValue(1).ToString();
                resumen.inactivos = barras.GetValue(2).ToString();

                disponibles = Int32.Parse(barras.GetValue(0).ToString()) - Int32.Parse(barras.GetValue(1).ToString()) - Int32.Parse(barras.GetValue(2).ToString());
                resumen.disponibles = "" + disponibles;

                resumen.totalEspacioAlmacen = "" + (Int32.Parse(barras.GetValue(3).ToString()) - Int32.Parse(barras.GetValue(5).ToString()));
                resumen.espacioActivos = barras.GetValue(4).ToString();
                resumen.espacioInactivos = barras.GetValue(5).ToString();

                espacioDisponibles = Int32.Parse(barras.GetValue(3).ToString()) - Int32.Parse(resumen.espacioActivos) - Int32.Parse(resumen.espacioInactivos);
                resumen.espacioDisponibles = "" + espacioDisponibles;




                break;

            }
        }
        catch (Exception e)
        {
            // resumen = null;
        }
        finally
        {
            ConectString.Close();
        }
        return resumen;
    }

    public static List<ResumenBean> resumenMovimientoLista(ResumenBean resumenBean)
    {
        String resultado = "";

        List<ResumenBean> listResumenBean = new List<ResumenBean>();
        ResumenBean resumen = new ResumenBean();
        SqlConnection ConectString = new SqlConnection(DBConn.alamcenDS(null));
        ConectString.Open();

        String sql = "";

        try
        {
            sql =
                  " SELECT " +
                    " RR.TEXTO_MATERIAL, " +
                    " RR.MATERIAL, " +
                    " RR.LOTE, " +
                    " RR.UMA, " +
                    " RR.UBICACION, " +
                    " RR.ID_PALETA, " +
                    " RR.BULTOS Suma_Bultos, " +
                    " count(1) Suma_Paletas " +
                    " FROM (	  SELECT  " +
                                 " M.CO_MATERIAL MATERIAL,  " +
                                 " M.DE_MATERIAL TEXTO_MATERIAL,  " +
                                 " R.NU_LOTE LOTE,  " +
                                 " R.ID_TIPO_UNI_ALMACENA ID_UMA,  " +
                                 " TUA.DE_TIPO_UNI_ALMACENA UMA,   " +
                                 " UF.CO_UBICACION_FISICA UBICACION,  " +
                                 " TMM.ID_TIPO_MOVIMIENTO, " +
                                 " CONCAT(P.ID_PALETA, '-', CONVERT(VARCHAR(50), FORMAT(P.FE_CREA, 'yy')))  ID_PALETA,  " +
                                 " R.CA_PALETAS,  " +
                                 " R.CA_CAJA_BOLSA  BULTOS, " +
                                 " 1 CANTIDAD " +
                           " FROM dbo.ALTM_RECEPCION R, dbo.ALVM_MATERIAL M, dbo.ALTV_PALETA P, dbo.ALTR_TIPO_MOVIMIENTO TMR, dbo.ALTR_TIPO_UNIDAD_ALMACENA TUA,  " +
                           " 	 dbo.ALTV_STOCK_MOVIMIENTO SM, dbo.ALTM_UBICACION_FISICA UF,	(SELECT * FROM dbo.ALTR_TIPO_MOVIMIENTO TM WHERE TM.CO_TIPO_MOVIMIENTO IN ('01','04','05')) TMM	,	  " +
                           " (SELECT SM.ID_PALETA, MAX(SM.ID_STOCK_MOVIMIENTO) ID_STOCK_MOVIMIENTO FROM dbo.ALTM_UBICACION_FISICA UF, dbo.ALTV_STOCK_MOVIMIENTO SM,  dbo.ALTM_STOCK S, dbo.ALTR_TIPO_MOVIMIENTO TM " +
                           " WHERE SM.ID_UBICACION_FISICA = UF.ID_UBICACION_FISICA AND  " +
                                 " UF.ID_UBICACION_FISICA =S.ID_UBICACION_FISICA AND " +
                                 " SM.ID_TIPO_MOVIMIENTO = TM.ID_TIPO_MOVIMIENTO  " +
                           " GROUP BY SM.ID_PALETA) UM	  " +
                           " WHERE R.ID_RECEPCION = P.ID_RECEPCION AND R.ID_MATERIAL = M.ID_MATERIAL AND  " +
                                  " R.ID_TIPO_MOVIMIENTO = TMR.ID_TIPO_MOVIMIENTO AND  " +
                                  " R.ID_TIPO_UNI_ALMACENA = TUA.ID_TIPO_UNI_ALMACENA AND " +
                                  " P.ID_PALETA = SM.ID_PALETA AND SM.ID_UBICACION_FISICA = UF.ID_UBICACION_FISICA AND  " +
                                  " SM.ID_PALETA= UM.ID_PALETA AND SM.ID_STOCK_MOVIMIENTO = UM.ID_STOCK_MOVIMIENTO AND SM.ID_TIPO_MOVIMIENTO = TMM.ID_TIPO_MOVIMIENTO AND " +
                                   " M.CO_MATERIAL LIKE '" + resumenBean.material + "%'  AND " +
                                  " M.DE_MATERIAL LIKE '" + resumenBean.textoMaterial + "%'  AND " +
                                  " R.NU_LOTE     LIKE '" + resumenBean.lote + "%'  AND " +
                                  " R.ID_TIPO_UNI_ALMACENA = '" + resumenBean.tipoUma + "' " +
                                  " ) RR  " +
                        " GROUP BY	RR.MATERIAL, " +
                                   " RR.TEXTO_MATERIAL, " +
                                   " RR.LOTE, " +
                                   " RR.UMA, " +
                                   " RR.UBICACION, " +
                                   " RR.ID_PALETA, " +
                                   " RR.BULTOS " +
                    " ORDER BY RR.ID_PALETA ASC ";




            SqlCommand cmd = new SqlCommand(sql, ConectString);

            SqlDataReader barras = cmd.ExecuteReader();

            while (barras.Read())
            {
                resumenBean = new ResumenBean();

                resumenBean.material = barras.GetValue(0).ToString();
                resumenBean.textoMaterial = barras.GetValue(1).ToString();
                resumenBean.lote = barras.GetValue(2).ToString();
                resumenBean.uma = barras.GetValue(3).ToString();
                resumenBean.ubicacion = barras.GetValue(4).ToString();
                resumenBean.idPaleta = barras.GetValue(5).ToString();
                resumenBean.sumaBultos = barras.GetValue(6).ToString();
                resumenBean.sumaPaletas = barras.GetValue(7).ToString();

                listResumenBean.Add(resumenBean);

            }
        }
        catch (Exception e)
        {
            listResumenBean = null;
        }
        finally
        {
            ConectString.Close();
        }
        return listResumenBean;
    }
}
  
}